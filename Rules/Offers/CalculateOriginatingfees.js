function CalculateOriginatingfees(loanAmount, originatingFeePer,originatingFeeMinLoanAmount,fixedOriginatingFee) {
        if(loanAmount<originatingFeeMinLoanAmount)
		{
			return fixedOriginatingFee;
		}
		
		return (loanAmount * originatingFeePer) / 100;
    }