function CalculateDefaultOffer(payload) {

	if (payload != null && payload.application != null) {
		var drawDownData = payload.application;
		var approveAmount =0;
		if (drawDownData.requestedAmount < 5000) {
			approveAmount = drawDownData.requestedAmount;
		}
		else{
			approveAmount =5000;
		}

		var Data = {
			'approveAmount': approveAmount
		};
		return {
			'result': 'Passed',
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} else {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': ['Offer Data not found']
		};
	}
}
