function data_attribute_drawdown_created(payload) {

	if (payload != null && payload.eventData.DrawDown != null) {
		var drawDownData = payload.eventData.DrawDown;
		var application = payload.eventData.LocApplication;
		var dealData =  payload.eventData.SelectedDealoffer;
		var Data = {
			'DrawDown':{
				'DrawDownData':drawDownData,
				'applicationData':application,
				'selatedDealOffer' : dealData
			}
		};
		return {
			'result':'Passed',
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} else {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': ['Offer Data not found']
		};
	}
}
