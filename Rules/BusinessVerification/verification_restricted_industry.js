function RestrictedIndustryVerificationRule(payload) {
    var result = 'Passed';
    var IsRestricted = false;
    var Industry = '';
    var IndustryName = '';
    if (payload && payload.application) {
        var input = payload.application[0];
        if (input) {
            Industry = input.industry;
            var objIndustryList = payload.industryList_Attribute[0];
            IsRestricted = objIndustryList.Industry[0].IsRestricted;
            IndustryName = objIndustryList.Industry[0].IndustryName;
            if (IsRestricted == true) {
                result = 'Failed';
            }
        }
    }
    var Data = {
        'IsRestrictedIndustry': IsRestricted
    };
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}
