function data_attribute_accountdata(payload) {
	var DataAttributePostFix = '';
	function GetAccountList(payload) {
		var result = 'Failed';
		var accountList = [];
		try {
			if (typeof(payload) != 'undefined') {
				var Accounts = [];
				var cashFlowInput = payload.eventData.Response;
				Accounts = cashFlowInput.Accounts;
				result = 'Passed';
				Accounts.forEach(function (account) {
					var accountViewModel = {
						'AccountID': '',
						'InstitutionName': '',
						'AccountType': '',
						'referenceNumber': ''
					};
					if (account.AccountMeta != null) {
						accountViewModel.AccountID = account.AccountMeta.Number;
						accountViewModel.AccountType = account.AccountMeta.Name;
					}
					accountViewModel.InstitutionName = account.InstitutionType;
					DataAttributePostFix = account.InstitutionType.replace(' ', '');
					accountViewModel.referenceNumber = payload.eventData.ReferenceNumber;
					accountList.push(accountViewModel);
				});
			}
			var resultKey = 'AccountDetail-' + DataAttributePostFix;
			var Data = {};
			Data[resultKey] = accountList;
			return {
				'result': result,
				'detail': null,
				'data': Data,
				'rejectcode': '',
				'exception': []
			};
		} catch (e) {
			return {
				'result': result,
				'detail': null,
				'data': null,
				'exception': [e.message],
				'rejectCode': ''
			};
		}
	}
	return GetAccountList(payload);
}