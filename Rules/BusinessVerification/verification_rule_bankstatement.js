function verification_rule_bankstatement(payload) {
	var result = 'Failed';
	var AccountNumber = '';
	var RoutingNumber = '';
	var MinRoutingNumberLength = 9;
	try {
		if (payload != null && payload != undefined) {
			var objData = payload.bankVerificationData[0];
			var fundingData = payload.fundingAccount[0];
			AccountNumber = objData.accountMatch;
			RoutingNumber = objData.routingMatch;
			if (RoutingNumber.length >= MinRoutingNumberLength) {
				result = 'Passed';
			}
		}
		var Data = {
			'AccountNumber': AccountNumber,
			'RoutingNumber': RoutingNumber
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
}