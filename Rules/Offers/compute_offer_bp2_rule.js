function Compute_offer_bp2_rule(payload) {

    function GetRoundNumber(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals).toFixed(decimals);
    }
    function validateInput(input) {
        var errorData = [];
        var errorMessage;
        if (typeof (input) != 'undefined') {
            var tierAttribute = input.tier_Attribute[0];
            if (typeof (tierAttribute) == 'undefined') {
                errorMessage = 'Tier attribute data not found';
                errorData.push(errorMessage);
                return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
            if (tierAttribute.value == '4') {
                return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
            var grossSaleAttribute = input.annual_gross_sale_Attribute[0];
            if (typeof (grossSaleAttribute) == 'undefined') {
                errorMessage = 'Annual gross sale attribute data not found';
                errorData.push(errorMessage);
                return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
            var sellRateAttribute = input.gradelookup_Attribute[0];
            if (typeof (sellRateAttribute) == 'undefined') {

                errorMessage = 'SellRate attribute data not found';
                errorData.push(errorMessage);
                return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
            return {
                'result': 'Passed',
                'detail': errorData,
                'data': null,
                'rejectCode': ''
            };

        }
        else {
            errorMessage = 'input data not found.';
            errorData.push(errorMessage);
            return {
                'result': false,
                'detail': null,
                'data': null,
                'exception': errorData,
                'rejectCode': ''
            };
        }
    }
    function ComputeOffers(payload) {
        try {
            var objValidateInput = validateInput(payload);
            if (objValidateInput.result == 'Passed') {
                var annual_gross_sale = 0;
                annual_gross_sale = parseFloat(payload.annual_gross_sale_Attribute[0].value);
                var Tier = payload.tier_Attribute[0];
                var Term = [];
                var offers = [];
                /*  if (parseInt(Tier.SegmentLableValue) != 4) { */
                Term = payload.gradelookup_Attribute[0].gradeList;
                var loan_amount_cap = GetLoanAmountCap(parseInt(Tier.value));
                if (Term != null && Term.length > 0) {
                    for (var j = 0; j < Term.length; j++) {
                        var monthly_revenue = annual_gross_sale / 12;
                        var daily_revenue = monthly_revenue / 21;
                        var max_daily_debt_obligation = 0,
                            max_debt_payment = 0;
                        if (Term[j].pti != '') {
                            max_daily_debt_obligation = daily_revenue * Term[j].pti;
                            max_debt_payment = max_daily_debt_obligation * 21 * Term[j].term;
                        }
                        var cal_loan_offer = 0;
                        var buy_rate = 0;
                        var sell_rate = 0;
                        if (Term[j].sell_rate != '') {
                            cal_loan_offer = max_debt_payment / Term[j].sell_rate;
                            buy_rate = Term[j].sell_rate - 0.15;
                            buy_rate = GetRoundNumber(buy_rate, 2);
                            cal_loan_offer = GetRoundNumber(cal_loan_offer, 0);
                            sell_rate = GetRoundNumber(Term[j].sell_rate, 2);
                        }
                        var loan_offer = cal_loan_offer;
                        if (cal_loan_offer >= loan_amount_cap) {
                            loan_offer = loan_amount_cap;
                        }
                        var offerModel = {
                            'term': Term[j].term,
                            'sell_rate': sell_rate,
                            'pti': Term[j].pti,
                            'monthly_revenue': monthly_revenue,
                            'daily_revenue': daily_revenue,
                            'max_daily_debt_obligation': max_daily_debt_obligation,
                            'max_debt_payment': max_debt_payment,
                            'cal_loan_offer': cal_loan_offer,
                            'loan_offer_min': 1,
                            'loan_offer_max': loan_offer,
                            'loan_amount_cap': loan_amount_cap,
                            'buy_rate': buy_rate
                        };
                        if (loan_offer > 0) {
                            offers.push(offerModel);
                        }
                    }
                }
                var Data = {
                    'offers': offers
                };
                return {
                    'Result': true,
                    'Data': Data
                };
            }
            else {
                return objValidateInput;
            }

        } catch (e) {
            return {
                'result': false,
                'detail': null,
                'data': null,
                'exception': [e.message],
                'rejectCode': ''
            };
        }
    }

    function GetLoanAmountCap(tier) {
        if (tier == 1) return 50000;
        else if (tier == 2) return 30000;
        else if (tier == 3) return 20000;
    }
    return ComputeOffers(payload);
}
