function re_apply_eligibility_check(payload) {
    var result = 'Failed';
    var Data = {};
    var eligibleStatus = ['600.40', '600.30', '600.20', '600.10'];
    var reapplyDaysLimit = 0;
    var enableReapplyDaysWindow = 10;
    if (payload.eventData && payload.eventData.CurrentStatus && payload.application
        && payload.application[0]) {
        var application = payload.application[0];
        var currentStatus = payload.eventData.CurrentStatus;
        var reapplyData = null;
        if (payload.reapply && payload.reapply[0]) {
            reapplyData = payload.reapply[0];
        };
        if (reapplyData && reapplyData.ReapplyEnabled) {
            var enablereapplydate = new Date(reapplyData.ReapplyEnabledOn.Time);
            var todaysDate = new Date();
            var dayDiffForEnableReapply = this.call('datedifference', ['D', enablereapplydate, todaysDate]);
            return {
                'result': 'Passed',
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': []
            };
        }
        if (eligibleStatus.indexOf(currentStatus.Code) > -1) {
            if (application.applicationDate && application.applicationDate.Time) {
                var appDate = new Date(application.applicationDate.Time);
                var todaysDate = new Date();
                var dayDiff = this.call('datedifference', ['D', appDate, todaysDate]);
                if (dayDiff > reapplyDaysLimit) {
                    result = 'Passed';
                }
                Data = {
                    'dayDiff': dayDiff,
                    'StatusCode': currentStatus.Code,
                    'appDate': appDate,
                    'todaysDate': todaysDate
                };
            }
        }
    }
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}