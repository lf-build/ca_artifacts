function verification_rule_businesscreditrisk(payload) {

	var result = 'Failed';
	var errorData = [];
	var errorMessage;
	var Data = {};

	try {
		if (payload == null) {
			errorMessage = 'Payload/ApplicationNumber is not filled properly';
			errorData.push(errorMessage);
			result = result;
		}
		var experianBusinessResponse = payload.experianBusinessReport[0];
		if (experianBusinessResponse != null && !experianBusinessResponse.Failed) {
			result = 'Passed';
			Data = {
				'referenceNumber': experianBusinessResponse.referenceNumber
			};

		}

	} catch (e) {
		errorMessage = 'Unable to verify : ' + e.message;
		errorData.push(errorMessage);
		result = result;
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': errorData
	};
}
