function verification_rule_businessaddresswhitepages(payload) {
	var result = 'Failed';
	if (payload && payload.whitepageReport[0]) {
		var whitePageData = payload.whitepageReport[0];
		try {
			var data = {
				'referenceNumber': whitePageData.referenceNumber
			};
			if (whitePageData.AddressChecks !== null && whitePageData.AddressChecks.IsValid === true) {
				result = 'Passed';
			}
			return {
				'result': result,
				'detail': ['Whitepages Business Address Verification Done'],
				'data': data,
				'rejectcode': '',
				'exception': []
			};
		} catch (error) {
			return {
				'result': 'Failed',
				'detail': [error.message],
				'data': null,
				'rejectcode': '',
				'exception': [error.message]
			};
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': null,
		'rejectcode': '',
		'exception': []
	};
}
