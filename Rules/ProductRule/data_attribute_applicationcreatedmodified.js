function data_attribute_applicationcreatedmodified(payload) {
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	};
	var result = 'Passed';
	var BusinessApplicantName = null;
	var SICCode = null;
	var DBA = null;
	var DUNSNumber = null;
	var NAICCode = null;
	var ContactFirstName = null;
	var ContactLastName = null;
	var ContactMiddleName = null;
	var ApplicantBankIsDefault = null;
	var ApplicantBankIsVerified = null;
	var ApplicantBankVerifiedDate = null;
	var ApplicantBankAccountHolderName = null;
	var ApplicantBankAccountNumber = null;
	var ApplicantBankAccountType = null;
	var ApplicantBankId = null;
	var ApplicantBankName = null;
	var ApplicantBankRoutingNumber = null;
	var ApplicantBankVerifiedBy = null;
	var ApplicantPhone = null;
	var BusinessPhone = null;
	var ApplicantWorkEmail = '';
	var Tags = null;
	var Gurantors = null;
	var LoanTimeFrame = null;
	var Submitted = null;
	var ProductId = null;
	var RequestedTermType = null;
	var ApplicationId = null;
	var ApplicationNumber = null;
	var ApplicationDate = null;
	var RequestedAmount = null;
	var RequestedTermValue = null;
	var PurposeOfLoan = null;
	var DecisionDate = null;
	var ExpiryDate = null;
	var BusinessAddress = null;
	var SocialLinks = null;
	var BusinessFax = null;
	var BusinessWebsite = null;
	var BusinessStartDate = null;
	var BusinessTaxID = null;
	var LegalBusinessName = null;
	var BusinessType = null;
	var SourceId = null;
	var SourceType = null;
	var StatusHistory = null;
	var StatusCode = null;
	var StatusName = null;
	var StatusDate = null;
	var Owners = null;
	var AnnualRevenue = null;
	var AverageBankBalance = null;
	var PropertyType = null;
	var Industry = null;
	var HaveExistingLoan = null;
	if (typeof(payload) != 'undefined') {
		var application = payload.eventData.Application;
		var applicant = payload.eventData.Applicant;
		if (applicant.PhoneNumbers != null && applicant.PhoneNumbers.length > 0) {
			for (var i = 0; i < applicant.PhoneNumbers.length; i++) {
				if (applicant.PhoneNumbers[i].PhoneType == 'Mobile') {
					ApplicantPhone = applicant.PhoneNumbers[i].Phone;
				}
			}
		}
		if (application.PrimaryPhone != null) {
			BusinessPhone = application.PrimaryPhone.Phone;
		}
		if (applicant.EmailAddresses != null && applicant.EmailAddresses.length > 0) {
			for (var i = 0; i < applicant.EmailAddresses.length; i++) {
				if (applicant.EmailAddresses[i].EmailType == 'Work') {
					ApplicantWorkEmail = applicant.EmailAddresses[i].Email;
				}
			}
		}
		if (applicant.Owners != null && applicant.Owners.length > 0) {
			Owners = applicant.Owners;
			var objPrimaryOwner = Owners.filter(function (item) {
					if (item.IsPrimary == true) {
						return item;
					}
				});
			var primaryOwner = null;
			if (objPrimaryOwner != null && objPrimaryOwner.length > 0) {
				primaryOwner = objPrimaryOwner[0];
				BusinessApplicantName = jsUcfirst(primaryOwner.FirstName) + ' ' + jsUcfirst(primaryOwner.LastName);
				ContactFirstName = jsUcfirst(primaryOwner.FirstName);
				ContactMiddleName = null;
				ContactLastName = jsUcfirst(primaryOwner.LastName);
			}
		}
		if (applicant.Addresses != null && applicant.Addresses.length > 0) {
			for (var i = 0; i < applicant.Addresses.length; i++) {
				if (applicant.Addresses[i].AddressType == 'Business') {
					BusinessAddress = applicant.Addresses[i];
				}
			}
		}
		if (applicant.SocialLinks != null) {
			SocialLinks[0] = applicant.SocialLinks.FacebookAddress;
			SocialLinks[1] = applicant.SocialLinks.YelpAddress;
			SocialLinks[2] = applicant.SocialLinks.GoogleAddress;
		}
		BusinessFax = applicant.PrimaryFax;
		BusinessWebsite = applicant.BusinessWebsite;
		BusinessStartDate = applicant.BusinessStartDate;
		BusinessTaxID = applicant.BusinessTaxID;
		LegalBusinessName = applicant.LegalBusinessName;
		BusinessType = applicant.BusinessType;
		DBA = applicant.DBA;
		SICCode = applicant.SICCode;
		DUNSNumber = applicant.DUNSNumber;
		NAICCode = applicant.NAICCode;
		ProductId = application.ProductId;
		if (application.LinkedBankInformation != null) {
			ApplicantBankIsDefault = application.LinkedBankInformation.IsDefault;
			ApplicantBankAccountHolderName = application.LinkedBankInformation.AccountHolderName;
			ApplicantBankName = application.LinkedBankInformation.BankName;
			ApplicantBankId = application.LinkedBankInformation.BankId;
			ApplicantBankRoutingNumber = application.LinkedBankInformation.RoutingNumber;
			ApplicantBankVerifiedBy = application.LinkedBankInformation.VerifiedBy;
			ApplicantBankIsVerified = application.LinkedBankInformation.IsVerified;
			ApplicantBankVerifiedDate = application.LinkedBankInformation.VerifiedDate;
			ApplicantBankAccountNumber = application.LinkedBankInformation.AccountNumber;
			ApplicantBankAccountType = application.LinkedBankInformation.AccountType;
		}
		ApplicationId = application.ApplicantId;
		ApplicationNumber = application.ApplicationNumber;
		LoanTimeFrame = application.DateNeeded;
		Gurantors = application.Gurantors;
		Submitted = application.ApplicationDate;
		ApplicationDate = application.ApplicationDate;
		RequestedAmount = application.RequestedAmount;
		RequestedTermType = application.RequestedTermType;
		RequestedTermValue = application.RequestedTermValue;
		PurposeOfLoan = application.PurposeOfLoan;
		DecisionDate = application.DecisionDate;
		ExpiryDate = application.ExpiryDate;
		AnnualRevenue = application.SelfDeclareInformation.AnnualRevenue;
		AverageBankBalance = application.SelfDeclareInformation.AverageBankBalance;
		HaveExistingLoan = application.SelfDeclareInformation.IsExistingBusinessLoan;
		PropertyType = applicant.PropertyType;
		Industry = applicant.Industry;
		if (application.Source != null) {
			SourceId = application.Source.SourceReferenceId;
			SourceType = application.Source.SourceType;
		}
		StatusCode = payload.eventData.StatusCode;
		StatusDate = payload.eventData.StatusDate;
		StatusName = payload.eventData.StatusName;
		Tags = payload.eventData.Tags;
	}
	var Data = {
		'application': {
			'businessApplicantName': BusinessApplicantName,
			'sICCode': SICCode,
			'dBA': DBA,
			'dUNSNumber': DUNSNumber,
			'nAICCode': NAICCode,
			'applicationDate': ApplicationDate,
			'contactFirstName': ContactFirstName,
			'contactLastName': ContactLastName,
			'contactMiddleName': ContactMiddleName,
			'applicantBankIsDefault': ApplicantBankIsDefault,
			'applicantBankIsVerified': ApplicantBankIsVerified,
			'applicantBankVerifiedDate': ApplicantBankVerifiedDate,
			'applicantBankAccountHolderName': ApplicantBankAccountHolderName,
			'applicantBankAccountNumber': ApplicantBankAccountNumber,
			'applicantBankAccountType': ApplicantBankAccountType,
			'applicantBankId': ApplicantBankId,
			'applicantBankName': ApplicantBankName,
			'applicantBankRoutingNumber': ApplicantBankRoutingNumber,
			'applicantBankVerifiedBy': ApplicantBankVerifiedBy,
			'applicantPhone': ApplicantPhone,
			'tags': Tags,
			'gurantors': Gurantors,
			'loanTimeFrame': LoanTimeFrame,
			'submitted': Submitted,
			'productId': ProductId,
			'requestedTermType': RequestedTermType,
			'applicationId': ApplicationId,
			'applicationNumber': ApplicationNumber,
			'requestedAmount': RequestedAmount,
			'requestedTermValue': RequestedTermValue,
			'purposeOfLoan': PurposeOfLoan,
			'decisionDate': DecisionDate,
			'expiryDate': ExpiryDate,
			'businessAddress': BusinessAddress,
			'socialLinks': SocialLinks,
			'businessFax': BusinessFax,
			'businessWebsite': BusinessWebsite,
			'businessStartDate': BusinessStartDate,
			'businessTaxID': BusinessTaxID,
			'legalBusinessName': LegalBusinessName,
			'businessType': BusinessType,
			'applicantWorkEmail': ApplicantWorkEmail,
			'sourceId': SourceId,
			'sourceType': SourceType,
			'statusHistory': StatusHistory,
			'statusCode': StatusCode,
			'statusName': StatusName,
			'statusDate': StatusDate,
			'owners': Owners,
			'businessPhone': BusinessPhone,
			'annualRevenue': AnnualRevenue,
			'averageBankBalance': AverageBankBalance,
			'haveExistingLoan': HaveExistingLoan,
			'propertyType': PropertyType,
			'industry': Industry
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}