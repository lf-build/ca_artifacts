function taggingRuleForDPD(payload) {

	var applyTags = [];
	var removeTags = [];
	if (payload != null && payload.eventData != null && payload.eventData.Data != null) {
		var dpdData = payload.eventData.Data.Due;

		if (dpdData.DPDDays == 0)
		{
			applyTags.push('DPD 0');
			removeTags.push('DPD 30');
				removeTags.push('DPD 90');
					removeTags.push('DPD 90+');
						removeTags.push('Collection Stage 1');
						    removeTags.push('Collection Stage 2');
					        	removeTags.push('Collection Stage 3');
						             removeTags.push('Collection Stage 4');
		}			
		else if (dpdData.DPDDays <= 30) {			
			applyTags.push('DPD 30');
			removeTags.push('DPD 0');
		} else if (dpdData.DPDDays > 30 && dpdData.DPDDays <= 60) {
			applyTags.push('DPD 60');
			removeTags.push('DPD 30');
		} else if (dpdData.DPDDays > 60 && dpdData.DPDDays <= 90) {
			applyTags.push('DPD 90');
			removeTags.push('DPD 60');
		} else if (dpdData.DPDDays > 90) {
			applyTags.push('DPD 90+');
			removeTags.push('DPD 90');
		}

		if (dpdData.DPDDays > 90 && dpdData.DPDDays <= 120) {
			applyTags.push('Collection Stage 1');
		} else if (dpdData.DPDDays > 120 && dpdData.DPDDays <= 150) {
			applyTags.push('Collection Stage 2');
			removeTags.push('Collection Stage 1');
		} else if (dpdData.DPDDays > 150 && dpdData.DPDDays <= 180) {
			applyTags.push('Collection Stage 3');
			removeTags.push('Collection Stage 2');
		} else if (dpdData.DPDDays > 180) {
			applyTags.push('Collection Stage 4');
			removeTags.push('Collection Stage 3');
		}
	}
	var Data = {
		'ApplyTags': applyTags,
		'RemoveTags': removeTags
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
