function verification_rule_businesstaxidverification(payload) {
	var result = 'Passed';
	var errorData = [];
	var errorMessage;
	var restrictedCategory = ['slow pay', 'default account', 'fraudulent', 'default'];
	var Data = { 'referenceNumber': null };
	var Notes = [];
	try {
		if (payload == null) {
			errorMessage = 'Payload/ApplicationNumber is not filled properly';
			errorData.push(errorMessage);
			result = 'Failed';
		} else if (payload.datamerchReport && payload.datamerchReport[0] && (payload.datamerchReport[0].NoHitOrFailed == true || payload.datamerchReport[0].Fein)) {
			result = 'Failed';
			Data.referenceNumber = payload.datamerchReport[0].referenceNumber;
		}
	} catch (e) {
		errorMessage = 'Unable to verify : ' + e.message;
		errorData.push(errorMessage);
		result = 'Failed';
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': errorData
	};
}
