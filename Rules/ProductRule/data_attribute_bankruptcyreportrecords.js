function data_attribute_bankruptcyreportrecords(payload) {
	var result = 'Passed';
	var allFraudRecords = [];
	var RecordsList = [];
	var key = '';
	var BankruptcyInPast = false;
	var BankruptcyInLastThan1Year = false;
	var minBankruptcy = 2;
	var myCustomRecords = {
		'BankruptcyData': [{}
		]
	};
	var Header = payload.eventData.Response.Header;
	var Records = payload.eventData.Response.Records;
	var RecordsCount = payload.eventData.Response.RecordCount;
	if (payload.eventData.Request != null && payload.eventData.Request.OwnerId != null && payload.eventData.Request.OwnerId != undefined) {
		key = payload.eventData.Request.OwnerId;
	}
	if (Records != null && Records.length > 0) {
		if (Records.length >= minBankruptcy) {
			BankruptcyInPast = true;
		} else {
			var fillDate = Records[0].FilingDate.Month + '/' + Records[0].FilingDate.Day + '/' + Records[0].FilingDate.Year;
			if (fillDate != null) {
				var Current = GetMonthDiff(new Date(fillDate), new Date());
				if (Current <= 12) {
					BankruptcyInLastThan1Year = true;
				}
			}
		}
		for (var i = 0; i < Records.length; i++) {
			var recordses = {
				Datefiled: null,
				Chapterfiled: null,
				Disposition: null,
				DebtorInformation: [],
				DateDispositioned: null,
				CaseNumber: null,
				DisplayCaseNumber: null,
				ClosedDatefiled: null
			};
			if (Records[i].FilingDate != null) {
				recordses.Datefiled = Records[i].FilingDate.Month + '/' + Records[i].FilingDate.Day + '/' + Records[i].FilingDate.Year;
			}
			recordses.Disposition = Records[i].Disposition;
			recordses.Chapterfiled = Records[i].Chapter;
			if (Records[i].IsFcra) {
				recordses.CaseNumber =  Records[i].CaseNumber;
				recordses.DisplayCaseNumber = 'Case Number:' + Records[i].CaseNumber + '      (FCRA)';
			} else {
				recordses.CaseNumber = Records[i].CaseNumber;
				recordses.DisplayCaseNumber = 'Case Number:' + Records[i].CaseNumber + '      (NON-FCRA)';
			}
			if (Records[i].Debtors && Records[i].Debtors.length > 0) {
				for (var j = 0; j < Records[i].Debtors.length; j++) {
					var debtor = {
						BusinessId: null,
						UniqueId: null,
						Ssn: null,
						AppendedSSN: null,
						TaxId: null,
						AppendedTaxId: null,
						DateTransferred: null,
						ConvertedDate: null,
						Phones: [],
						Addresses: [],
						Names: [],
					};
					debtor.BusinessId = Records[i].Debtors[j].BusinessId;
					debtor.UniqueId = Records[i].Debtors[j].UniqueId;
					debtor.Ssn = Records[i].Debtors[j].Ssn;
					debtor.AppendedSSN = Records[i].Debtors[j].AppendedSSN;
					debtor.TaxId = Records[i].Debtors[j].TaxId;
					debtor.AppendedTaxId = Records[i].Debtors[j].AppendedTaxId;
					debtor.DateTransferred = Records[i].Debtors[j].DateTransferred;
					debtor.ConvertedDate = Records[i].Debtors[j].ConvertedDate;
					debtor.Names = Records[i].Debtors[j].Names;
					debtor.Phones = Records[i].Debtors[j].Phones;
					debtor.Addresses = Records[i].Debtors[j].Addresses;
					recordses.DebtorInformation.push(debtor);
				}
			}
			if (Records[i].ClosedDate != null) {
				recordses.ClosedDatefiled = Records[i].ClosedDate.Month + '/' + Records[i].ClosedDate.Day + '/' + Records[i].ClosedDate.Year;
			}
			RecordsList.push(recordses);
		}
		myCustomRecords.BankruptcyData = RecordsList;
		var BankruptcyDataViewModel = {
			'Records': myCustomRecords,
			'referenceNumber': payload.eventData.ReferenceNumber,
			'SecondaryName': key,
			'BankruptcyInLastThan1Year': BankruptcyInLastThan1Year,
			'BankruptcyInPast': BankruptcyInPast
		};
		allFraudRecords.push(BankruptcyDataViewModel);
	}
	function GetMonthDiff(d1, d2) {
		var months;
		months = (d2.getFullYear() - d1.getFullYear()) * 12;
		months -= d1.getMonth() + 1;
		months += d2.getMonth();
		return months <= 0 ? 0 : months;
	}
	var Data = {};
	Data[key] = allFraudRecords;
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}