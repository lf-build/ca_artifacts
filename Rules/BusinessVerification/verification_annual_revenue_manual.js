function AnnualRevenueVerificationRuleManual(payload) {
    var result = 'Failed';
    var Data = {};
    if (payload && payload.annualRevenueVerificationData) {
        Data = payload.annualRevenueVerificationData[0];
        if (Data && Data.pass == true) {
            result = 'Passed';
        }
    }
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}
