function TimeInBusinessVerificationManual(payload) {
    var result = 'Failed';
    var Data = {};
    if (payload && payload.timeInBusinessVerificationData) {
        Data = payload.timeInBusinessVerificationData[0];
        if (Data && Data.pass == true) {
            result = 'Passed';
        }
    }
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}