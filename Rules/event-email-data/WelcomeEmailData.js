function WelcomeEmailData(payload) {
    function jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    if (payload && payload.eventData.DataAttributes) {
        if (payload.eventData.DataAttributes.application[0]) {
            var applicationData = payload.eventData.DataAttributes.application[0];
            var legalBusinessName = jsUcfirst(applicationData.legalBusinessName);
            var email = '';
            var fullName = '';
            var contactName = applicationData.businessApplicantName;
            var url = '{{server_url}}:{{borrower_port}}/login';
            var queryStringData = '?token=' + applicationData.applicationNumber;

            var objPrimaryOwner = applicationData.owners.filter(function (item) {
                if (item.IsPrimary == true) { return item; }
            });

            var primaryOwner = null;
            if (objPrimaryOwner != null && objPrimaryOwner.length > 0) {
                primaryOwner = objPrimaryOwner[0];
            }

            if (primaryOwner != null && primaryOwner.EmailAddresses[0]) {
                email = primaryOwner.EmailAddresses[0].Email;
                fullName = jsUcfirst(primaryOwner.FirstName);
            }
            var result = {
                Email: email,
                Name: fullName,
                ContactName: contactName,
                LegalBusinessName: legalBusinessName,
                Logo: 'https://s3-us-west-2.amazonaws.com/lf-staging-ca-assets/CALogo/ca_logo.png',
                url: url,
                queryString: queryStringData,
                ContactAddress: 'https://s3-us-west-2.amazonaws.com/lf-staging-ca-assets/CALogo/contact_address.jpg'
            };
            return {
                'result': 'Passed',
                'detail': null,
                'data': result,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    var errorData = [];
    errorData.push('Data Not found for email');
    return {
        'result': 'Failed',
        'detail': null,
        'data': '',
        'rejectcode': '',
        'exception': errorData
    };
}
