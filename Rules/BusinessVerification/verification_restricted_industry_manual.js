function RestrictedIndustryVerificationRuleManual(payload) {
    var result = 'Failed';
    var Data = {};
    if (payload && payload.restrictedIndustryVerificationData) {
        Data = payload.restrictedIndustryVerificationData[0];
        if (Data && Data.pass == true) {
            result = 'Passed';
        }
    }
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}
