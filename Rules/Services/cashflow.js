function cashflow() {
    var baseUrl = '{{cashflow_internal_url}}';
    var self = this;
    return {
        getTransactions: function (entityType, entityId, accountId) {
            var url = [baseUrl, entityType, entityId, 'transaction', accountId].join('/');
            return new Promise(function (resolve, reject) {
                try {
                    self.http.get(url).catch(function (error) {
                        reject('cashflow get failed for1 ' + entityType + ':' + entityId);
                    })
                        .then(function (response) {
                            resolve(response);
                        });
                } catch (exception) {
                    reject('cashflow get failed for2 ' + entityType + ':' + entityId + ':' + exception);
                }
            });
        }
    };
}