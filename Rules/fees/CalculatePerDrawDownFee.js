function CalculatePerDrawDownFee(payload) {
	
		if (payload == null || payload.eventData.drawDown == null || payload.eventData.selectedDealoffer == null) {
			return {
				'result': 'Failed',
				'detail': null,
				'data': null,
				'rejectcode': '',
				'exception': ['payload Data not fount']
			};
		}
		var percentageType = payload.eventData.selectedDealoffer[0].offer.drawDownFeeType;
		var drawDownFee = payload.eventData.selectedDealoffer[0].offer.drawDownFee;
		var drawdownAmount = payload.eventData.drawDown.RequestedAmount;
		var feeAmount = 0;
		if (percentageType.toLowerCase() == 'percentage') {
			feeAmount = drawdownAmount * (drawDownFee / 100);
		} else {
			feeAmount = drawDownFee;
		}
		
		var Data = {
			'feeAmount': feeAmount
		};

		return {
			'result': 'Passed',
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	
}
