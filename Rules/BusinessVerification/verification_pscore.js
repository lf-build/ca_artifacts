function PscoreVerificationRule(payload) {
    var result = 'Passed';
    var PScore = 0;
    if (payload && payload.pScore_Attributes) {
        var inputScore = payload.pScore_Attributes[0];
        if (inputScore) {
            PScore = inputScore.p_score;
            var MinPScore = 700;
            if (PScore <= MinPScore) {
                result = 'Failed';
            }
        }
    }
    var Data = {
        'PScore': PScore
    };
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}
