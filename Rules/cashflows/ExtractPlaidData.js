function ExtractPlaidData(input) {

	var errorData = [];
	try {
		var lstAccounts = [];
		var lstTransactions = [];
		var data = {};
		if (input != null) {
			var SelectedAccountId = input.SelectedAccountId;
			if (input.Accounts != null && input.Accounts.length > 0) {
				input.Accounts.forEach(function (account) {

					var objAccount = {
						'ProviderAccountId': '',
						'AvailableBalance': 0,
						'CurrentBalance': 0,
						'BankName': '',
						'AccountType': '',
						'AccountNumber': '',
						'Source': '',
						'IsCashflowAccount': false,
						'IsFundingAccount': false
					};
					objAccount.Source = 'Plaid';
					objAccount.ProviderAccountId = account.Id;
					if (account.Id == SelectedAccountId) {
						objAccount.IsCashflowAccount = true;
						objAccount.IsFundingAccount = true;
					}
					objAccount.BankName = account.InstitutionType;
					objAccount.AccountType = account.Type;
					if (account.AccountMeta != null) {
						objAccount.AccountNumber = account.AccountMeta.Number;
					}
					if (account.Balance != null) {
						objAccount.AvailableBalance = account.Balance.Available;
						objAccount.CurrentBalance = account.Balance.Current;
					}
					lstAccounts.push(objAccount);
				});
			}
			if (input.Transactions != null && input.Transactions.length > 0) {
				input.Transactions.forEach(function (transaction) {
					var objTransaction = {
						'ProviderAccountId': '',
						'AccountId': '',
						'Amount': 0,
						'Categories': '',
						'CategoryId': '',
						'Description': '',
						'Meta': '',
						'Pending': '',
						'TransactionDate': ''
					};

					objTransaction.ProviderAccountId = transaction.Account;
					objTransaction.Amount = transaction.Amount;
					objTransaction.TransactionDate = transaction.Date;
					objTransaction.Description = transaction.Name;
					objTransaction.Pending = transaction.Pending;
					objTransaction.Categories = transaction.Category;
					objTransaction.CategoryId = transaction.CategoryId;
					lstTransactions.push(objTransaction);
				});
			}

			data = {
				Accounts: lstAccounts,
				Transactions: lstTransactions
			};
		}
	} catch (e) {
		return null;
	}
	return data;
};