function data_attribute_experian_personal_report(payload) {
	var result = 'Passed';
	var riskModel = null;
	var records = null;
	var ILN7310 = 0;
	var IQT9536 = 0;
	var MTF6244 = 0;
	var AUL5020 = 0;
	var AUA6280 = 0;
	var REH3423 = 0;
	var ALL6203 = 0;
	var ALM6160 = 0;
	var ALL8323 = 0;
	var BUS0416 = 0;
	var AUA6200 = 0;
	var BCC5320 = 0;
	var Surname = '';
	var FirstName = '';
	var DOB = '';
	var StreetName = '';
	var City = '';
	var State = '';
	var Zip = '';
	var SSN = '';
	var Phone = '';
	var m_IQT9536 = 0;
	var m_ILN7310 = 0;
	var m_BUS0416 = 0;
	var m_AUL5020 = 0;
	var m_AUA6200 = 0;
	var m_AUA6280 = -0.612557391;
	var arrAUA6280_range1 = [998, 999];
	var arrAUA6280_range2 = [0, 30, 997];
	var m_ALM6160 = -0.369743806;
	var arrALM6160_range = [1, 997];
	var m_ALL8323 = 0;
	var m_ALL6203 = 0.035777717;
	var arrALL6203_range = [90, 120];
	var m_REH3423 = 0;
	var m_MTF6244 = 0;
	var m_BCC5320 = 0;
	var arrMTF6244_range1 = [1, 400];
	var arrMTF6244_range2 = [994];
	var arrAUA6200_range1 = [400];
	var arrAUA6200_range2 = [1, 30];
	var MonthlyInstallments = 0;
	var RealEstatePayment = 0;
	var TotalPayment = 0;
	var PastDueAmount = 0;
	var PublicRecordsCount = 0;
	var TotalInquiries = 0;
	var premierAttributes = null;
	var Score = '';
	var key = '';
	var InformationalMessageText = '';
	var StatementMessageText = '';
	var PDFReportAttributes = null;
	if (typeof(payload) != 'undefined') {
		var response = payload.eventData.Response;
		if (payload.eventData.Request != null && payload.eventData.Request.OwnerId != null && payload.eventData.Request.OwnerId != undefined) {
			key = payload.eventData.Request.OwnerId;
		}
		if (response.ProductCreditProfile != null) {
			if (response.ProductCreditProfile.InformationalMessage != null && response.ProductCreditProfile.InformationalMessage.length > 0) {
				if (response.ProductCreditProfile.InformationalMessage[0].MessageText != null)
					InformationalMessageText = response.ProductCreditProfile.InformationalMessage[0].MessageText;
			}
			if (response.ProductCreditProfile.Statement != null && response.ProductCreditProfile.Statement.StatementText != null && response.ProductCreditProfile.Statement.StatementText.MessageText != null) {
				StatementMessageText = response.ProductCreditProfile.Statement.StatementText.MessageText;
			}
			premierAttributes = response.ProductCreditProfile.PremierAttributes;
			if (response.ProductCreditProfile.RiskModel != null && response.ProductCreditProfile.RiskModel.length > 0) {
				Score = response.ProductCreditProfile.RiskModel[0].Score;
			}
			if (response.ProductCreditProfile.ConsumerIdentity != null && response.ProductCreditProfile.ConsumerIdentity.length > 0) {
				Surname = response.ProductCreditProfile.ConsumerIdentity[0].Name.Surname;
				FirstName = response.ProductCreditProfile.ConsumerIdentity[0].Name.First;
				var dob = response.ProductCreditProfile.ConsumerIdentity[0].Dob;
				if (response.ProductCreditProfile.ConsumerIdentity[0].Phone != null) {
					Phone = response.ProductCreditProfile.ConsumerIdentity[0].Phone.Number;
				}
				if (dob) {
					DOB = dob.replace(/(\\d\\d)(\\d\\d)(\\d\\d\\d\\d)/, '$1-$2-$3');
				}
			}
			if (response.ProductCreditProfile.AddressInformation != null && response.ProductCreditProfile.AddressInformation.length > 0) {
				StreetName = response.ProductCreditProfile.AddressInformation[0].StreetName + '  ' + response.ProductCreditProfile.AddressInformation[0].StreetSuffix;
				City = response.ProductCreditProfile.AddressInformation[0].City;
				State = response.ProductCreditProfile.AddressInformation[0].State;
				Zip = response.ProductCreditProfile.AddressInformation[0].Zip;
			}
			if (response.ProductCreditProfile.Ssn != null && response.ProductCreditProfile.Ssn.length > 0) {
				SSN = response.ProductCreditProfile.Ssn[0].Number;
			}
			if (response.ProductCreditProfile.ProfileSummary != null) {
				MonthlyInstallments = isNaN(response.ProductCreditProfile.ProfileSummary.MonthlyPayment) ? 0 : parseInt(response.ProductCreditProfile.ProfileSummary.MonthlyPayment);
				RealEstatePayment = isNaN(response.ProductCreditProfile.ProfileSummary.RealEstateBalance) ? 0 : parseInt(response.ProductCreditProfile.ProfileSummary.RealEstateBalance);
				PastDueAmount = response.ProductCreditProfile.ProfileSummary.PastDueAmount;
				TotalPayment = MonthlyInstallments + RealEstatePayment;
				PublicRecordsCount = response.ProductCreditProfile.ProfileSummary.PublicRecordsCount;
				TotalInquiries = response.ProductCreditProfile.ProfileSummary.TotalInquiries;
			}
			if (premierAttributes && premierAttributes.Attributes) {
				for (var i = 0; i < premierAttributes.Attributes.length; i++) {
					if (premierAttributes.Attributes[i].Name == 'ILN7310') {
						ILN7310 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'IQT9536') {
						IQT9536 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'MTF6244') {
						MTF6244 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'AUL5020') {
						AUL5020 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'AUA6280') {
						AUA6280 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'REH3423') {
						REH3423 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'ALL6203') {
						ALL6203 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'ALM6160') {
						ALM6160 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'ALL8323') {
						ALL8323 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'BUS0416') {
						BUS0416 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'AUA6200') {
						AUA6200 = premierAttributes.Attributes[i].Value;
					} else if (premierAttributes.Attributes[i].Name == 'BCC5320') {
						BCC5320 = premierAttributes.Attributes[i].Value;
					}
				}
			}
		}
		if (IQT9536 > 992) {
			m_IQT9536 = 30;
		} else if (IQT9536 < 0) {
			m_IQT9536 = 0;
		} else if (IQT9536 > 90) {
			m_IQT9536 = 90;
		} else if (IQT9536 >= 0 && IQT9536 <= 90) {
			m_IQT9536 = IQT9536;
		} else {
			m_IQT9536 = 30;
		}
		if (ILN7310 == 999) {
			m_ILN7310 = 40;
		} else if (ILN7310 == 998) {
			m_ILN7310 = 6;
		} else if (ILN7310 == 994) {
			m_ILN7310 = 0;
		} else if (ILN7310 > 40) {
			m_ILN7310 = 40;
		} else if (ILN7310 < 0) {
			m_ILN7310 = 0;
		} else if (ILN7310 >= 0 && ILN7310 <= 40) {
			m_ILN7310 = ILN7310;
		} else {
			m_ILN7310 = 40;
		}
		if (BUS0416 == 99) {
			m_BUS0416 = 0;
		} else if (BUS0416 == 98) {
			m_BUS0416 = 0;
		} else if (BUS0416 > 1) {
			m_BUS0416 = 1;
		} else if (BUS0416 < 0) {
			m_BUS0416 = 0;
		} else if (BUS0416 >= 0 && BUS0416 <= 1) {
			m_BUS0416 = BUS0416;
		} else {
			m_BUS0416 = 0;
		}
		if (AUL5020 == 999999999) {
			m_AUL5020 = 0;
		} else if (AUL5020 == 999999998) {
			m_AUL5020 = 0;
		} else if (AUL5020 == 999999997) {
			m_AUL5020 = 18000;
		} else if (AUL5020 == 999999996) {
			m_AUL5020 = 7000;
		} else if (AUL5020 > 50000) {
			m_AUL5020 = 50000;
		} else if (AUL5020 < 0) {
			m_AUL5020 = 0;
		} else if (AUL5020 >= 0 && AUL5020 <= 50000) {
			m_AUL5020 = AUL5020;
		}
		if (arrAUA6280_range1.indexOf(parseInt(AUA6280)) > -1) {
			m_AUA6280 = -0.194644822;
		} else if (AUA6280 == 1) {
			m_AUA6280 = 0.018771837;
		} else if (arrAUA6280_range2.indexOf(parseInt(AUA6280)) > -1) {
			m_AUA6280 = 0.301992697;
		}
		if (arrALM6160_range.indexOf(parseInt(ALM6160)) > -1) {
			m_ALM6160 = 0.368271862;
		}
		if (ALL8323 == 9999) {
			m_ALL8323 = 90;
		} else if (ALL8323 == 9994) {
			m_ALL8323 = 200;
		} else if (ALL8323 > 200) {
			m_ALL8323 = 200;
		} else if (ALL8323 < 30) {
			m_ALL8323 = 30;
		} else if (ALL8323 >= 30 && ALL8323 <= 200) {
			m_ALL8323 = ALL8323;
		} else {
			m_ALL8323 = 30;
		}
		if (arrALL6203_range.indexOf(parseInt(ALL6203)) > -1) {
			m_ALL6203 = -1.238895901;
		}
		if (REH3423 == 99) {
			m_REH3423 = 2;
		} else if (REH3423 == 98) {
			m_REH3423 = 3;
		} else if (REH3423 == 97) {
			m_REH3423 = 3;
		} else if (REH3423 > 3) {
			m_REH3423 = 3;
		} else if (REH3423 < 0) {
			m_REH3423 = 0;
		} else if (REH3423 >= 0 && REH3423 <= 3) {
			m_REH3423 = REH3423;
		} else {
			m_REH3423 = 3;
		}
		if (arrMTF6244_range1.indexOf(parseInt(MTF6244)) > -1) {
			m_MTF6244 = 1.025948864;
		} else if (arrMTF6244_range2.indexOf(parseInt(MTF6244)) > -1) {
			m_MTF6244 = 0.074812558;
		} else {
			m_MTF6244 = -0.290543237;
		}
		if (arrAUA6200_range1.indexOf(parseInt(AUA6200)) > -1) {
			m_AUA6200 = -0.495447982;
		} else if (arrAUA6200_range2.indexOf(parseInt(AUA6200)) > -1) {
			m_AUA6200 = 0.169560032;
		} else {
			m_AUA6200 = -0.172544475;
		}
		if (BCC5320 == 999999999) {
			m_BCC5320 = 0;
		} else if (BCC5320 == 999999998) {
			m_BCC5320 = 0;
		} else if (BCC5320 == 999999997) {
			m_BCC5320 = 0;
		} else if (BCC5320 == 999999996) {
			m_BCC5320 = 80000;
		} else if (BCC5320 > 80000) {
			m_BCC5320 = 80000;
		} else if (BCC5320 < 0) {
			m_BCC5320 = 0;
		} else if (BCC5320 >= 0 && BCC5320 <= 80000) {
			m_BCC5320 = BCC5320;
		} else {
			m_BCC5320 = 0;
		}
		var PDFReportAttributes = GetExperianPersonalPDFData(payload);
		records = {
			'Score': Score,
			'ILN7310': ILN7310,
			'IQT9536': IQT9536,
			'MTF6244': MTF6244,
			'AUL5020': AUL5020,
			'AUA6280': AUA6280,
			'REH3423': REH3423,
			'ALL6203': ALL6203,
			'ALM6160': ALM6160,
			'ALL8323': ALL8323,
			'BUS0416': BUS0416,
			'AUA6200': AUA6200,
			'BCC5320': BCC5320,
			'Surname': Surname,
			'FirstName': FirstName,
			'DOB': DOB,
			'StreetName': StreetName,
			'City': City,
			'State': State,
			'Zip': Zip,
			'SSN': SSN,
			'Phone': Phone,
			'm_IQT9536': m_IQT9536,
			'm_ILN7310': m_ILN7310,
			'm_BUS0416': m_BUS0416,
			'm_AUL5020': m_AUL5020,
			'm_AUA6280': m_AUA6280,
			'm_ALM6160': m_ALM6160,
			'm_ALL8323': m_ALL8323,
			'm_ALL6203': m_ALL6203,
			'm_REH3423': m_REH3423,
			'm_MTF6244': m_MTF6244,
			'm_AUA6200': m_AUA6200,
			'm_BCC5320': m_BCC5320,
			'TotalPayment': TotalPayment,
			'MonthlyInstallments': MonthlyInstallments,
			'RealEstatePayment': RealEstatePayment,
			'PastDueAmount': PastDueAmount,
			'PublicRecordsCount': PublicRecordsCount,
			'TotalInquiries': TotalInquiries,
			'referenceNumber': payload.eventData.ReferenceNumber,
			'SecondaryName': key,
			'PDFReportAttributes': PDFReportAttributes,
			'StatementMessageText': StatementMessageText,
			'InformationalMessageText': InformationalMessageText
		}
	}
	var Data = {};
	Data[key] = records;
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
	function GetExperianPersonalPDFData(input) {
		var PDFReportAttributes = '';
		var Header = '';
		var ConsumerIdentity = '';
		var ProductCreditProfile = '';
		var SSN = '';
		var AddressInformation = '';
		var EmploymentInformation = '';
		var Messages = '';
		var FraudServices = '';
		var RiskModel = '';
		var PublicRecord = '';
		var Inquiry = '';
		var TradeLine = '';
		var ConsumerAssistanceReferralAddress = '';
		var ProfileSummary = '';
		var reportDate = '';
		var reportTime = '';
		var consumerName = '';
		var socialSecurityNumber = '';
		var consumerOtherNames = '';
		var OtherSocialSecurityNumbers = '';
		var consumerDOB = '';
		var consumerAddress = ''; ;
		var consumerOtherAddresses = [];
		var employeeInfo = {};
		var employeeAlternateInfo = [];
		var response = '';
		var FraudShieldSummary = {};
		var Demographics = {};
		function _dateFormat(date) {
			if (date) {
				return date.replace(/(\\d\\d)(\\d\\d)(\\d\\d)/, '$1/$2/$3');
			}
			return null;
		};
		function _phoneFormat(phone) {
			if (phone) {
				return phone.replace(/(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)/, '($1)$2-$3');
			}
			return null;
		};
		function _ssnFormat(ssn) {
			if (ssn) {
				return ssn.replace(/(\\d\\d\\d)(\\d\\d)(\\d\\d\\d\\d)/, '$1-$2-$3');
			}
			return null;
		};
		function _timeFormat(time) {
			var convertime = '';
			if (time) {
				time = time.replace(/(\\d\\d)(\\d\\d)(\\d\\d)/, '$1:$2:$3');
				time = time.toString().match(/^([01]\\d|2[0-3])(:)([0-5]\\d)(:[0-5]\\d)?$/) || [time];
				if (time.length > 1) {
					time = time.slice(1);
					time[5] = +time[0] < 12 ? ' AM' : ' PM';
					time[0] = +time[0] % 12 || 12;
				}
				return time.join('');
			}
			return convertime;
		};
		function _getseprate(str, seprater) {
			if (str && seprater) {
				var seprateArray = [];
				seprateArray = str.split(seprater);
				return seprateArray;
			}
			return [];
		}
		function _moneyWithoutFraction(amount) {
			if (amount.trim()) {
				if (isNaN(amount)) {
					return amount;
				}
				var options = {
					style: 'currency',
					currency: 'USD'
				};
				options.minimumFractionDigits = 0;
				var currency = parseFloat(amount);
				return currency.toLocaleString('en-US', options);
			}
		};
		if (payload && payload.eventData && payload.eventData.Response) {
			response = payload.eventData.Response;
			if (response.ProductCreditProfile) {
				var ProductCreditProfile = response.ProductCreditProfile;
				if (ProductCreditProfile.Header) {
					Header = ProductCreditProfile.Header;
				}
				if (ProductCreditProfile.ConsumerIdentity) {
					ConsumerIdentity = ProductCreditProfile.ConsumerIdentity;
				}
				if (ProductCreditProfile.Demographics) {
					ConsumerIdentity = ProductCreditProfile.Demographics;
				}
				if (ProductCreditProfile.Ssn) {
					SSN = ProductCreditProfile.Ssn;
				}
				if (ProductCreditProfile.AddressInformation) {
					AddressInformation = ProductCreditProfile.AddressInformation;
				}
				if (ProductCreditProfile.EmploymentInformation) {
					EmploymentInformation = ProductCreditProfile.EmploymentInformation;
				}
				if (ProductCreditProfile.Statement) {
					Messages = ProductCreditProfile.Statement;
				}
				if (ProductCreditProfile.FraudServices) {
					FraudServices = ProductCreditProfile.FraudServices;
				}
				if (ProductCreditProfile.RiskModel) {
					RiskModel = ProductCreditProfile.RiskModel;
				}
				if (ProductCreditProfile.PublicRecord) {
					PublicRecord = ProductCreditProfile.PublicRecord;
				}
				if (ProductCreditProfile.Inquiry) {
					Inquiry = ProductCreditProfile.Inquiry;
				}
				if (ProductCreditProfile.TradeLine) {
					TradeLine = ProductCreditProfile.TradeLine;
				}
				if (ProductCreditProfile.ProfileSummary) {
					ProfileSummary = ProductCreditProfile.ProfileSummary;
				}
				if (ProductCreditProfile.ConsumerAssistanceReferralAddress) {
					ConsumerAssistanceReferralAddress = ProductCreditProfile.ConsumerAssistanceReferralAddress;
				}
				if (Header && Header.ReportDate && Header.ReportTime) {
					reportDate = _dateFormat(Header.ReportDate);
					reportTime = _timeFormat(Header.ReportTime);
				}
				if (ConsumerIdentity && ConsumerIdentity.length > 0) {
					var SurName = '';
					var FirstName = '';
					var MiddleName = '';
					if (ConsumerIdentity[0].Name) {
						if (ConsumerIdentity[0].Name.Surname) {
							SurName = ConsumerIdentity[0].Name.Surname;
						}
						if (ConsumerIdentity[0].Name.First) {
							FirstName = ConsumerIdentity[0].Name.First;
						}
						if (ConsumerIdentity[0].Name.Middle) {
							MiddleName = ConsumerIdentity[0].Name.Middle;
						}
					}
					consumerName = SurName + ' ' + FirstName + ' ' + MiddleName;
					if (ConsumerIdentity.length > 1) {
						for (var i = 1; i < ConsumerIdentity.length; i++) {
							var otherNameSurName = '';
							var otherNameFirstName = '';
							var otherNameMiddleName = '';
							if (ConsumerIdentity[i].Name) {
								if (ConsumerIdentity[0].Name.Surname) {
									otherNameSurName = ConsumerIdentity[i].Name.Surname;
								}
								if (ConsumerIdentity[i].Name.First) {
									otherNameFirstName = ConsumerIdentity[i].Name.First;
								}
								if (ConsumerIdentity[i].Name.Middle) {
									otherNameMiddleName = ConsumerIdentity[i].Name.Middle;
								}
							}
							consumerOtherNames = (consumerOtherNames + ' ' + otherNameSurName + ' ' + otherNameFirstName + ' ' + otherNameMiddleName + ',');
						}
					}
				}
				if (SSN) {
					for (var i = 0; i < SSN.length; i++) {
						if (SSN[i].VariationIndicator && SSN[i].VariationIndicator.Value && SSN[i].VariationIndicator.Value.toLowerCase() == 'same') {
							socialSecurityNumber = _ssnFormat(SSN[i].Number);
						} else {
							if (SSN[i].VariationIndicator && SSN[i].VariationIndicator.Code && SSN[i].VariationIndicator.Code == '*') {
								OtherSocialSecurityNumbers = '*' + (_ssnFormat(SSN[i].Number));
							}
							OtherSocialSecurityNumbers = (_ssnFormat(SSN[i].Number));
						}
					}
				}
				if (ConsumerIdentity && ConsumerIdentity.length > 0) {
					consumerDOB = _dateFormat(ConsumerIdentity[0].Dob);
				}
				if (AddressInformation && AddressInformation.length > 0) {
					for (var i = 0; i < AddressInformation.length; i++) {
						var HomeOwnership = {
							'Code': null,
							'Value': null
						};
						var DwellingType = {
							'Code': null,
							'Value': null
						};
						var Origination = {
							'Code': null,
							'Value': null
						};
						var Addresses = {
							'DoesNotMatchInquiry': null,
							'StreetPrefix': null,
							'StreetName': null,
							'StreetSuffix': null,
							'City': null,
							'State': null,
							'Zip': null,
							'CensusGeoCode': null,
							'CountyCode': null,
							'FirstReportedDate': null,
							'LastUpdatedDate': null,
							'TimesReported': null,
							'LastReportingSubcode': null,
							'Origination': Origination,
							'DwellingType': DwellingType,
							'HomeOwnership': HomeOwnership,
							'AddressInquiry': null,
							'AddressesInquiryMessage': null
						};
						if (AddressInformation[i].Origination && AddressInformation[i].Origination.Code && AddressInformation[i].Origination.Code == '1') {
							Addresses.DoesNotMatchInquiry = '*';
						}
						if (AddressInformation[i].HomeOwnership && AddressInformation[i].HomeOwnership.Code) {
							HomeOwnership.Code = AddressInformation[i].HomeOwnership.Code;
						}
						if (AddressInformation[i].HomeOwnership && AddressInformation[i].HomeOwnership.Value) {
							HomeOwnership.Value = AddressInformation[i].HomeOwnership.Value;
						}
						if (AddressInformation[i].DwellingType && AddressInformation[i].DwellingType.Code) {
							DwellingType.Code = AddressInformation[i].DwellingType.Code;
						}
						if (AddressInformation[i].DwellingType && AddressInformation[i].DwellingType.Value) {
							DwellingType.Value = AddressInformation[i].DwellingType.Value;
						}
						if (AddressInformation[i].Origination && AddressInformation[i].Origination.Code) {
							Origination.Code = AddressInformation[i].Origination.Code;
						}
						if (AddressInformation[i].Origination && AddressInformation[i].Origination.Value) {
							Origination.Value = AddressInformation[i].Origination.Value;
						}
						Addresses.HomeOwnership = HomeOwnership;
						Addresses.DwellingType = DwellingType;
						Addresses.Origination = DwellingType;
						Addresses.StreetPrefix = AddressInformation[i].StreetPrefix;
						Addresses.StreetName = AddressInformation[i].StreetName;
						Addresses.StreetSuffix = AddressInformation[i].StreetSuffix;
						Addresses.City = AddressInformation[i].City;
						Addresses.State = AddressInformation[i].State;
						Addresses.Zip = AddressInformation[i].Zip;
						Addresses.CensusGeoCode = AddressInformation[i].CensusGeoCode;
						Addresses.CountyCode = AddressInformation[i].CountyCode;
						Addresses.FirstReportedDate = _dateFormat(AddressInformation[i].FirstReportedDate);
						Addresses.LastUpdatedDate = _dateFormat(AddressInformation[i].LastUpdatedDate);
						Addresses.TimesReported = AddressInformation[i].TimesReported;
						Addresses.LastReportingSubcode = AddressInformation[i].LastReportingSubcode;
						if (AddressInformation[i].Origination && AddressInformation[i].Origination.Code == '1') {
							Addresses.AddressInquiry = '*';
							if (AddressInformation[i].Origination && AddressInformation[i].Origination.Value) {
								Addresses.AddressesInquiryMessage = AddressInformation[i].Origination.Value;
							}
						}
						if (i == 0) {
							consumerAddress = Addresses;
						} else {
							consumerOtherAddresses.push(Addresses);
						}
					}
				}
				if (EmploymentInformation && EmploymentInformation.length > 0) {
					var Origination = {
						'Code': null,
						'Value': null
					};
					var employeeInfos = {
						'FirstReportedDate': null,
						'LastUpdatedDate': null,
						'Origination': Origination,
						'Name': null,
						'AddressFirstLine': null,
						'AddressSecondLine': null,
						'AddressExtraLine': null,
						'Zip': null,
						'EmployeeInquiry': null,
						'EmployeeInquiryMessage': null
					};
					for (var i = 0; i < EmploymentInformation.length; i++) {
						if (EmploymentInformation[i].Origination && EmploymentInformation[i].Origination.Code && EmploymentInformation[i].Origination.Code == '1') {
							employeeInfos.EmployeeInquiry = '*';
							if (EmploymentInformation[i].Origination && EmploymentInformation[i].Origination.Value) {
								employeeInfos.EmployeeInquiryMessage = EmploymentInformation[i].Origination.Value;
							}
						}
						if (EmploymentInformation[i].Origination) {
							if (EmploymentInformation[i].Origination.Code) {
								Origination.Code = EmploymentInformation[i].Origination.Code;
							}
							if (EmploymentInformation[i].Origination.Value) {
								Origination.Value = EmploymentInformation[i].Origination.Value;
							}
						}
						employeeInfos.Origination = Origination;
						employeeInfos.FirstReportedDate = _dateFormat(EmploymentInformation[i].FirstReportedDate);
						employeeInfos.LastUpdatedDate = _dateFormat(EmploymentInformation[i].LastUpdatedDate);
						employeeInfos.Name = EmploymentInformation[i].Name;
						employeeInfos.AddressFirstLine = EmploymentInformation[i].AddressFirstLine;
						employeeInfos.AddressSecondLine = EmploymentInformation[i].AddressSecondLine;
						employeeInfos.AddressExtraLine = EmploymentInformation[i].AddressExtraLine;
						employeeInfos.Zip = EmploymentInformation[i].Zip;
						if (i == 0) {
							employeeInfo = employeeInfos;
						} else {
							employeeAlternateInfo.push(employeeInfos);
						}
					}
				}
				if (RiskModel && RiskModel.length > 0) {
					for (var i = 0; i < RiskModel.length; i++) {
						if (RiskModel[i].ModelIndicator.Code.trim().toUpperCase() == '2') {
							RiskModel[i].ScoreRange = '1 to 999';
						} else if (RiskModel[i].ModelIndicator.Code.trim().toUpperCase() == 'V3') {
							RiskModel[i].ScoreRange = '300 to 850';
						} else if (RiskModel[i].ModelIndicator.Code.trim().toUpperCase() == 'BP') {
							RiskModel[i].ScoreRange = '1 to 1400';
						} else if (RiskModel[i].ModelIndicator.Code.trim().toUpperCase() == 'AF') {
							RiskModel[i].ScoreRange = '300 to 850';
						} else if (RiskModel[i].ModelIndicator.Code.trim().toUpperCase() == 'II') {
							RiskModel[i].ScoreRange = '0015 to 0990';
						} else if (RiskModel[i].ModelIndicator.Code.trim().toUpperCase() == 'D1') {
							RiskModel[i].ScoreRange = '0000 to 0101';
						}
					}
				}
				if (FraudServices && FraudServices.length > 0) {
					var FraudShieldSummary = {
						'Indicator': [],
						'InquiryAddress': [],
						'OnFileAddress': [],
					};
					for (var i = 0; i < FraudServices.length; i++) {
						var Type = {
							'Code': null
						};
						var Sic = {
							'Code': null
						};
						var SocialErrorCode = {
							'Code': null
						};
						var AddressErrorCode = {
							'Code': null
						};
						var FraudShieldSummaryData = {
							'Text': [],
							'SocialDate': null,
							'SocialCount': null,
							'AddressDate': null,
							'AddressCount': null,
							'SsnFirstPossibleIssuanceYear': null,
							'SsnLastPossibleIssuanceYear': null,
							'DateOfBirth': null,
							'DateOfDeath': null,
							'Indicator': null,
							'Type': Type,
							'Sic': Sic,
							'SocialErrorCode': SocialErrorCode,
							'AddressErrorCode': AddressErrorCode
						};
						if (FraudServices[i].Type && FraudServices[i].Type.Code) {
							Type.Code = FraudServices[i].Type.Code;
						}
						if (FraudServices[i].Sic && FraudServices[i].Sic.Code) {
							Sic.Code = FraudServices[i].Sic.Code;
						}
						if (FraudServices[i].SocialErrorCode && FraudServices[i].SocialErrorCode.Code) {
							SocialErrorCode.Code = FraudServices[i].SocialErrorCode.Code;
						}
						if (FraudServices[i].AddressErrorCode && FraudServices[i].AddressErrorCode.Code) {
							AddressErrorCode.Code = FraudServices[i].AddressErrorCode.Code;
						}
						FraudShieldSummaryData.Text = _getseprate(FraudServices[i].Text, '/');
						FraudShieldSummaryData.SocialDate = _dateFormat(FraudServices[i].SocialDate);
						FraudShieldSummaryData.SocialCount = FraudServices[i].SocialCount;
						FraudShieldSummaryData.AddressDate = _dateFormat(FraudServices[i].AddressDate);
						FraudShieldSummaryData.AddressCount = FraudServices[i].AddressCount;
						FraudShieldSummaryData.SsnFirstPossibleIssuanceYear = FraudServices[i].SsnFirstPossibleIssuanceYear;
						FraudShieldSummaryData.SsnLastPossibleIssuanceYear = FraudServices[i].SsnLastPossibleIssuanceYear;
						FraudShieldSummaryData.DateOfBirth = _dateFormat(FraudServices[i].DateOfBirth);
						FraudShieldSummaryData.DateOfDeath = _dateFormat(FraudServices[i].DateOfDeath);
						FraudShieldSummaryData.Indicator = FraudServices[i].Indicator;
						FraudShieldSummaryData.Type = Type;
						FraudShieldSummaryData.Sic = Sic;
						FraudShieldSummaryData.SocialErrorCode = SocialErrorCode;
						FraudShieldSummaryData.AddressErrorCode = AddressErrorCode;
						if (FraudServices[i].Type && FraudServices[i].Type.Code) {
							if (FraudServices[i].Type.Code == '1') {
								FraudShieldSummary.InquiryAddress.push(FraudShieldSummaryData);
							}
							if (FraudServices[i].Type.Code == '2') {
								FraudShieldSummary.Indicator.push(FraudShieldSummaryData);
							}
							if (FraudServices[i].Type.Code == '4') {
								FraudShieldSummary.OnFileAddress.push(FraudShieldSummaryData);
							}
						}
					}
					if (Messages && Messages.DateReported) {
						if (ProductCreditProfile.Statement && ProductCreditProfile.Statement.DateReported) {
							Messages.DateReported = _dateFormat(ProductCreditProfile.Statement.DateReported);
						}
					}
					if (PublicRecord && PublicRecord.length > 0) {
						for (var i = 0; i < PublicRecord.length; i++) {
							var StatusDate = _dateFormat(ProductCreditProfile.PublicRecord[i].StatusDate);
							var Amount = _moneyWithoutFraction(ProductCreditProfile.PublicRecord[i].Amount);
							var FilingDate = _dateFormat(ProductCreditProfile.PublicRecord[i].FilingDate);
							var AssetAmount = 0;
							if (ProductCreditProfile.PublicRecord[i].Bankruptcy && ProductCreditProfile.PublicRecord[i].Bankruptcy.AssetAmount) {
								AssetAmount = _moneyWithoutFraction(ProductCreditProfile.PublicRecord[i].Bankruptcy.AssetAmount);
								ProductCreditProfile.PublicRecord[i].Bankruptcy.AssetAmount = AssetAmount;
							}
							var LiabilitiesAmount = 0;
							if (ProductCreditProfile.PublicRecord[i].Bankruptcy && ProductCreditProfile.PublicRecord[i].Bankruptcy.LiabilitiesAmount) {
								LiabilitiesAmount = _moneyWithoutFraction(ProductCreditProfile.PublicRecord[i].Bankruptcy.LiabilitiesAmount);
								ProductCreditProfile.PublicRecord[i].Bankruptcy.LiabilitiesAmount = LiabilitiesAmount;
							}
							PublicRecord[i].StatusDate = StatusDate;
							PublicRecord[i].FilingDate = FilingDate;
							PublicRecord[i].Amount = Amount;
						}
					}
					if (TradeLine && TradeLine.length > 0) {
						for (var i = 0; i < TradeLine.length; i++) {
							TradeLine[i].PaymentProfileArray = {};
							var LastPaymentDate = _dateFormat(ProductCreditProfile.TradeLine[i].LastPaymentDate);
							var BalanceAmount = '$0';
							var OpenDate = _dateFormat(ProductCreditProfile.TradeLine[i].OpenDate);
							var StatusDate = _dateFormat(ProductCreditProfile.TradeLine[i].StatusDate);
							var BalanceDate = _dateFormat(ProductCreditProfile.TradeLine[i].BalanceDate);
							var InitialPaymentLevelDate = '';
							var BalanceAmount = '';
							if (ProductCreditProfile.TradeLine[i].BalanceAmount && ProductCreditProfile.TradeLine[i].BalanceAmount.length > 0) {
								BalanceAmount = _moneyWithoutFraction(ProductCreditProfile.TradeLine[i].BalanceAmount);
							}
							if (ProductCreditProfile.TradeLine[i].EnhancedPaymentData && ProductCreditProfile.TradeLine[i].EnhancedPaymentData.InitialPaymentLevelDate) {
								InitialPaymentLevelDate = _dateFormat(ProductCreditProfile.TradeLine[i].EnhancedPaymentData.InitialPaymentLevelDate);
							}
							var Amount = '';
							if (ProductCreditProfile.TradeLine[i].Amount.length > 0 && ProductCreditProfile.TradeLine[i].Amount[0].Value) {
								Amount = _moneyWithoutFraction(ProductCreditProfile.TradeLine[i].Amount[0].Value);
							}
							var AmountPastDue = _moneyWithoutFraction(ProductCreditProfile.TradeLine[i].AmountPastDue);
							var OriginalAmount = '';
							if (ProductCreditProfile.TradeLine[i].Amount && ProductCreditProfile.TradeLine[i].Amount.length > 0) {
								for (var p = 0; p < ProductCreditProfile.TradeLine[i].Amount.length; p++) {
									if(ProductCreditProfile.TradeLine[i].Amount[p].Qualifier && ProductCreditProfile.TradeLine[i].Amount[p].Qualifier.Code=='O' && ProductCreditProfile.TradeLine[i].Amount[p].Qualifier.Value=='Original')
									{
                                        OriginalAmount = _moneyWithoutFraction(ProductCreditProfile.TradeLine[i].Amount[p].Value)
									}
							}
							}
							var HighBalance = '';
							if (ProductCreditProfile.TradeLine[i].Amount && ProductCreditProfile.TradeLine[i].Amount.length > 0) {
								for (var z = 0; z < ProductCreditProfile.TradeLine[i].Amount.length; z++) {
									if(ProductCreditProfile.TradeLine[i].Amount[z].Qualifier && ProductCreditProfile.TradeLine[i].Amount[z].Qualifier.Code=='H' && ProductCreditProfile.TradeLine[i].Amount[z].Qualifier.Value=='High balance')
									{
                                        HighBalance = _moneyWithoutFraction(ProductCreditProfile.TradeLine[i].Amount[z].Value)
									}
							}
							}
							var ScheduledPayment  = '';
							if (ProductCreditProfile.TradeLine[i].MonthlyPaymentType && ProductCreditProfile.TradeLine[i].MonthlyPaymentType.Code == 'S' && ProductCreditProfile.TradeLine[i].MonthlyPaymentType.Value == 'Scheduled Term') {
								ScheduledPayment = _moneyWithoutFraction(ProductCreditProfile.TradeLine[i].MonthlyPaymentAmount);
							}
							TradeLine[i].OpenDate = OpenDate;
							TradeLine[i].BalanceAmount = BalanceAmount;
							TradeLine[i].LastPaymentDate = LastPaymentDate;
							TradeLine[i].StatusDate = StatusDate;
							TradeLine[i].BalanceDate = BalanceDate;
							TradeLine[i].EnhancedPaymentData.InitialPaymentLevelDate = InitialPaymentLevelDate;
							TradeLine[i].Amount[0].Value = Amount;
							TradeLine[i].AmountPastDue = AmountPastDue;
							TradeLine[i].HighBalance = HighBalance;
							TradeLine[i].OriginalAmount = OriginalAmount;
							TradeLine[i].ScheduledPayment = ScheduledPayment;
							var Payment = {};
							Payment.monthValue0 = [];
							Payment.monthValue1 = [];
							Payment.monthValue2 = [];
							if (ProductCreditProfile.TradeLine[i].BalanceDate && ProductCreditProfile.TradeLine[i].PaymentProfile) {
								var startmonth = ProductCreditProfile.TradeLine[i].BalanceDate.substring(0, 2);
								var year = ProductCreditProfile.TradeLine[i].BalanceDate.slice(-4);
								var Profile = ProductCreditProfile.TradeLine[i].PaymentProfile;
								var index = 0;
								for (var j = 0; j < 3; j++) {
									var monthstart = startmonth;
									var monthValue = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '];
									monthValue[0] = year - j;
									for (var k = 12; k >= 1; k--) {
										if (k <= monthstart) {
											monthValue[k] = Profile.charAt(index);
											index++;
										} else {
											monthValue[k] = ' ';
										}
									}
									Payment['monthValue' + j] = monthValue;
									startmonth = 12;
								}
							}
							TradeLine[i].PaymentProfileArray = Payment;
						}
					}
					if (Inquiry && Inquiry.length > 0) {
						for (var i = 0; i < Inquiry.length; i++) {
							var InquiryDate = _dateFormat(ProductCreditProfile.Inquiry[i].Date);
							var Amount = _moneyWithoutFraction(ProductCreditProfile.Inquiry[i].Amount);
							Inquiry[i].Date = InquiryDate;
							Inquiry[i].Amount = Amount;
						}
					}
					if (ProfileSummary) {
						ProfileSummary.InstallmentBalance = _moneyWithoutFraction(ProfileSummary.InstallmentBalance);
						ProfileSummary.RevolvingBalance = _moneyWithoutFraction(ProfileSummary.RevolvingBalance);
						ProfileSummary.PastDueAmount = _moneyWithoutFraction(ProfileSummary.PastDueAmount);
						ProfileSummary.MonthlyPayment = _moneyWithoutFraction(ProfileSummary.MonthlyPayment);
						ProfileSummary.RealEstateBalance = _moneyWithoutFraction(ProfileSummary.RealEstateBalance);
						ProfileSummary.RealEstatePayment = _moneyWithoutFraction(ProfileSummary.RealEstatePayment);
						ProfileSummary.OldestTradeOpenDate = _dateFormat(ProfileSummary.OldestTradeOpenDate);
					}
				}
			}
		}
		return PDFReportAttributes = {
			'Header': Header,
			'ConsumerIdentity': ConsumerIdentity,
			'SSN': SSN,
			'AddressInformation': AddressInformation,
			'EmploymentInformation': EmploymentInformation,
			'Messages': Messages,
			'FraudServices': FraudServices,
			'RiskModel': RiskModel,
			'PublicRecord': PublicRecord,
			'Inquiry': Inquiry,
			'TradeLine': TradeLine,
			'ConsumerAssistanceReferralAddress': ConsumerAssistanceReferralAddress,
			'ProfileSummary': ProfileSummary,
			'reportDate': reportDate,
			'reportTime': reportTime,
			'consumerName': consumerName,
			'socialSecurityNumber': socialSecurityNumber,
			'consumerOtherNames': consumerOtherNames,
			'OtherSocialSecurityNumbers': OtherSocialSecurityNumbers,
			'consumerDOB': consumerDOB,
			'consumerAddress': consumerAddress,
			'consumerOtherAddresses': consumerOtherAddresses,
			'employeeInfo': employeeInfo,
			'employeeAlternateInfo': employeeAlternateInfo,
			'FraudShieldSummary': FraudShieldSummary,
			'Demographics': Demographics
		}
	}
}