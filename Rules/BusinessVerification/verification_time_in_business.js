function TimeInBusinessVerification(payload) {
    var result = 'Passed';
    var TIB = 0;
    if (payload) {
        var input = payload.application[0];
        if (input) {
            var MinTimeInBusiness = 180;
            TIB = GetDaysDiff(new Date(input.businessStartDate.Time), new Date());
            if (TIB < MinTimeInBusiness) {
                result = 'Failed';
            }
        }
    }
    var Data = {
        'TimeInBusiness': TIB
    };
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
    function GetDaysDiff(d1, d2) {
        var _MS_PER_DAY = 1000 * 60 * 60 * 24;
        var utc1 = Date.UTC(d1.getFullYear(), d1.getMonth(), d1.getDate());
        var utc2 = Date.UTC(d2.getFullYear(), d2.getMonth(), d2.getDate());

        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
}