function getconsumerlist(payload) {
    var consumerObj = {
        'OwnerId': null,
        'FirstName': null,
        'LastName': null,
        'Dob': null,
        'Phone': null,
        'Email': null,
        'Ssn': null,
        'Fein': null,
        'BusinessName': null,
        'BusinessTaxId': null,
        'BusinessPhone': null,
        'Ein': null
    };
    var consumerList = [];
    if (typeof (payload) != 'undefined') {
        var application = payload.eventData.Application;
        var applicant = payload.eventData.Applicant;
        if (application.PrimaryPhone != null) {
            consumerObj.BusinessPhone = application.PrimaryPhone.Phone;
        }
        consumerObj.BusinessName = applicant.LegalBusinessName;
        consumerObj.ein = applicant.EIN;
        if (applicant) {
            consumerObj.BusinessTaxId = applicant.BusinessTaxID;
            if (applicant.Owners != null && applicant.Owners.length > 0) {
                for (var index = 0; index < applicant.Owners.length; index++) {
                    var owner = applicant.Owners[index];
                    var clonedConsumerObj = {
                        'OwnerId': null,
                        'FirstName': null,
                        'LastName': null,
                        'Dob': null,
                        'Phone': null,
                        'Email': null,
                        'Ssn': null,
                        'Fein': null,
                        'BusinessName': consumerObj.BusinessName,
                        'BusinessTaxId': consumerObj.BusinessTaxId,
                        'BusinessPhone': consumerObj.BusinessPhone,
                        'Ein': consumerObj.ein
                    };
                    clonedConsumerObj.OwnerId = owner.OwnerId;
                    clonedConsumerObj.FirstName = owner.FirstName;
                    clonedConsumerObj.LastName = owner.LastName;
                    clonedConsumerObj.Ssn = owner.SSN;
                    clonedConsumerObj.Dob = owner.DOB;
                    if (owner.PhoneNumbers[0] && owner.PhoneNumbers[0].Phone) {
                        clonedConsumerObj.Phone = owner.PhoneNumbers[0].Phone;
                    }
                    if (owner.EmailAddresses[0] && owner.EmailAddresses[0].Email) {
                        clonedConsumerObj.Email = owner.EmailAddresses[0].Email;
                    }
                    consumerList.push(clonedConsumerObj);
                }
            }
        }
    }
    return {
        'result': 'Passed',
        'detail': null,
        'data': { 'Consumers': consumerList },
        'rejectcode': '',
        'exception': []
    };
}