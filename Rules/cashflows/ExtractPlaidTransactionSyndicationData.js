function ExtractPlaidTransactionSyndicationData(input) {

	var errorData = [];
	try {
		var lstTransactions = [];
		var data = {};
		if (input != null) {

			if (input.Transactions != null && input.Transactions.length > 0) {
				input.Transactions.forEach(function (transaction) {
					var objTransaction = {
						'ProviderAccountId': '',
						'AccountId': '',
						'Amount': 0,
						'Categories': '',
						'CategoryId': '',
						'Description': '',
						'Meta': '',
						'Pending': '',
						'TransactionDate': ''
					};

					objTransaction.ProviderAccountId = transaction.Account;
					objTransaction.Amount = transaction.Amount;
					objTransaction.TransactionDate = transaction.Date;
					objTransaction.Description = transaction.Name;
					objTransaction.Pending = transaction.Pending;
					objTransaction.Categories = transaction.Category;
					objTransaction.CategoryId = transaction.CategoryId;
					lstTransactions.push(objTransaction);
				});
			}

			data = {
				Transactions: lstTransactions
			};
		}
	} catch (e) {
		return null;
	}
	return data;
};