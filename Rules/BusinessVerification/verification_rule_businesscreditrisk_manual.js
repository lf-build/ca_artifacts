function verification_rule_businesscreditrisk_manual(payload) {
	var result = 'Failed';
	var Data = {};
	if (payload && payload.businessCreditRiskVerificationManualData) {
		Data = payload.businessCreditRiskVerificationManualData[0];
		if (Data && Data.pass == true) {
			result = 'Passed';
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
