function get_grade_lookup(payload) {

    if (typeof (payload) != 'undefined') {
		
	/*put validation by lookuo by */
		
     var riskgrade = payload.riskGrade_Attribute[0];
     var saleRate = payload.saleRate_Attribute[0];
	 var garedList = [];
	  for (var i = 0; i < saleRate.data.length; i++) {
		  
		  var currentData= saleRate.data[i];
		  if(currentData.Grade == riskgrade.value)
		  {
			  garedList.push(currentData);
		  }
		  
	  }
	 
        var records = {
            'gradeList': garedList
        };
        return {
            'result': 'Passed',
            'detail': null,
            'data': records,
            'rejectcode': '',
            'exception': []
        };
    }
    return {
        'result': 'Failed',
        'detail': null,
        'data': '',
        'rejectcode': '',
        'exception': []
    };
}