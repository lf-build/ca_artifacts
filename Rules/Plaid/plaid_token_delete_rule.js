function plaid_token_delete_rule(input) {
	var eligibleStatusCode = ['400.20', '600.10', '600.20', '600.30', '600.40'];
	var NoOfDays = '0';
	var filterApplicationsResult = [];
	var applicationFiltersService = this.call('applicationFilters');
	return applicationFiltersService.get(input).then(function (inputResponse) {
		if (inputResponse && inputResponse.length > 0) {
			for (var i = 0; i < inputResponse.length; i++) {
				if (eligibleStatusCode.indexOf(inputResponse[i].statusCode) > 0) {
					if (inputResponse[i].applicationDate.time) {
						var appDate = new Date(inputResponse[i].applicationDate.time);
						var todaysDate = new Date();
						var dayDiff = datedifference('D', appDate, todaysDate);
						if (dayDiff >= NoOfDays) {
							var filterapplication = {
								EntiyId: '',
								AccessToken: '',
								ItemCreateOn: '',
								ApplicationSubmitedDate: '',
								Status: '',
								StatusCode: '',
								StatusDate: ''
							};
							filterapplication.EntiyId = inputResponse[i].applicationNumber;
							filterapplication.ApplicationSubmitedDate = inputResponse[i].applicationDate;
							filterapplication.Status = inputResponse[i].statusName;
							filterapplication.StatusCode = inputResponse[i].statusCode;
							filterapplication.StatusDate = inputResponse[i].statusDate;
							filterApplicationsResult.push(filterapplication);
						}
					}
				}
			}
			return {
				'filterApplicationsResult': filterApplicationsResult
			};
		}
	});
	return null;
	function datedifference(datepart, fromdate, todate) {
		datepart = datepart.toLowerCase();
		var diff = todate - fromdate;
		var divideBy = {
			w: 604800000,
			d: 86400000,
			h: 3600000,
			n: 60000,
			s: 1000
		};
		return Math.floor(diff / divideBy[datepart]);
	}
}