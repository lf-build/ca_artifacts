function data_attribute_productdata(payload) {
	if(payload)
	var Data = {
		'product': payload.eventData.Product
	};
	return {
		'result': true,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
