function generateYodleeCashflow(input) {
    var cashFlowInput = input.Event.Data.Response;
    validateInput(cashFlowInput);
    var Months = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
    ];
    var YodleeTransactionBaseType = {
        CREDIT: 'CREDIT',
        DEBIT: 'DEBIT'
    };
    var YodleeTransactionCategories = {
        Loans: 17,
        Payroll: 18,
        NSF: 19
    };
    var YodleeTransactionSubCategories = {
        NSF_FEES: 'NSF_FEES'
    };
    var CashFlowViewModel = {
        MonthlyCashFlows: [],
        TransactionSummary: TransactionSummaryViewModel
    };
    var TransactionSummaryViewModel = {
        CountOfMonthlyStatement: 0,
        StartDate: '',
        EndDate: '',
        AverageDeposit: 0,
        AvergagWithdrawl: 0,
        AverageDailyBalance: 0,
        NumberOfNagatvieBalance: 0,
        NumberOfNSF: 0,
        NSFAmount: 0,
        LoanPaymentAmount: 0,
        NumberOfLoanPayment: 0,
        PayrollAmount: 0,
        NumberOfPayroll: 0
    };
    var MonthlyCashFlowViewModel = {
        Name: '',
        Year: 0,
        BeginingBalance: 0,
        EndingBalance: 0,
        DepositCount: 0,
        WithdrawlCount: 0,
        TotalDepositAmount: 0,
        TotalWithDrawlAmount: 0,
        AverageDailyBalance: 0,
        FirstTransactionDate: '',
        CounterDeposit: 0,
        MinDepositAmount: 0,
        MaxDepositAmount: 0,
        MinWithdrawlAmount: 0,
        MaxWithdrawlAmount: 0,
        MinAccountBalance: 0,
        NumberOfNSF: 0,
        NSFAmount: 0,
        LoanPaymentAmount: 0,
        NumberOfLoanPayment: 0,
        PayrollAmount: 0,
        NumberOfPayroll: 0,
        NumberOfNegativeBalance: 0,
    };
    var AccountsCashFlowVm = {
        AccountId: '',
        CashFlow: CashFlowViewModel
    };
    var AllAccountsCashFlowList = {
        FinalCashFlow: []
    };
    var accountIds = [];
    var allAccountsTransactions = [];
    allAccountsTransactions = cashFlowInput.Transactions;
    allAccountsTransactions.forEach(function (trans) {
        if (accountIds.indexOf(trans.AccountId) < 0) {
            accountIds.push(trans.AccountId);
        }
    });
    if (accountIds != undefined && accountIds.length > 0) {
        accountIds.forEach(function (accountId) {
            var currentAccountTransactions = allAccountsTransactions.filter(function (transactionData) {
                return transactionData.AccountId === accountId;
            });
            var cashFlowResult = CashFlowViewModel;
            cashFlowResult.MonthlyCashFlows = [];
            if (currentAccountTransactions.length > 0) {
                currentAccountTransactions = currentAccountTransactions.sort(function (a, b) {
                    var transDateA = new Date(a.PostDate);
                    var transDateB = new Date(b.PostDate);
                    if (transDateA < transDateB) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                    ;
                });
                var totalTransactions = currentAccountTransactions.length;
                var maxTransactionDate = new Date(currentAccountTransactions[0].PostDate);
                var minTransactionDate = new Date(currentAccountTransactions[totalTransactions - 1].PostDate);
                var currentMonth = maxTransactionDate.getMonth();
                var currentYear = maxTransactionDate.getFullYear();
                var endingBalanceofTheDay = currentAccountTransactions[0].RunningBalance.Amount;
                var beginingBalance = endingBalanceofTheDay;
                var isFirstTransaction = true;
                var isLastMonth = true;
                var dateOfLastTransaction;
                while (currentAccountTransactions.length > 0) {
                    var currentMonthTransactions = currentAccountTransactions.filter(function (item) {
                        var transDate = new Date(item.PostDate);
                        if (transDate.getMonth() === currentMonth && transDate.getFullYear() === currentYear) {
                            return item;
                        }
                    });
                    if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {
                        var currentMonthCashFlow = MonthlyCashFlowViewModel;
                        currentMonthCashFlow.EndingBalance = beginingBalance;
                        currentMonthCashFlow.Year = currentYear;
                        currentMonthCashFlow.Name = Months[currentMonth];
                        var monthlyDailyBalance = new Array(31);
                        currentMonthTransactions.forEach(function (transaction) {
                            if (transaction != undefined) {
                                if (beginingBalance < 0) {
                                    currentMonthCashFlow.NumberOfNegativeBalance += 1;
                                }
                                var currentTransactionDate = new Date(transaction.PostDate);
                                var day = currentTransactionDate.getDate();
                                var calculateDailyEnding = isFirstTransaction ? isFirstTransaction :
                                    (dateOfLastTransaction.getDate() != currentTransactionDate.getDate());
                                if (calculateDailyEnding) {
                                    endingBalanceofTheDay = beginingBalance;
                                    monthlyDailyBalance[day - 1] = endingBalanceofTheDay;
                                }
                                dateOfLastTransaction = currentTransactionDate;
                                var transactionAmount = (transaction.Amount == undefined || transaction.Amount == null
                                    || transaction.Amount.Amount == undefined || transaction.Amount.Amount == null) ? 0 : transaction.Amount.Amount;
                                if (transaction.BaseType.toLowerCase() === YodleeTransactionBaseType.CREDIT.toLowerCase()) {
                                    beginingBalance -= transactionAmount;
                                    currentMonthCashFlow.TotalDepositAmount += transactionAmount;
                                    if (currentMonthCashFlow.MaxDepositAmount == 0 || currentMonthCashFlow.MaxDepositAmount < transactionAmount) {
                                        currentMonthCashFlow.MaxDepositAmount = transactionAmount;
                                    }
                                    if (currentMonthCashFlow.MinDepositAmount == 0 || currentMonthCashFlow.MinDepositAmount > transactionAmount) {
                                        currentMonthCashFlow.MinDepositAmount = transactionAmount;
                                    }
                                    currentMonthCashFlow.DepositCount++;
                                }
                                else {
                                    beginingBalance += transactionAmount;
                                    currentMonthCashFlow.TotalWithDrawlAmount += transactionAmount;
                                    if (currentMonthCashFlow.MaxWithdrawlAmount == 0 || currentMonthCashFlow.MaxWithdrawlAmount < transactionAmount) {
                                        currentMonthCashFlow.MaxWithdrawlAmount = transactionAmount;
                                    }
                                    if (currentMonthCashFlow.MinWithdrawlAmount == 0 || currentMonthCashFlow.MinWithdrawlAmount > transactionAmount) {
                                        currentMonthCashFlow.MinWithdrawlAmount = transactionAmount;
                                    }
                                    currentMonthCashFlow.WithdrawlCount++;
                                }
                                if (transaction.SubType != null &&
                                    transaction.SubType.toLowerCase() === YodleeTransactionSubCategories.NSF_FEES.toLowerCase()) {
                                    currentMonthCashFlow.NSFAmount += transactionAmount;
                                    currentMonthCashFlow.NumberOfNSF += 1;
                                }
                                switch (transaction.CategoryId) {
                                    case YodleeTransactionCategories.Loans:
                                        currentMonthCashFlow.LoanPaymentAmount += transactionAmount;
                                        currentMonthCashFlow.NumberOfLoanPayment += 1;
                                        break;
                                }
                                isFirstTransaction = false;
                                monthlyDailyBalance[day - 2] = beginingBalance;
                                var index = currentAccountTransactions.indexOf(transaction, 0);
                                if (index > -1) {
                                    currentAccountTransactions.splice(index, 1);
                                }
                            }
                        });
                        currentMonthCashFlow.FirstTransactionDate = customDate(dateOfLastTransaction);
                        currentMonthCashFlow.BeginingBalance = beginingBalance;
                        var totalDaysInmonth;
                        if (isLastMonth) {
                            totalDaysInmonth = maxTransactionDate.getDate();
                        }
                        else {
                            totalDaysInmonth = getDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear());
                        }
                        currentMonthCashFlow.AverageDailyBalance = calculateAvgDailyBalance(monthlyDailyBalance, totalDaysInmonth);
                        cashFlowResult.MonthlyCashFlows.push(currentMonthCashFlow);
                        isLastMonth = false;
                    }
                    currentMonth -= 1;
                    if (currentMonth < 0) {
                        currentYear -= 1;
                        currentMonth = 11;
                    }
                }
                cashFlowResult.TransactionSummary = TransactionSummaryViewModel;
                cashFlowResult.TransactionSummary.CountOfMonthlyStatement = cashFlowResult.MonthlyCashFlows.length;
                cashFlowResult.TransactionSummary.StartDate = customDate(minTransactionDate);
                cashFlowResult.TransactionSummary.EndDate = customDate(maxTransactionDate);
                var totalDiposit = 0;
                var totalWithdrl = 0;
                cashFlowResult.MonthlyCashFlows.forEach(function (cashflow) {
                    totalDiposit += cashflow.TotalDepositAmount;
                    totalWithdrl += cashflow.TotalWithDrawlAmount;
                    cashFlowResult.TransactionSummary.NumberOfNagatvieBalance += cashflow.NumberOfNegativeBalance;
                    cashFlowResult.TransactionSummary.LoanPaymentAmount += cashflow.LoanPaymentAmount;
                    cashFlowResult.TransactionSummary.NumberOfLoanPayment += cashflow.NumberOfLoanPayment;
                    cashFlowResult.TransactionSummary.PayrollAmount += cashflow.PayrollAmount;
                    cashFlowResult.TransactionSummary.NumberOfPayroll += cashflow.NumberOfPayroll;
                    cashFlowResult.TransactionSummary.NSFAmount += cashflow.NSFAmount;
                    cashFlowResult.TransactionSummary.NumberOfNSF += cashflow.NumberOfNSF;
                });
                cashFlowResult.TransactionSummary.AverageDeposit = customRound(totalDiposit / cashFlowResult.TransactionSummary.CountOfMonthlyStatement, 2);
                cashFlowResult.TransactionSummary.AvergagWithdrawl = customRound(totalWithdrl / cashFlowResult.TransactionSummary.CountOfMonthlyStatement, 2);
                var currntAccountIdCashFlow = AccountsCashFlowVm;
                currntAccountIdCashFlow.AccountId = accountId;
                currntAccountIdCashFlow.CashFlow = cashFlowResult;
                AllAccountsCashFlowList.FinalCashFlow.push(currntAccountIdCashFlow);
            }
        });
        return AllAccountsCashFlowList;
    }
    ;
}
;
function calculateAvgDailyBalance(dailyBalanceList, numberOfDaysInMonth) {
    var totalMonthlyDailyBalance = 0;
    var emptyCount = 1;
    var index = 0;
    while (index <= (numberOfDaysInMonth - 1)) {
        if (dailyBalanceList[index] == undefined) {
            emptyCount++;
        }
        else {
            totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
            emptyCount = 1;
        }
        index++;
    }
    return customRound(totalMonthlyDailyBalance / numberOfDaysInMonth, 2);
}
;
function getDaysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate();
}
;
function pad(s) { return (s < 10) ? '0' + s : s; }
;
function customDate(d) {
    return [pad(d.getMonth() + 1), pad(d.getDate()), d.getFullYear()].join('/');
}
;
function customRound(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}
;
function validateInput(cashFlowInput) {
    if (cashFlowInput == null) {
        throw new Error('cashFlowInput can not be null');
    }
    if (cashFlowInput.Transactions == undefined || cashFlowInput.Transactions == null) {
        throw new Error('Transactions can not be null or undefined');
    }
    cashFlowInput.Transactions.forEach(function (transaction) {
        if (transaction.PostDate == undefined || transaction.PostDate == null || transaction.PostDate == '') {
            throw new Error('All transaction should have PostDate');
        }
        if (transaction.BaseType == undefined || transaction.BaseType == null || transaction.BaseType == '') {
            throw new Error('All transaction should have BaseType');
        }
    });
}
var input = {
    "EntityType": "application",
    "EntityId": "LCUSA-0000001",
    "Event": {
        "Id": "1",
        "Name": "",
        "Source": "",
        "TenantId": "",
        "Data": {
            "Response": {
                "Transactions": [{
                        "Container": "creditCard",
                        "Id": 15283748,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2014-01-17",
                        "TransactionDate": null,
                        "postDate": "2014-01-17",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283750,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-16",
                        "TransactionDate": null,
                        "postDate": "2014-01-16",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283752,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2014-01-15",
                        "TransactionDate": null,
                        "postDate": "2014-01-15",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283753,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2014-01-14",
                        "TransactionDate": null,
                        "postDate": "2014-01-14",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283755,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-13",
                        "TransactionDate": null,
                        "postDate": "2014-01-13",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283757,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-12",
                        "TransactionDate": null,
                        "postDate": "2014-01-12",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283759,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2014-01-11",
                        "TransactionDate": null,
                        "postDate": "2014-01-11",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283761,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2014-01-10",
                        "TransactionDate": null,
                        "postDate": "2014-01-10",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283763,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-09",
                        "TransactionDate": null,
                        "postDate": "2014-01-09",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283764,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-08",
                        "TransactionDate": null,
                        "postDate": "2014-01-08",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283767,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2014-01-07",
                        "TransactionDate": null,
                        "postDate": "2014-01-07",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283768,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2014-01-06",
                        "TransactionDate": null,
                        "postDate": "2014-01-06",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283770,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-05",
                        "TransactionDate": null,
                        "postDate": "2014-01-05",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283772,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2014-01-04",
                        "TransactionDate": null,
                        "postDate": "2014-01-04",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283775,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2014-01-03",
                        "TransactionDate": null,
                        "postDate": "2014-01-03",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283776,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-02",
                        "TransactionDate": null,
                        "postDate": "2014-01-02",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283778,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2014-01-01",
                        "TransactionDate": null,
                        "postDate": "2014-01-01",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283779,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-12-30",
                        "TransactionDate": null,
                        "postDate": "2013-12-30",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283780,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-12-29",
                        "TransactionDate": null,
                        "postDate": "2013-12-29",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283782,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-28",
                        "TransactionDate": null,
                        "postDate": "2013-12-28",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283785,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-27",
                        "TransactionDate": null,
                        "postDate": "2013-12-27",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283789,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-12-26",
                        "TransactionDate": null,
                        "postDate": "2013-12-26",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283790,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-12-25",
                        "TransactionDate": null,
                        "postDate": "2013-12-25",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283792,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-24",
                        "TransactionDate": null,
                        "postDate": "2013-12-24",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283793,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-12-23",
                        "TransactionDate": null,
                        "postDate": "2013-12-23",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283794,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-12-22",
                        "TransactionDate": null,
                        "postDate": "2013-12-22",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283796,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-21",
                        "TransactionDate": null,
                        "postDate": "2013-12-21",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283797,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-20",
                        "TransactionDate": null,
                        "postDate": "2013-12-20",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283799,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-12-19",
                        "TransactionDate": null,
                        "postDate": "2013-12-19",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283850,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-12-18",
                        "TransactionDate": null,
                        "postDate": "2013-12-18",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283851,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-16",
                        "TransactionDate": null,
                        "postDate": "2013-12-16",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283853,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-15",
                        "TransactionDate": null,
                        "postDate": "2013-12-15",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283856,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-12-14",
                        "TransactionDate": null,
                        "postDate": "2013-12-14",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283858,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-12-13",
                        "TransactionDate": null,
                        "postDate": "2013-12-13",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283860,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-12",
                        "TransactionDate": null,
                        "postDate": "2013-12-12",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283861,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-12-11",
                        "TransactionDate": null,
                        "postDate": "2013-12-11",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283863,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-12-10",
                        "TransactionDate": null,
                        "postDate": "2013-12-10",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283864,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-09",
                        "TransactionDate": null,
                        "postDate": "2013-12-09",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283866,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-08",
                        "TransactionDate": null,
                        "postDate": "2013-12-08",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283868,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-12-07",
                        "TransactionDate": null,
                        "postDate": "2013-12-07",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283869,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-12-06",
                        "TransactionDate": null,
                        "postDate": "2013-12-06",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283870,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-05",
                        "TransactionDate": null,
                        "postDate": "2013-12-05",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283872,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-04",
                        "TransactionDate": null,
                        "postDate": "2013-12-04",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283874,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-12-03",
                        "TransactionDate": null,
                        "postDate": "2013-12-03",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283876,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-12-02",
                        "TransactionDate": null,
                        "postDate": "2013-12-02",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283877,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-12-01",
                        "TransactionDate": null,
                        "postDate": "2013-12-01",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283881,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-11-30",
                        "TransactionDate": null,
                        "postDate": "2013-11-30",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283879,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-11-30",
                        "TransactionDate": null,
                        "postDate": "2013-11-30",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283882,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-29",
                        "TransactionDate": null,
                        "postDate": "2013-11-29",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283885,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-28",
                        "TransactionDate": null,
                        "postDate": "2013-11-28",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283887,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-11-27",
                        "TransactionDate": null,
                        "postDate": "2013-11-27",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283890,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-11-26",
                        "TransactionDate": null,
                        "postDate": "2013-11-26",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283891,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-25",
                        "TransactionDate": null,
                        "postDate": "2013-11-25",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283893,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-24",
                        "TransactionDate": null,
                        "postDate": "2013-11-24",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283896,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-11-23",
                        "TransactionDate": null,
                        "postDate": "2013-11-23",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283897,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-11-22",
                        "TransactionDate": null,
                        "postDate": "2013-11-22",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283899,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-21",
                        "TransactionDate": null,
                        "postDate": "2013-11-21",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283951,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-11-20",
                        "TransactionDate": null,
                        "postDate": "2013-11-20",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283953,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-11-19",
                        "TransactionDate": null,
                        "postDate": "2013-11-19",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283954,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-18",
                        "TransactionDate": null,
                        "postDate": "2013-11-18",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283956,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-16",
                        "TransactionDate": null,
                        "postDate": "2013-11-16",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283957,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-11-15",
                        "TransactionDate": null,
                        "postDate": "2013-11-15",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283959,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-11-14",
                        "TransactionDate": null,
                        "postDate": "2013-11-14",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283960,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-13",
                        "TransactionDate": null,
                        "postDate": "2013-11-13",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283962,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-12",
                        "TransactionDate": null,
                        "postDate": "2013-11-12",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283964,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-11-11",
                        "TransactionDate": null,
                        "postDate": "2013-11-11",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283966,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-11-10",
                        "TransactionDate": null,
                        "postDate": "2013-11-10",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283968,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-09",
                        "TransactionDate": null,
                        "postDate": "2013-11-09",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283971,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-11-08",
                        "TransactionDate": null,
                        "postDate": "2013-11-08",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283973,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-11-07",
                        "TransactionDate": null,
                        "postDate": "2013-11-07",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283975,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-06",
                        "TransactionDate": null,
                        "postDate": "2013-11-06",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283977,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-05",
                        "TransactionDate": null,
                        "postDate": "2013-11-05",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283979,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-11-04",
                        "TransactionDate": null,
                        "postDate": "2013-11-04",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283981,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-11-03",
                        "TransactionDate": null,
                        "postDate": "2013-11-03",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283983,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-02",
                        "TransactionDate": null,
                        "postDate": "2013-11-02",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283984,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-11-01",
                        "TransactionDate": null,
                        "postDate": "2013-11-01",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283985,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-10-31",
                        "TransactionDate": null,
                        "postDate": "2013-10-31",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283986,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-10-30",
                        "TransactionDate": null,
                        "postDate": "2013-10-30",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283987,
                        "Amount": {
                            "Amount": 180.89,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-10-29",
                        "TransactionDate": null,
                        "postDate": "2013-10-29",
                        "Description": {
                            "Original": "AMAZON MKTPLACE PMTS",
                            "Consumer": null,
                            "Simple": "Amazon"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": "amazon",
                            "Source": "FACTUAL",
                            "Name": "Amazon",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283989,
                        "Amount": {
                            "Amount": 101.07,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 7,
                        "Category": "Entertainment/Recreation",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-10-28",
                        "TransactionDate": null,
                        "postDate": "2013-10-28",
                        "Description": {
                            "Original": "BLOCKBUSTEREXPRESS",
                            "Consumer": null,
                            "Simple": "Blockbuster"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "blockbuster",
                            "Source": "FACTUAL",
                            "Name": "Blockbuster",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283990,
                        "Amount": {
                            "Amount": 20.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 22,
                        "Category": "Restaurants",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000011,
                        "Date": "2013-10-27",
                        "TransactionDate": null,
                        "postDate": "2013-10-27",
                        "Description": {
                            "Original": "McDonalds",
                            "Consumer": null,
                            "Simple": "McDonald's"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "mcdonald's",
                            "Source": "FACTUAL",
                            "Name": "McDonald's",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283991,
                        "Amount": {
                            "Amount": 2500.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 13,
                        "Category": "Home Improvement",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-10-26",
                        "TransactionDate": null,
                        "postDate": "2013-10-26",
                        "Description": {
                            "Original": "Furniture Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283992,
                        "Amount": {
                            "Amount": 1000.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-10-25",
                        "TransactionDate": null,
                        "postDate": "2013-10-25",
                        "Description": {
                            "Original": "Online Purchase",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "ONLINE_PURCHASE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283993,
                        "Amount": {
                            "Amount": 10.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-10-24",
                        "TransactionDate": null,
                        "postDate": "2013-10-24",
                        "Description": {
                            "Original": "Service Charges",
                            "Consumer": null,
                            "Simple": "Service Charge"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "CHARGES_FEES",
                        "SubType": "SERVICE_CHARGE",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283994,
                        "Amount": {
                            "Amount": 110.01,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 2,
                        "Category": "Automotive/Fuel",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000003,
                        "Date": "2013-10-23",
                        "TransactionDate": null,
                        "postDate": "2013-10-23",
                        "Description": {
                            "Original": "SHELL OIL",
                            "Consumer": null,
                            "Simple": "Shell"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "shell",
                            "Source": "FACTUAL",
                            "Name": "Shell",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283995,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 15,
                        "Category": "Cable/Satellite/Telecom",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-10-22",
                        "TransactionDate": null,
                        "postDate": "2013-10-22",
                        "Description": {
                            "Original": "Telephone Bill Paid",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283996,
                        "Amount": {
                            "Amount": 124.72,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 43,
                        "Category": "Electronics/General Merchandise",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000004,
                        "Date": "2013-10-21",
                        "TransactionDate": null,
                        "postDate": "2013-10-21",
                        "Description": {
                            "Original": "WAL-MART",
                            "Consumer": null,
                            "Simple": "Walmart"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "walmart",
                            "Source": "FACTUAL",
                            "Name": "Walmart",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283997,
                        "Amount": {
                            "Amount": 200.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "TRANSFER",
                        "CategoryId": 26,
                        "Category": "Credit Card Payments",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000018,
                        "Date": "2013-10-20",
                        "TransactionDate": null,
                        "postDate": "2013-10-20",
                        "Description": {
                            "Original": "Payment - Thank You",
                            "Consumer": null,
                            "Simple": "Credit Card Payment"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PAYMENT",
                        "SubType": "CREDIT_CARD_PAYMENT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "creditCard",
                        "Id": 15283998,
                        "Amount": {
                            "Amount": 101.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 0.0,
                            "Currency": null
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 24,
                        "Category": "Service Charges/Fees",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-10-19",
                        "TransactionDate": null,
                        "postDate": "2013-10-19",
                        "Description": {
                            "Original": "AIR VALET",
                            "Consumer": null,
                            "Simple": "AirValet"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325976,
                        "Type": "PURCHASE",
                        "SubType": "PURCHASE",
                        "Merchant": {
                            "Id": "1390",
                            "Source": "YODLEE",
                            "Name": "AirValet",
                            "Address": {
                                "City": null,
                                "State": null,
                                "Country": null,
                                "Zip": null
                            }
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17699631,
                        "Amount": {
                            "Amount": 343465.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "UNCATEGORIZE",
                        "CategoryId": 1,
                        "Category": "Uncategorized",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000017,
                        "Date": "2013-07-16",
                        "TransactionDate": "2013-07-16",
                        "postDate": "2013-07-16",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10340107,
                        "Type": null,
                        "SubType": null,
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17419828,
                        "Amount": {
                            "Amount": 343465.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "INCOME",
                        "CategoryId": 27,
                        "Category": "Deposits",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000012,
                        "Date": "2013-07-16",
                        "TransactionDate": "2013-07-16",
                        "postDate": "2013-07-16",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": "Deposit"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325974,
                        "Type": "DEPOSITS_CREDITS",
                        "SubType": "DEPOSIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17699632,
                        "Amount": {
                            "Amount": 3465.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "UNCATEGORIZE",
                        "CategoryId": 1,
                        "Category": "Uncategorized",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000017,
                        "Date": "2013-01-16",
                        "TransactionDate": "2013-01-16",
                        "postDate": "2013-01-16",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10340108,
                        "Type": null,
                        "SubType": null,
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17699630,
                        "Amount": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 33,
                        "Category": "Check Payment",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-01-16",
                        "TransactionDate": "2013-07-16",
                        "postDate": "2013-01-16",
                        "Description": {
                            "Original": "CHECK # 998",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10340107,
                        "Type": null,
                        "SubType": null,
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17419831,
                        "Amount": {
                            "Amount": 3465.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "INCOME",
                        "CategoryId": 27,
                        "Category": "Deposits",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000012,
                        "Date": "2013-01-16",
                        "TransactionDate": "2013-01-16",
                        "postDate": "2013-01-16",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": "Deposit"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325975,
                        "Type": "DEPOSITS_CREDITS",
                        "SubType": "DEPOSIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17419827,
                        "Amount": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 33,
                        "Category": "Check Payment",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-01-16",
                        "TransactionDate": "2013-07-16",
                        "postDate": "2013-01-16",
                        "Description": {
                            "Original": "CHECK # 998",
                            "Consumer": null,
                            "Simple": "Payment by Check"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325974,
                        "Type": "PAYMENT",
                        "SubType": "PAYMENT_BY_CHECK",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17699634,
                        "Amount": {
                            "Amount": 3103.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "UNCATEGORIZE",
                        "CategoryId": 1,
                        "Category": "Uncategorized",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000017,
                        "Date": "2013-01-14",
                        "TransactionDate": "2013-01-12",
                        "postDate": "2013-01-14",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10340108,
                        "Type": null,
                        "SubType": null,
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17419833,
                        "Amount": {
                            "Amount": 3103.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "DEBIT",
                        "CategoryType": "EXPENSE",
                        "CategoryId": 19,
                        "Category": "Other Expenses",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000009,
                        "Date": "2013-01-14",
                        "TransactionDate": "2013-01-12",
                        "postDate": "2013-01-14",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325975,
                        "Type": "OTHER_WITHDRAWALS",
                        "SubType": "DEBIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17699633,
                        "Amount": {
                            "Amount": 5646.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "UNCATEGORIZE",
                        "CategoryId": 1,
                        "Category": "Uncategorized",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000017,
                        "Date": "2013-01-10",
                        "TransactionDate": "2013-01-11",
                        "postDate": "2013-01-10",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10340108,
                        "Type": null,
                        "SubType": null,
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17419832,
                        "Amount": {
                            "Amount": 5646.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "INCOME",
                        "CategoryId": 27,
                        "Category": "Deposits",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000012,
                        "Date": "2013-01-10",
                        "TransactionDate": "2013-01-11",
                        "postDate": "2013-01-10",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": "Deposit"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325975,
                        "Type": "DEPOSITS_CREDITS",
                        "SubType": "DEPOSIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17699635,
                        "Amount": {
                            "Amount": 9846.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "UNCATEGORIZE",
                        "CategoryId": 1,
                        "Category": "Uncategorized",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000017,
                        "Date": "2013-01-02",
                        "TransactionDate": "2013-01-16",
                        "postDate": "2013-01-02",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": null
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10340108,
                        "Type": null,
                        "SubType": null,
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }, {
                        "Container": "bank",
                        "Id": 17419834,
                        "Amount": {
                            "Amount": 9846.0,
                            "Currency": "USD"
                        },
                        "RunningBalance": {
                            "Amount": 59.0,
                            "Currency": "USD"
                        },
                        "BaseType": "CREDIT",
                        "CategoryType": "INCOME",
                        "CategoryId": 27,
                        "Category": "Deposits",
                        "CategorySource": "SYSTEM",
                        "HighLevelCategoryId": 10000012,
                        "Date": "2013-01-02",
                        "TransactionDate": "2013-01-16",
                        "postDate": "2013-01-02",
                        "Description": {
                            "Original": "DESC",
                            "Consumer": null,
                            "Simple": "Deposit"
                        },
                        "IsManual": false,
                        "Status": "POSTED",
                        "PostingOrder": 0,
                        "AccountId": 10325975,
                        "Type": "DEPOSITS_CREDITS",
                        "SubType": "DEPOSIT",
                        "Merchant": {
                            "Id": null,
                            "Source": null,
                            "Name": null,
                            "Address": null
                        }
                    }
                ]
            }
        }
    }
};
console.log(generateYodleeCashflow(input));
