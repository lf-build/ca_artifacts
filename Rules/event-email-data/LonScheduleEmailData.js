function LonScheduleEmailData(payload) {
	 try {
	if (payload != null) {
		var eventDetails = payload.eventData.EventData;
		var emailId =eventDetails.LoanResponse.PrimaryApplicantDetails.Emails != null ?eventDetails.LoanResponse.PrimaryApplicantDetails.Emails[0].EmailAddress : 'foram.j@sigmainfo.net';
		var name = jsUcfirst(eventDetails.LoanResponse.PrimaryApplicantDetails.Name.First + ' ' + eventDetails.LoanResponse.PrimaryApplicantDetails.Name.Last);
		var designation = eventDetails.LoanResponse.PrimaryApplicantDetails != null ? eventDetails.LoanResponse.PrimaryApplicantDetails.Role :'';
		var businessName = eventDetails.LoanResponse.PrimaryBusinessDetails != null ? eventDetails.LoanResponse.PrimaryBusinessDetails.BusinessName : '';
		var scheduleDate = eventDetails.ScheduleResponse != null ? eventDetails.ScheduleResponse.ScheduleVersionDate.Time : null;
		var reason = eventDetails.ScheduleResponse.ScheduledCreatedReason;
		var currentBalance =eventDetails.AccountingResponse != null? eventDetails.AccountingResponse.PBOT.TotalPrincipalOutStanding + eventDetails.AccountingResponse.PBOT.TotalInterestOutstanding:0; 
		var newDrawDownAmount = eventDetails.ScheduleResponse != null ?  eventDetails.ScheduleResponse.LoanAmount :0;
		var totalBalanceDue =currentBalance + newDrawDownAmount;
		var scheduleList = 	eventDetails.ScheduleResponse != null ? eventDetails.ScheduleResponse.ScheduleDetails : [];
		var payoffSchedule =eventDetails.ScheduleResponse != null ? eventDetails.ScheduleResponse : [];
		var todayDate = new Date();		
        var date = todayDate.getMonth()+1 +'-'+ todayDate.getDate() +'-'+ todayDate.getFullYear();
		var loanDue = eventDetails.AccountingResponse != null? eventDetails.AccountingResponse.PBOT.TotalPrincipalOutStanding +eventDetails.AccountingResponse.PBOT.TotalInterestOutstanding+eventDetails.AccountingResponse.PBOT.TotalFeeOutstanding  :0;
		var payoffAmount = eventDetails.AccountingResponse != null? (eventDetails.AccountingResponse.PayOff != null ?eventDetails.AccountingResponse.PayOff.PayOffAmount:0) :0;
		var payoffDate = eventDetails.AccountingResponse != null? (eventDetails.AccountingResponse.PayOff != null ?eventDetails.AccountingResponse.PayOff.PayOffDate :'') :'';
		
		
		var result = {
			Email: emailId,
			Name : name,
			LoanNumber : payload.entityId,
			BusinessName : businessName,
			ScheduleDate : scheduleDate,
			Reason:reason,
			CurrentBalance : totalBalanceDue - newDrawDownAmount,
			NewDrawDownAmount : newDrawDownAmount,
			TotalBalanceDue:totalBalanceDue,
			ScheduleList : scheduleList,
			PayoffSchedule:payoffSchedule,
			TodayDate:date,
			LoanDue:loanDue,
			Designation : designation,
			PayoffAmount:payoffAmount,
			PaiedoffDate :payoffDate
		};
		return {
				'result': 'Passed',
				'detail': null,
				'data': result,
				'rejectcode': '',
				'exception': []
			};

	}
	var errorData = [];
	errorData.push('Data Not found for email');
	return {

		'result' : 'Failed',
		'detail' : null,
		'data' : '',
		'rejectcode' : '',
		'exception': errorData
	};
	
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	};
	 }
    catch (e) {
        return {
            'result': false,
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };
    }
}
