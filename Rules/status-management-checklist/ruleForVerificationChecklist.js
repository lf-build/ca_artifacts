function ruleForVerificationChecklist(payload) {
	var factService = this.call('factVerification');
	function getVerificationFactDetails(input, factName) {
		for (var i = 0; i < input.length; i++) {
			if (input[i]['factName'] == factName) {
				return input[i];
			}
		}
	}
	if (typeof (payload) != 'undefined') {
		return factService.get(payload.entityType, payload.entityId).then(function (response) {
			var result = 'Failed';
			var counter = 0;
			var totalFacts = 0;
			var factList = ['CashflowVerification', 'BankruptcyVerification'];
			totalFacts = factList.length;
			for (var i = 0; i < factList.length; i++) {
				var cashflowVerification = getVerificationFactDetails(response, factList[i]);
				if (typeof (cashflowVerification) != 'undefined') {
					if (cashflowVerification.currentStatus == 'Completed') {
						counter = counter + 1;
					}
				}
			}
			var optionalFactList = ['BusinessCreditRiskVerification'];
			for (var j = 0; j < optionalFactList.length; j++) {
				var optionalVerification = getVerificationFactDetails(response, optionalFactList[j]);
				if (typeof (optionalVerification) != 'undefined') {
					totalFacts = totalFacts + 1;
					if (optionalVerification.currentStatus == 'Completed') {
						counter = counter + 1;
					}
				}
			}
			if (counter == totalFacts) {
				return {
					'result': 'Passed',
					'detail': '',
					'detail': [],
					'rejectCode': '',
					'exception': null
				};
			} else {
				return {
					'result': 'Failed',
					'detail': 'Verification Pending',
					'data': [],
					'rejectCode': '',
					'exception': null
				};
			}
		}).catch(function (error) {
			return {
				'result': 'Failed',
				'detail': error.message,
				'data': [],
				'rejectCode': '',
				'exception': null
			};
		});
	} else {
		return {
			'result': 'Failed',
			'detail': 'Verification Pending',
			'data': [],
			'rejectCode': '',
			'exception': null
		};
	}
}
