function data_attribute_datamerchsearchmerchantrequestedrule(payload) {
    var result = 'Passed';
    var fein = '';
    var legalName = '';
    var dba = '';
    var address = '';
    var street1 = '';
    var street2 = '';
    var city = '';
    var state = '';
    var businessPhone = '';
    var businessStartdate = '';
    var industry = '';
    var merchantNotes = {};
    var restrictedCategory = ['Slow Pay'];
    var isSlowPay = false;
    var merchantNotesList = '';
    if (typeof (payload) != 'undefined' && payload.eventData.Response
        && payload.eventData.Response.Merchants
        && payload.eventData.Response.Merchants.length > 0) {
        var datamerchSearchMerchantResponse = payload.eventData.Response.Merchants[0];
        fein = datamerchSearchMerchantResponse.Merchant.Fein;
        legalName = datamerchSearchMerchantResponse.Merchant.LegalName;
        dba = datamerchSearchMerchantResponse.Merchant.Dba;
        address = datamerchSearchMerchantResponse.Merchant.Address;
        street1 = datamerchSearchMerchantResponse.Merchant.Street1;
        street2 = datamerchSearchMerchantResponse.Merchant.Street2;
        city = datamerchSearchMerchantResponse.Merchant.City;
        state = datamerchSearchMerchantResponse.Merchant.State;
        businessPhone = datamerchSearchMerchantResponse.Merchant.BusinessPhone;
        businessStartdate = datamerchSearchMerchantResponse.Merchant.BusinessStartdate;
        industry = datamerchSearchMerchantResponse.Merchant.Industry;
        merchantNotesList = datamerchSearchMerchantResponse.Merchant.MerchantNotes;
        var notes = [];
        if (merchantNotesList) {
            for (var i = 0; i < merchantNotesList.length; i++) {
                var note = {
                    Category: null,
                    Note: null,
                    CreatedAt: null,
                    AddedBy: null
                };
                note.Category = merchantNotesList[i].MerchantNote.Category;
                note.Note = merchantNotesList[i].MerchantNote.Note;
                note.CreatedAt = merchantNotesList[i].MerchantNote.CreatedAt;
                note.AddedBy = merchantNotesList[i].MerchantNote.AddedBy;
                notes.push(note);
            }
        }
        for (var i = 0; i < notes.length; i++) {
            if (restrictedCategory.indexOf(notes[i].Category) > -1) {
                isSlowPay = true; break;
            }
        }
        merchantNotes = { Notes: notes };
        var Data = {
            'datamerchReport':
            {
                'Fein': fein,
                'LegalName': legalName,
                'Dba': dba,
                'Address': address,
                'Street1': street1,
                'Street2': street2,
                'City': city,
                'State': state,
                'BusinessPhone': businessPhone,
                'BusinessStartdate': businessStartdate,
                'Industry': industry,
                'MerchantNotes': merchantNotes,
                'IsSlowPay': isSlowPay,
                'referenceNumber': payload.eventData.ReferenceNumber
            }
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '', 'exception': []
        };
    }
    return {
        'result': result,
        'detail': null,
        'data': {
            'datamerchReport':
            {
                'Fein': fein,
                'LegalName': legalName,
                'Dba': dba,
                'Address': address,
                'Street1': street1,
                'Street2': street2,
                'City': city,
                'State': state,
                'BusinessPhone': businessPhone,
                'BusinessStartdate': businessStartdate,
                'Industry': industry,
                'MerchantNotes': { Notes: [] },
                'IsSlowPay': isSlowPay,
                'referenceNumber': payload.eventData.ReferenceNumber
            }
        },
        'rejectcode': '', 'exception': []
    };
}