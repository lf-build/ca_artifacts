function data_attribute_criminalreportrecords(payload) {
	var result = 'Passed';
	var allFraudRecord = [];
	var RecordList = [];
	var myCustomRecord = {
		'CriminalData': [{}
		]
	};
	var key = '';
	if (payload.eventData.Request != null && payload.eventData.Request.OwnerId != null && payload.eventData.Request.OwnerId != undefined) {
		key = payload.eventData.Request.OwnerId;
	}
	var Header = payload.eventData.Response.Header;
	var Records = payload.eventData.Response.Records;
	var RecordCount = payload.eventData.Response.recordCount;
	if (Records != null && Records.length > 0) {
		for (var i = 0; i < Records.length; i++) {
			for (var index = 0; index < Records[i].Offenses.length; index++) {
				var Recordes = {
					CourtOffense: null,
					CourtDispositionDate: null,
					CourtDisposition: null,
					Offense: null,
					ArrestLevelORDegree: null,
					CourtLevelORDegree: null,
					CourtStatute: null,
					CourtPlea: null,
					ArrestStatute: null,
					CaseNumber: null
				};
				Recordes.CourtOffense = Records[i].Offenses[index].Court.Offense;
				Recordes.CourtDispositionDate = Records[i].Offenses[index].Court.DispositionDate.Month + '/' + Records[i].Offenses[index].Court.DispositionDate.Day + '/' + Records[i].Offenses[index].Court.DispositionDate.Year;
				Recordes.CourtDisposition = Records[i].Offenses[index].Court.Disposition;
				Recordes.CourtLevelORDegree = Records[i].Offenses[index].Court.Level;
				Recordes.CourtStatute = Records[i].Offenses[index].Court.Statute;
				Recordes.CourtPlea = Records[i].Offenses[index].Court.Plea;
				Recordes.CaseNumber = 'Case Number:' + Records[i].CaseNumber;
				RecordList.push(Recordes);
			}
		}
		myCustomRecord.CriminalData = RecordList;
		var CriminalDataViewModel = {
			'Records': myCustomRecord,
			'SecondaryName': key
		};
		allFraudRecord.push(CriminalDataViewModel);
	};
	var Data = {};
	Data[key] = allFraudRecord;
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}