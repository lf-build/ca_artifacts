function taggingRuleForVerificationDocumentReceived(payload) {

	var applyTags = [];
	var removeTags = [];
	if (payload != null && payload.eventData != null && payload.eventData.Data != null) {
		var objFactDetails = payload.eventData.Data;

		if (objFactDetails.Fact == 'SignedDocumentVerification') {
			removeTags.push('Pending Signature');
		}
	}
	var Data = {
		'ApplyTags': applyTags,
		'RemoveTags': removeTags
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}