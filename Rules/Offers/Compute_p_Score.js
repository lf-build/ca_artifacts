function Compute_p_Score(payload) {

    try {
        if (typeof (payload) != 'undefined') {

            var weightedScore = 0;
            var diff_xb_ALL6203 = '';
            var diff_xb_ALL8323 = '';
            var diff_xb_ALM6160 = '';
            var diff_xb_AUA6200 = '';
            var diff_xb_AUL5020 = '';
            var diff_xb_BCC5320 = '';
            var diff_xb_BUS0416 = '';
            var diff_xb_ILN7310 = '';
            var diff_xb_IQT9536 = '';
            var diff_xb_MTF6244 = '';
            var diff_xb_REH3423 = '';
            var diffDictionary = [];
            for (var propertyName in payload) {
                var propertyValue = payload[propertyName];
                if (typeof (propertyValue[0].WeightedScore) != 'undefined') {
                    var propertyWeightedScore = parseFloat(propertyValue[0].WeightedScore);
                    weightedScore += propertyWeightedScore;

                    if (propertyName == 'm_ALL6203_PScore_Attribute') {
                        diff_xb_ALL6203 = propertyWeightedScore - (-0.008437071);
                        diffDictionary.push({
                            key: 'diff_xb_ALL6203', value: diff_xb_ALL6203,
                            description: 'Worst ever status on the most recently opened trade, excluding collections including indeterminate'
                        });
                    }
                    else if (propertyName == 'm_ALL8323_PScore_Attribute') {
                        diff_xb_ALL8323 = propertyWeightedScore - (-0.440456622);
                        diffDictionary.push({
                            key: 'diff_xb_ALL8323', value: diff_xb_ALL8323,
                            description: 'Average number of months since ever 90 or more days delinquent or derogatory trades were opened excluding collections including indeterminate'
                        });
                    }
                    else if (propertyName == 'm_ALM6160_PScore_Attribute') {
                        diff_xb_ALM6160 = propertyWeightedScore - (-0.028293696);
                        diffDictionary.push({
                            key: 'diff_xb_ALM6160', value: diff_xb_ALM6160,
                            description: 'Worst present status on a trade reported in the last 6 months including non-medical collections and indeterminate'
                        });
                    }
                    else if (propertyName == 'm_AUA6200_PScore_Attribute') {
                        diff_xb_AUA6200 = propertyWeightedScore - (-0.008724728);
                        diffDictionary.push({
                            key: 'diff_xb_AUA6200', value: diff_xb_AUA6200,
                            description: 'Worst ever status on an auto loan or lease trade including indeterminate'
                        });
                    }
                    else if (propertyName == 'm_AUL5020_PScore_Attribute') {
                        diff_xb_AUL5020 = propertyWeightedScore - (-0.124599338);
                        diffDictionary.push({
                            key: 'diff_xb_AUL5020', value: diff_xb_AUL5020,
                            description: 'Total balance on open auto lease trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_BCC5320_PScore_Attribute') {
                        diff_xb_BCC5320 = propertyWeightedScore - (-0.074639828);
                        diffDictionary.push({
                            key: 'diff_xb_BCC5320', value: diff_xb_BCC5320,
                            description: 'Total credit amount on open revolving bankcard trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_BUS0416_PScore_Attribute') {
                        diff_xb_BUS0416 = propertyWeightedScore - (-0.100463939);
                        diffDictionary.push({
                            key: 'diff_xb_BUS0416', value: diff_xb_BUS0416,
                            description: 'Total number of open personal liable business loan, line of credit, or credit card trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_ILN7310_PScore_Attribute') {
                        diff_xb_ILN7310 = propertyWeightedScore - (0.260181761);
                        diffDictionary.push({
                            key: 'diff_xb_ILN7310', value: diff_xb_ILN7310,
                            description: 'Percentage of all trades excluding collections that are open installment trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_IQT9536_PScore_Attribute') {
                        diff_xb_IQT9536 = propertyWeightedScore - (-0.294373456);
                        diffDictionary.push({
                            key: 'diff_xb_IQT9536', value: diff_xb_IQT9536,
                            description: 'Average number of days between inquiries made in the last 6 months (no deduplication)'
                        });
                    }
                    else if (propertyName == 'm_MTF6244_PScore_Attribute') {
                        diff_xb_MTF6244 = propertyWeightedScore - (-0.031898509);
                        diffDictionary.push({
                            key: 'diff_xb_MTF6244', value: diff_xb_MTF6244,
                            description: 'Worst ever status on a first mortgage trade opened in 2004 including indeterminate'
                        });
                    }
                    else if (propertyName == 'm_REH3423_PScore_Attribute') {
                        diff_xb_REH3423 = propertyWeightedScore - (0.082993201);
                        diffDictionary.push({
                            key: 'diff_xb_REH3423', value: diff_xb_REH3423,
                            description: 'Total number of open revolving trades with a balance to credit amount ratio >= 100 reported in the last 6 months excluding home equity line of credit trades'
                        });
                    }
                }
            }

            if (diffDictionary != null && diffDictionary.length > 0) {
                diffDictionary.sort(function (a, b) {
                    return b.value - a.value;
                });
            }
            var topReasonCounter = 4;
            var topReasons = diffDictionary.slice(0, topReasonCounter);

            var p_score = 0;
            var p_xbeta = -1.4317297640 + weightedScore;
            p_score = Math.round(650 - 28.8539 * p_xbeta, 1);
            var p_prob = Math.exp(p_xbeta) / (1 + Math.exp(p_xbeta));
            var records = {
                'p_prob': p_prob,
                'p_score': p_score,
                'top_reasons': topReasons,
                'all_reasons': diffDictionary
            };

            for (var i = 0; i < topReasons.length; i++) {
                var key = 'factor-' + (i + 1);
                records[key] = topReasons[i].description;
            }

            return {
                'result': 'Passed',
                'detail': null,
                'data': records,
                'rejectcode': '',
                'exception': []
            };
        }
        return {
            'result': 'Failed',
            'detail': null,
            'data': '',
            'rejectcode': '',
            'exception': []
        };
    }
    catch (e) {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };

    }

}