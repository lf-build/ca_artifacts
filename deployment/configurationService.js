var path = require('path');
var fs = require('fs');
var config = require('../dev.json');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveConfiguration(serviceUrl, configFile, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(configFile);
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        console.log('Publishing Open Configuration ' + configFile);
        var data = fs.readFileSync(configFile, 'utf8');
        var configurationKey = pathObject.name;
        console.log('Configuring for ' + configurationKey);
        data = data.replace(/{{external_ip}}/g, serviceUrl.split(':')[1].replace('//', '')).replace(/{{internal_ip}}/g, config.internalIp).replace(/{{appToken}}/g, config.authorizationToken);
        return api.delete(serviceUrl + '/' + configurationKey, data).then(function (result) {
            if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                console.log("Configuration Deleted " + configurationKey);
                return api.post(serviceUrl + '/' + configurationKey, data).then(function (result) {
                    if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                        console.log("Configuration Published " + configurationKey);
                    }
                    else {
                        console.log("Failed to Publish configuration " + configurationKey);
                    }
                });
            }
            else {
                console.error("Deleted Configuration Failed for " + configurationKey + ",message:" + result.statusMessage + ",response:" + result.body);
            }
        });
    }
    else {
        console.warn('Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
}

function importConfiguration(serviceUrl, folderPath, token) {
    var files = walkSync(folderPath);
    var configToProcess = [];

    for (var i = 0; i < files.length; i++) {
        var element = files[i];
        var pathObject = path.parse(element);
        if (pathObject.ext.toLocaleLowerCase() == ".json") {
            configToProcess.push(element);
        }
        else {
            console.warn('Skipping file ' + element + ' as extension :' + pathObject.ext.toLocaleLowerCase());
        }
    }

    configToProcess = configToProcess.map(r => () => saveConfiguration(serviceUrl, r, token));

    return configToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Configuration failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveConfiguration: function (serviceUrl, configFile) {
            return saveConfiguration(serviceUrl, configFile, token);
        },
        importConfiguration: function (serviceUrl, folderPath) {
            return importConfiguration(serviceUrl, 'Configuration', token);
        }
    }
}