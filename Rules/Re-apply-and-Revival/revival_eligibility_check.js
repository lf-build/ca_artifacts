function revival_eligibility_check(payload) {
	var result = 'Failed';
	var Data = {};
	var noteligibleStatus = ['600.10'];
	var reviveDaysLimit = 0;
	if (payload.eventData && payload.eventData.CurrentStatus && payload.application && payload.application[0]) {
		var application = payload.application[0];
		var currentStatus = payload.eventData.CurrentStatus;
		if (noteligibleStatus.indexOf(currentStatus.Code) < 0) {
			if (application.applicationDate && application.applicationDate.Time) {
				var appDate = new Date(application.applicationDate.Time);
				var todaysDate = new Date();
				var dayDiff = this.call('datedifference', ['D', appDate, todaysDate]);
				if (dayDiff >= reviveDaysLimit) {
					result = 'Passed';
				}
				Data = {
					'dayDiff': dayDiff,
					'StatusCode': currentStatus.Code,
					'appDate': appDate,
					'todaysDate': todaysDate
				};
			}
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}