function criscko_data_extraction(input) {
    if (input) {
        return {
            'BusinessId': input.BusinessId,
            'EntityId': input.EntityId,
            'EntityType': input.EntityType
        };
    }
    return null;
}