function GetComputedAnnualRevenue(payload) {

    try {
        if (typeof (payload) != 'undefined') {
            var annual_gross_sale = 0;
            var selectedCashflow = payload.cashflowVerificationData[0].SelectedAccountData;
            if (selectedCashflow != null && selectedCashflow.length > 0) {
                annual_gross_sale = selectedCashflow[0].CashFlow.TransactionSummary.AnnualCalculatedRevenue;
            }

            var result = {
                'key': 'annual_gross_sale',
                'value': annual_gross_sale
            };
            return {
                'result': 'Passed',
                'detail': null,
                'data': result,
                'rejectcode': '',
                'exception': []
            };
        }
        return {
            'result': 'Failed',
            'detail': null,
            'data': '',
            'rejectcode': '',
            'exception': []
        };
    }
    catch (e) {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };

    }

}