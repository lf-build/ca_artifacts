function data_attribute_experian_business_report_rule(payload) {
    try {
        var subscriberNumber = null;
        var subscriberNumber = null;
        var inquiryTransactionNumber = null;
        var cpuVersion = null;
        var action = 'HIGH RISK';
        var RTC037 = 0;
        var CLC006 = 0;
        var TaxLienFilingCount = '';
        var JudgmentFilingCount = '';
        var UccFilings = '';
        var IntelliScore = '';
        var FinancialStability = '';
        var LegalBalance = '';
        var Bankruptcy = '';
        var UCCFiledDate = '';
        var SecuredParty = '';
        var CautionaryUCCFilings = '';
        var Sumoflegalfilings = '';
        var Collateral = '';
        var DateofIncorporation = '';
        var CurrentStatus = '';
        var Timeinbusiness = 0;
        var m_action_high = 0;
        var m_action_miss = 0;
        var m_CLC006 = 0;
        var m_RTC037 = 0;
        var arrActions = ['HIGH RISK', 'RECENT BANKRUPTCY ON FILE'];
        if (payload && payload.eventData.Response) {
            var response = payload.eventData.Response.PremierProfile;
            if (response && response.InputSummary && response.InputSummary.length > 0) {
                var inputSummary = response.InputSummary[0];
                subscriberNumber = inputSummary.SubscriberNumber;
                inquiryTransactionNumber = inputSummary.InquiryTransactionNumber;
                cpuVersion = inputSummary.CPUVersion;
            }
            if (response && response.IntelliscoreScoreInformation && response.IntelliscoreScoreInformation.length > 0) {
                action = response.IntelliscoreScoreInformation[0].Action;
                for (var i = 0; i < response.IntelliscoreScoreInformation.length; i++) {
                    if (response.IntelliscoreScoreInformation[i].ModelInformation && response.IntelliscoreScoreInformation[i].ModelInformation.length > 0) {
                        if (response.IntelliscoreScoreInformation[i].ModelInformation[0].ModelTitle == 'INTELLISCORE PLUS V2') {
                            if (response.IntelliscoreScoreInformation[i].ScoreInfo && response.IntelliscoreScoreInformation[i].ScoreInfo.length > 0) {
                                IntelliScore = response.IntelliscoreScoreInformation[i].ScoreInfo[0].Score;
                            }
                        }
                        else if (response.IntelliscoreScoreInformation[i].ModelInformation[i].ModelTitle == 'FINANCIAL STABILITY RISK') {
                            if (response.IntelliscoreScoreInformation[i].ScoreInfo && response.IntelliscoreScoreInformation[i].ScoreInfo.length > 0) {
                                FinancialStability = response.IntelliscoreScoreInformation[i].ScoreInfo[0].Score;
                            }
                        }
                    }
                }
            }
            if (response && response.ExpandedCreditSummary && response.ExpandedCreditSummary.length > 0) {
                CLC006 = response.ExpandedCreditSummary[0].AllTradelineCount;
                LegalBalance = response.ExpandedCreditSummary[0].LegalBalance;
                UCCFiledDate = response.ExpandedCreditSummary[0].MostRecentUccFilingDate;
                CautionaryUCCFilings = response.ExpandedCreditSummary[0].UccDerogCount;
                Bankruptcy = response.ExpandedCreditSummary[0].BankruptcyFilingCount;
                TaxLienFilingCount = response.ExpandedCreditSummary[0].TaxLienFilingCount;
                JudgmentFilingCount = response.ExpandedCreditSummary[0].JudgmentFilingCount;
                UccFilings = response.ExpandedCreditSummary[0].UCCFilings;
                Sumoflegalfilings = response.ExpandedCreditSummary[0].LegalBalance;
                if (UCCFiledDate) {
                    UCCFiledDate = UCCFiledDate.replace(/(\\d\\d\\d\\d)(\\d\\d)(\\d\\d)/, '$1-$2-$3');
                }

            }

            if (response && response.UccFilings && response.UccFilings.length > 0) {
                SecuredParty = response.UccFilings[0].SecuredParty;
                if (response.UccFilings[0].CollateralCodes && response.UccFilings[0].CollateralCodes.Collateral && response.UccFilings[0].CollateralCodes.Collateral[0].length > 0) {
                    Collateral = response.UccFilings[0].CollateralCodes.Collateral[0].Value;
                }
            }
            if (response && response.CorporateRegistration) {
                DateofIncorporation = response.CorporateRegistration.IncorporatedDate;
                if (DateofIncorporation) {
                    DateofIncorporation = DateofIncorporation.replace(/(\\d\\d\\d\\d)(\\d\\d)(\\d\\d)/, '$1-$2-$3');
                }
                if (response.CorporateRegistration.StatusFlag) {
                    CurrentStatus = response.CorporateRegistration.StatusFlag.Text;
                }
            }
            if (response && response.PaymentTotals && response.PaymentTotals.length > 0 && response.PaymentTotals[0].ContinouslyReportedTradeLines
                && response.PaymentTotals[0].ContinouslyReportedTradeLines.length > 0) {
                RTC037 = response.PaymentTotals[0].ContinouslyReportedTradeLines[0].NumberOfLines;
            }
            if (response && response.DemographicInformation) {
                Timeinbusiness = response.DemographicInformation.YearsInBusinessOrLowRange;
            }

            if (action == null) {
                m_action_miss = 1;
            };
            if (arrActions.indexOf(action) > -1) {
                m_action_high = 1;
            };

            if (CLC006 == null) {
                m_CLC006 = 0;
            }
            else if (CLC006 < 0) {
                m_CLC006 = 0;
            }
            else if (CLC006 > 1) {
                m_CLC006 = 1;
            }
            else if (CLC006 >= 0 && CLC006 <= 1) {
                m_CLC006 = CLC006;
            }


            if (RTC037 == null) {
                m_RTC037 = 0;
            }
            else if (RTC037 < 0) {
                m_RTC037 = 0;
            }
            else if (RTC037 > 5) {
                m_RTC037 = 5;
            }
            else if (RTC037 >= 0 && RTC037 <= 5) {
                m_RTC037 = RTC037;
            }

            var records = {
                'action': action,
                'CLC006': CLC006,
                'RTC037': RTC037,
                'IntelliScore': IntelliScore,
                'FinancialStability': FinancialStability,
                'TaxLienFilingCount': TaxLienFilingCount,
                'JudgmentFilingCount': JudgmentFilingCount,
                'UccFilings': UccFilings,
                'LegalBalance': LegalBalance,
                'Bankruptcy': Bankruptcy,
                'm_action_miss': m_action_miss,
                'm_action_high': m_action_high,
                'm_CLC006': m_CLC006,
                'm_RTC037': m_RTC037,
                'CautionaryUCCFilings': CautionaryUCCFilings,
                'Sumoflegalfilings': Sumoflegalfilings,
                'SecuredParty': SecuredParty,
                'Collateral': Collateral,
                'DateofIncorporation': DateofIncorporation,
                'CurrentStatus': CurrentStatus,
                'UCCFiledDate': UCCFiledDate,
                'Timeinbusiness': Timeinbusiness,
                'referenceNumber': payload.eventData.ReferenceNumber
            };
        }
        var Data = {
            'experianBusinessReport': records
        };

        return {
            'result': 'Passed',
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };

    } catch (e) {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };
    }
}

