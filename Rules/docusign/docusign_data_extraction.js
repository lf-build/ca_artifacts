function docusign_data_extraction(input) {
    var response = {
        'SignerEmail': null,
        'DocumentBytes': null,
        'Status': null,
        'EntityId': null,
        'EntityType': 'application'
    };
    if (input) {
        if (input.DocuSignEnvelopeInformation) {
            var docuSignEnvelopeInformation = input.DocuSignEnvelopeInformation;
            if (docuSignEnvelopeInformation.EnvelopeStatus) {
                var envelopeStatus = docuSignEnvelopeInformation.EnvelopeStatus;
                response.Status = envelopeStatus.Status;
                if (envelopeStatus.RecipientStatuses) {
                    var recipientStatuses = envelopeStatus.RecipientStatuses;
                    if (recipientStatuses.RecipientStatus) {
                        response.SignerEmail = recipientStatuses.RecipientStatus.Email;
                    }
                }
                if (envelopeStatus.CustomFields) {
                    if (envelopeStatus.CustomFields.CustomField) {
                        response.EntityId = envelopeStatus.CustomFields.CustomField.Value;
                    }
                }
            }
            if (docuSignEnvelopeInformation.DocumentPDFs) {
                if (docuSignEnvelopeInformation.DocumentPDFs.DocumentPDF) {
                    response.DocumentBytes = docuSignEnvelopeInformation.DocumentPDFs.DocumentPDF.PDFBytes;
                }
            }
        }
    }
    return response;
}