function verification_rule_auotomaticfraud(payload) {
	try {
		var result = 'Passed';
		var minBankruptcy = 2;
		var Data = null;
		var objData = null;
		var distinctCaseNumbers = [];
		var Current = null;
		var CurrentBankruptcyCount = 0;
		if (payload != null && payload != undefined) {
			var objPrimary = payload.bankruptcyReportRecords;
			var objPrimary = null;
			if (payload.bankruptcyReportRecords != null && payload.bankruptcyReportRecords.length > 0) {
				if (payload.bankruptcyReportRecords[0] && payload.bankruptcyReportRecords[0].Recordcount == 0) {
					objPrimary = payload.bankruptcyReportRecords[0];
					return {
						'result': result,
						'detail': null,
						'data': {
							'referenceNumber': objPrimary.referenceNumber
						},
						'rejectcode': '',
						'exception': []
					};
				} else if (payload.bankruptcyReportRecords[0] != null && payload.bankruptcyReportRecords[0].length > 0) {
					objPrimary = payload.bankruptcyReportRecords[0][0];
				}
			}
			Data = {
				'referenceNumber': objPrimary.referenceNumber
			};
			if (objPrimary != null) {
				objData = objPrimary.Records.BankruptcyData;
				if (objData) {
					for (var i = 0; i < objData.length; i++) {
						if (objData[i] && distinctCaseNumbers.indexOf(objData[i].CaseNumber) < 0) {
							distinctCaseNumbers.push(objData[i].CaseNumber);
						}
					}
				}
			}
			if (distinctCaseNumbers && distinctCaseNumbers.length >= minBankruptcy) {
				result = 'Failed';
			} else {
				if (objData[0].Datefiled != null) {
					Current = GetMonthDiff(new Date(objData[0].Datefiled), new Date());
					if (Current <= 12) {
						result = 'Failed';
					}
				}
			}
		}
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	}
	function GetMonthDiff(d1, d2) {
		var months;
		months = (d2.getFullYear() - d1.getFullYear()) * 12;
		months -= d1.getMonth() + 1;
		months += d2.getMonth();
		return months <= 0 ? 0 : months;
	}
}
