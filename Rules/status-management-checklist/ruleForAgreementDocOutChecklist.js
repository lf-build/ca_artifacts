function ruleForAgreementDocOutChecklist(payload) {
	var factService = this.call('factVerification');
	function getVerificationFactDetails(input, factName) {
		for (var i = 0; i < input.length; i++) {
			if (input[i]['factName'] == factName) {
				return input[i];
			}
		}
	}
	if (typeof(payload) != 'undefined') {
		return factService.get(payload.entityType, payload.entityId).then(function (response) {
			var result = 'Failed';
			var counter = 0;
			var factList = ['WelcomeCallVerification', 'SignedDocumentVerification'];
			for (var i = 0; i < factList.length; i++) {
				var cashflowVerification = getVerificationFactDetails(response, factList[i]);
				if (typeof(cashflowVerification) != 'undefined') {
					if (cashflowVerification.currentStatus == 'Completed') {
						counter = counter + 1;
					}
				}
			}
			if (counter == factList.length) {
				return {
					'result': 'Passed',
					'detail': ''
				};
			} else {
				return {
					'result': 'Failed',
					'detail': 'Verification Pending'
				};
			}
		}).catch (function (error) {
			return {
				'result': 'Failed',
				'detail': error.message
			};
		});
	} else {
		return {
			'result': 'Failed',
			'detail': 'Verification Pending'
		};
	}
}