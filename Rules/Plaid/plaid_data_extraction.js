function plaid_data_extraction(input) {
    if (input) {
        return {
            'PublicToken': input.PublicToken,
            'BankSupportedProductType': input.BankSupportedProductType,
            'AccountId': input.AccountId,
            'EntityId': input.EntityId,
			'EntityType': input.EntityType,
			'InstitutionId': input.InstitutionId,
            'InstitutionName': input.InstitutionName
			
        };
    }
    return null;
}