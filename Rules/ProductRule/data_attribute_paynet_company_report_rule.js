function data_attribute_paynet_company_report_rule(payload) {
	var master_score = null;
	var master_score_percentile = null;
	var master_score_key_factor_1 = null;
	var master_score_key_factor_2 = null;
	var master_score_key_factor_3 = null;
	var primary_equipment_type_code = null;
	var primary_equipment_type_desc = null;
	var oldest_contract_start_date = null;
	var last_activity_reported_date = null;
	var high_credit_amt = null;
	var avg_high_credit_per_lender_amt = null;
	var avg_orig_term_months = null;
	var cur_bal_amt = null;
	var result = 'Passed';
	var records = null;
	if (typeof(payload) != 'undefined') {
		var response = payload.eventData.Response;
		var report_data = response.xmlField.ReportData;
		master_score = report_data.MasterScore;
		master_score_percentile = report_data.MasterScorePercentile;
		master_score_key_factor_1 = report_data.MasterScoreKeyFactor_1;
		master_score_key_factor_2 = report_data.MasterScoreKeyFactor_2;
		master_score_key_factor_3 = report_data.MasterScoreKeyFactor_3;
		primary_equipment_type_code = report_data.PrimaryEquipmentTypeCode;
		primary_equipment_type_desc = report_data.PrimaryEquipmentTypeDesc;
		oldest_contract_start_date = report_data.OldestContractStartDate;
		last_activity_reported_date = report_data.LastActivityReportedDate;
		high_credit_amt = report_data.HighCreditAmt;
		avg_high_credit_per_lender_amt = report_data.AvgHighCreditPerLenderAmt;
		avg_orig_term_months = report_data.AvgOrigTermMonths;
		cur_bal_amt = report_data.CurBalAmt;
		records = {
			'master_score': master_score,
			'master_score_percentile': master_score_percentile,
			'master_score_key_factor_1': master_score_key_factor_1,
			'master_score_key_factor_2': master_score_key_factor_2,
			'master_score_key_factor_3': master_score_key_factor_3,
			'primary_equipment_type_code': primary_equipment_type_code,
			'primary_equipment_type_desc': primary_equipment_type_desc,
			'oldest_contract_start_date': oldest_contract_start_date,
			'last_activity_reported_date': last_activity_reported_date,
			'high_credit_amt': high_credit_amt,
			'avg_high_credit_per_lender_amt': avg_high_credit_per_lender_amt,
			'avg_orig_term_months': avg_orig_term_months,
			'cur_bal_amt': cur_bal_amt
		};
	}
	var Data = {
		'paynetCompanyReport': records
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
