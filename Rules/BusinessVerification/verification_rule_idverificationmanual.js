function verification_rule_idverificationmanual(payload) {

    var drivingLicenceMatch = false;
    var nameMatch = false;
    var dateOfBirthMatch = false;
    var result = 'Failed';

    function VerifyInput(payload) {
        try {
            if (payload != null && payload != undefined) {
                var objData = payload.idVerificationData[0];
                drivingLicenceMatch = objData.drivingLicenceMatch;
                nameMatch = objData.nameMatch;
                dateOfBirthMatch = objData.dateOfBirthMatch;

                if (drivingLicenceMatch == true && nameMatch == true && dateOfBirthMatch == true) {
                    result = 'Passed';
                }
            }

            var Data = {
                'nameMatch': nameMatch,
                'drivingLicenceMatch': drivingLicenceMatch,
                'dateOfBirthMatch': dateOfBirthMatch
            };

            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': 'Unable to verify',
                'data': objData,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    return VerifyInput(payload);
}