function get_industry_lookup(payload) {

    if (typeof (payload) != 'undefined') {
		
	/*put validation by lookuo by */
		
     var industryLookp = payload.industryLook_Attribute[0];
     var input = payload.application[0]; 
	 var industryList = [];
	  for (var i = 0; i < industryLookp.data.length; i++) {
		  
		  var currentData= industryLookp.data[i];
		  if(currentData.IndustryId  == parseInt(input.industry))
		  {
			  industryList.push(currentData);
			  break;
		  }
		  
	  }
	 
        var records = {
            'Industry': industryList
        };
        return {
            'result': 'Passed',
            'detail': null,
            'data': records,
            'rejectcode': '',
            'exception': []
        };
    }
    return {
        'result': 'Failed',
        'detail': null,
        'data': '',
        'rejectcode': '',
        'exception': []
    };
}