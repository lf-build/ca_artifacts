function CalculateStopAutoPay(payload) {
	if (payload == null || payload.Metrics == null || payload.Metrics.Metrics == null || payload.EventData == null) {
		return false;
	}
	if (payload.Metrics.Metrics.length > 0) {
		for (var i = 0; i < payload.Metrics.Metrics.length; i++) {
			if (payload.Metrics.Metrics[i].Name == 'SuccessivePaymentMissed') {
				if (payload.Metrics.Metrics[i].Value >= 2) {
					return true;
				}
			}
		}
	}
	if (parseInt(payload.EventData.SuccessiveFailCount) >= 2) {
		return true;
	}
	return false;
}
