function GetCalculatedOffer(payload) {
	try {
		if (typeof (payload) != 'undefined') {
			var NumberOfPayment = this.call('CalculateNumberOfPayment', [payload.eventData.typeOfPayment, payload.eventData.term]);
			var countTotalPayback = this.call('CalculateTotalPayback', [payload.eventData.loanAmount, payload.eventData.sellRate]);
			var countOriginatingfees = 0;
			if (payload.eventData.originatingFeeAmount.isEdited) {
				countOriginatingfees = payload.eventData.originatingFeeAmount.amount;
			} else {
				countOriginatingfees = this.call('CalculateOriginatingfees', [payload.eventData.loanAmount, payload.eventData.originatingFeePer, payload.eventData.originatingFeeMinLoanAmount, payload.eventData.fixedOriginatingFee]);
			}
			var offerModel = {
				_NumberOfPayment: NumberOfPayment,
				_calculatePaymentAmount: this.call('CalculatePaymentAmount', [countTotalPayback, NumberOfPayment]),
				_countTotalPayback: countTotalPayback,
				_computeGMCommision: this.call('CalculateGMCommision', [payload.eventData.buyRate, payload.eventData.sellRate, payload.eventData.loanAmount]),
				_computeFundingPoints: this.call('CalculateFundingPoints', [payload.eventData.sellRate, payload.eventData.buyRate]),
				_countOriginatingfees: countOriginatingfees,
				_computeExpectedYield: this.call('CalculateExpectedYield', [payload.eventData.loanAmount, countTotalPayback]),
				_computeFundedAmount: this.call('CalculateFundedAmount', [payload.eventData.approvedAmount, countOriginatingfees, payload.eventData.achFee])
			};
			return {
				'result': 'Passed',
				'detail': null,
				'data': offerModel,
				'rejectcode': '',
				'exception': []
			};
		}
		return {
			'result': 'Failed',
			'detail': null,
			'data': '',
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		var msg = 'Error occured :' + e.message;
		var errorData = [];
		errorData.push(msg);
		return {
			'result': 'Failed',
			'rejectcode': '',
			'detail': null,
			'data': null,
			'exception': errorData
		};
	}
}
