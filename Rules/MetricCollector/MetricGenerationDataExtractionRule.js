function MetricGenerationDataExtractionRule(payload) {
	var result = 'Passed';
	var zipCode = '';
	if (typeof (payload) != 'undefined') {
		if (payload.eventData.EventName == 'ApplicationCreated') {
			var filterData = payload.eventData.Data;
			if (filterData == null) {
				var Data = null;
			} else {
				var Data = {
					'facts': {
						'applicationCount': 1
					},
					'dimensions': {
						'businessType': filterData.BusinessType,
						'industryType': filterData.Industry,
						'partnerId': filterData.PartnerId,
						'purposeOfLoan': filterData.PurposeOfLoan,
						'zipCode': filterData.BusinessZipCode,
						'applicationDate': filterData.ApplicationDate.Time
					},
					'applicationDate': filterData.ApplicationDate
				}
			}
		} else if (payload.eventData.EventName == 'ApplicationFunded') {
			var filterData = payload.eventData.Data;
			if (filterData == null) {
				var Data = null;
			} else {
				var Data = {
					'facts': {
						'fundedAmount': filterData.AmountFunded,
						'commissionAmount': filterData.CommissionAmount,
						'fundedApplicationCount': 1
					},
					'dimensions': {
						'businessType': filterData.BusinessType,
						'industryType': filterData.Industry,
						'partnerId': filterData.PartnerId,
						'purposeOfLoan': filterData.PurposeOfLoan,
						'zipCode': filterData.BusinessZipCode,
						'fundingDate': filterData.FundingRequestedDate.Time
					},
					'applicationDate': filterData.ApplicationDate
				}
			}
		} else if (payload.eventData.EventName == 'ApplicationRejected') {
			var filterData = payload.eventData.Data;
			if (filterData == null) {
				var Data = null;
			} else {
				var Data = {
					'facts': {
						'applicationCount': 1
					},
					'dimensions': {
						'declineReason': filterData.DeclineReason[0],
						'applicationDate': filterData.ApplicationDate.Time
					},
					'applicationDate': filterData.ApplicationDate
				}
			}
		}
	} else {
		result = 'Failed';
		detail = 'payload is undefined';
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}