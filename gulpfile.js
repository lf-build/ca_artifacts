var gulp = require('gulp');
var replace = require('gulp-replace');
var argv = require('yargs').argv;
var path = require('path')
var fs = require('fs');
var ObjectID = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient, format = require('util').format;


var currentFile = argv.file;
var configuration = argv.configuration;
var config = require("./" + configuration + '.json');
var api = require('./deployment/api_module.js')(config.authorizationToken);
var ruleSvc = require('./deployment/ruleService.js')(config.authorizationToken);
var templateSvc = require('./deployment/templateService.js')(config.authorizationToken);
var lookupSvc = require('./deployment/lookupService.js')(config.authorizationToken);
var configurationSvc = require('./deployment/configurationService.js')(config.authorizationToken);

var serviceUrl = config.baseUrl + ":"

gulp.task('publishConfiguration', function () {
    console.log('Service Url:' + serviceUrl + config.configurationConfiguration.servicePort);
    return configurationSvc.saveConfiguration(serviceUrl + config.configurationConfiguration.servicePort, currentFile);
});

gulp.task('importConfiguration', function () {
    console.log('Service Url:' + serviceUrl + config.configurationConfiguration.servicePort);
    return configurationSvc.importConfiguration(serviceUrl + config.configurationConfiguration.servicePort, currentFile);
});

gulp.task('publishRule', function () {
    console.log('Service Url:' + serviceUrl + config.ruleConfiguration.servicePort);
    var pathObject = path.parse(currentFile);
    if (pathObject.ext.toLocaleLowerCase() == ".js") {
        console.log('Publishing Open Rule ' + currentFile);
        var data = fs.readFileSync(currentFile, 'utf8');
        return ruleSvc.saveRule(serviceUrl + config.ruleConfiguration.servicePort, data);
    }
    else {
        console.warn('Skipping file ' + currentFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
});

gulp.task('importRules', function () {
    console.log('Service Url:' + serviceUrl + config.ruleConfiguration.servicePort);
    return ruleSvc.importRules(serviceUrl + config.ruleConfiguration.servicePort, currentFile);
});

gulp.task('publishLookup', function () {
    console.log('Service Url:' + serviceUrl + config.lookupServiceConfiguration.servicePort);
    console.log('Publishing Open Lookup ' + currentFile);
    return lookupSvc.saveLookup(serviceUrl + config.lookupServiceConfiguration.servicePort, currentFile);
});

gulp.task('importLookup', function () {
    console.log('Service Url:' + serviceUrl + config.lookupServiceConfiguration.servicePort);
    console.log('Publishing Open Lookup ' + currentFile);
    return lookupSvc.importLookup(serviceUrl + config.lookupServiceConfiguration.servicePort, currentFile);
});

gulp.task('publishTemplate', function () {
    console.log('Service Url:' + serviceUrl + config.templateConfiguration.servicePort);
    console.log('Publishing Open Template ' + currentFile);
    return templateSvc.saveTemplate(serviceUrl + config.templateConfiguration.servicePort, currentFile);
});

gulp.task('importTemplates', function () {
    console.log('Service Url:' + serviceUrl + config.templateConfiguration.servicePort);
    console.log('Publishing Open Template ' + currentFile);
    return templateSvc.importTemplates(serviceUrl + config.templateConfiguration.servicePort, currentFile);
});

gulp.task('userActivate', function () {

    return new Promise(function (resolve, reject) {
        api.get(serviceUrl + config.identityServiceConfiguration.servicePort + '/all/users').then(function (result) {
            if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                for (let i = 0; i < result.body.length; i++) {
                    var data = { username: result.body[i].username, isactive: true };
                    //return new Promise(function (resolve, reject) {
                        api.put(serviceUrl + config.identityServiceConfiguration.servicePort + '/activate-deactivate', data).then(function (result) {
                            if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                                console.log("Updated for user: " + data.username);
                            }
                            else {
                                console.log("Failed to activate");
                            }
                        });
                    //});
                }
            }
            else {
                console.log("Failed to activate users");
            }
        });
    });
});

gulp.task('importGeoProfileData', function () {
    var pathObject = path.parse(currentFile);
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        console.log('Publishing Open Geo Data file ' + currentFile);
        var name = pathObject.name;
        var data = fs.readFileSync(currentFile, 'utf8');
        if (data != undefined) {
            MongoClient.connect('mongodb://' + config.internalIp + ':27017/geoprofile', function (err, db) {
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {
                    data[i]._id = (new ObjectID()).toString();
                    insertDocument(db, data[i], function () {
                        db.close();
                    });
                }
            });
        }
        return data;
    }
    return true;
});

var insertDocument = function (db, data, callback) {
    db.collection('geopincoderanking').insertOne(data, function (err, result) {
        console.log("Inserted a document into the geopincoderanking collection.");
        callback();
    });
};

gulp.task('loadTesting', function () {
    console.log('Service Url:' + serviceUrl + config.ruleConfiguration.servicePort);
    var pathObject = path.parse(currentFile);
    if (pathObject.ext.toLocaleLowerCase() == ".js") {
        console.log('Publishing Open Rule ' + currentFile);
        var data = fs.readFileSync(currentFile, 'utf8');
        return ruleSvc.saveRule(serviceUrl + config.ruleConfiguration.servicePort, data);
    }
    else {
        console.warn('Skipping file ' + currentFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
});