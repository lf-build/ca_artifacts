function data_attribute_primary(payload) {
	var result = 'Passed';
	var Data = [];
	
	if (typeof(payload) != 'undefined') {
		if (payload.application != null && payload.application[0].owners != null && payload.application[0].owners.length > 0) {
			var objPrimaryOwner = payload.application[0].owners.filter(function (o) {
					return o.IsPrimary == true;
				});

			if (objPrimaryOwner != null && objPrimaryOwner.length > 0) {				
				Data.push(objPrimaryOwner[0].OwnerId);
			}
		}
	}

	return {
		'result': result,
		'detail': Data,
		'data': null,
		'rejectcode': '',
		'exception': []
	};
}