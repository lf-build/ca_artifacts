function data_attribute_bbbsearchorganizationrequestedrule(payload) {
	var result = 'Passed';
	var ratingIcons = [];
	var businessURLs = '';
	var organizationType = '';
	var altOrganizationNames = '';
	var organizationLastChanged = '';
	var ratingLastChanged = '';
	var accreditationStatusLastChanged = '';
	var organizationName = '';
	var phones = [];
	var reportURL = '';
	if (typeof (payload) != 'undefined'
		&& payload.eventData &&
		payload.eventData.Response
		&& payload.eventData.Response
		&& payload.eventData.Response.SearchResults
		&& payload.eventData.Response.SearchResults.length > 0) {
		var bbbsearchorganizationResponse = payload.eventData.Response.SearchResults[0];
		if (bbbsearchorganizationResponse) {
			organizationType = bbbsearchorganizationResponse.OrganizationType;
			ratingIcons = bbbsearchorganizationResponse.RatingIcons;
			businessURLs = bbbsearchorganizationResponse.BusinessURLs;
			altOrganizationNames = bbbsearchorganizationResponse.AltOrganizationNames;
			organizationLastChanged = bbbsearchorganizationResponse.OrganizationLastChanged;
			ratingLastChanged = bbbsearchorganizationResponse.RatingLastChanged;
			accreditationStatusLastChanged = bbbsearchorganizationResponse.AccreditationStatusLastChanged;
			organizationName = bbbsearchorganizationResponse.OrganizationName;
			phones = bbbsearchorganizationResponse.Phones;
			reportURL = bbbsearchorganizationResponse.ReportURL;
		}
	}
	var Data = {
		'bbbReport': {
			'OrganizationType': organizationType,
			'RatingIcons': ratingIcons,
			'BusinessURLs': businessURLs,
			'AltOrganizationNames': altOrganizationNames,
			'OrganizationLastChanged': organizationLastChanged,
			'RatingLastChanged': ratingLastChanged,
			'AccreditationStatusLastChanged': accreditationStatusLastChanged,
			'OrganizationName': organizationName,
			'Phones': phones,
			'ReportURL': reportURL
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
