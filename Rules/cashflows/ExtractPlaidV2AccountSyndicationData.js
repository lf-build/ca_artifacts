function ExtractPlaidV2AccountSyndicationData(input) {
	var errorData = [];
	var UseSimulation = false;
	var Items = {
		'available_products': [],
		'billed_products': [],
		'error': null,
		'institution_id': '',
		'item_id': '',
		'request_id': '',
		'total_transactions': 0
	};
	try {
		var lstAccounts = [];
		var data = {};
		if (input != null) {
			if (input.Items && input.Items != null) {
				Items.available_products = input.Items.available_products;
				Items.billed_products = input.Items.billed_products;
				Items.error = input.Items.error;
				Items.institution_id = input.Items.institution_id;
				Items.item_id = input.Items.item_id;
			}
			if (input.requestId && input.requestId != null) {
				Items.request_id = input.requestId;
			}
			if (input.totalTransactions && input.totalTransactions != null) {
				Items.total_transactions = input.totalTransactions;
			}
			var SelectedAccountId = input.SelectedAccountId;
			UseSimulation = input.UseSimulation;
			if (input.Accounts != null && input.Accounts.length > 0) {
				input.Accounts.forEach(function (account) {
					var objAccount = {
						'ProviderAccountId': '',
						'AvailableBalance': null,
						'CurrentBalance': null,
						'BankName': '',
						'AccountType': '',
						'AccountNumber': '',
						'Source': '',
						'BalanceAsOfDate': '',
						'NameOnAccount': '',
						'RoutingNumber': '',
						'AccountId': '',
						'EntityId': '',
						'OfficialAccountName': '',
						'IsCashflowAccount': false,
						'IsFundingAccount': false
					};
					objAccount.Source = 'Plaid';
					objAccount.OfficialAccountName = account.official_name;
					objAccount.NameOnAccount = account.name;
					objAccount.ProviderAccountId = account.account_id;
					objAccount.BankName = input.Items.institution_id;
					objAccount.AccountType = account.subtype;
					if (account.mask != null) {
						objAccount.AccountNumber = account.mask;
					}
					if (account.balances != null) {
						if (account.balances.Available != undefined && account.balances.Available != null) {
							objAccount.AvailableBalance = account.balances.Available;
						}
						if (account.balances.Current != undefined && account.balances.Current != null) {
							objAccount.CurrentBalance = account.balances.Current;
						}
					}
					if (SelectedAccountId == account.account_id) {
						objAccount.IsCashflowAccount = true;
						objAccount.IsFundingAccount = true;
					}
					lstAccounts.push(objAccount);
				});
			}
			if (UseSimulation) {
				lstAccounts[0].IsCashflowAccount = true;
				lstAccounts[0].IsFundingAccount = true;
			}
			data = {
				Accounts: lstAccounts,
				Items: Items
			};
		}
	} catch (e) {
		return null;
	}
	return data;
};