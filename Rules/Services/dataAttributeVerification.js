function dataAttributeVerification() {
    var baseUrl = 'http://data-attributes:5000';
    var self = this;
    return {
        get: function (entityType, entityId, attributeName) {
            var url = [baseUrl, entityType, entityId, attributeName].join('/');
            return new Promise(function (resolve, reject) {
                try {
                    self.http.get(url).catch(function (error) {
                        reject('Data Attributes not Found for1 ' + entityType + ':' + entityId);
                    })
                        .then(function (response) {
                            resolve(response);
                        });
                } catch (exception) {
                    reject('Data Attributes not Found for2 ' + entityType + ':' + entityId + ':' + exception);
                }
            });
        }
    };
}