function get_fedchex_records(input) {
    function formatDate(date) {
        if (typeof date != undefined) {
            var dateParts = date.split('-');
            var returnValue = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
            return returnValue;
        } else {
            return '';
        }
    }
    var errorData = [];
    try {
        var customAccountType = 'BC';
        if (input.DataAttributes.AccountType.toLowerCase() == 'savings') {
            customAccountType = 'BS';
        }
        var data = {
            'Header': 'TestHeader',
            'TransactionType': input.DataAttributes.TransactionType,
            'SECCode': input.DataAttributes.SECCode,
            'ClientName': input.DataAttributes.ClientName,
            'ABANumber': input.DataAttributes.ABANumber,
            'AccountNumber': input.DataAttributes.AccountNumber,
            'AccountType': customAccountType,
            'Amount': input.DataAttributes.Amount.toString().replace('.', ''),
            'ReferenceNumber': input.DataAttributes.ReferenceNumber,
            'CompanyEntryDescription': input.DataAttributes.CompanyEntryDescription
        };
    } catch (e) {
        return null;
    }
    return data;
};