function Compute_bp2_Score(payload) {

    try {
        if (typeof (payload) != 'undefined') {

            var weightedScore = 0;
            var diff_xb_ALL6203 = '';
            var diff_xb_REH3423 = '';
            var diff_xb_MTF6244 = '';
            var diff_xb_IQT9536 = '';
            var diff_xb_ILN7310 = '';
            var diff_xb_BUS0416 = '';
            var diff_xb_AUL5020 = '';
            var diff_xb_AUA6280 = '';
            var diff_xb_ALM6160 = '';
            var diff_xb_ALL8323 = '';
            var diff_xb_action = '';
            var diff_xb_RTC037 = '';
            var diff_xb_CLC006 = '';
            var diffDictionary = [];
            for (var propertyName in payload) {
                var propertyValue = payload[propertyName];
                if (typeof (propertyValue[0].WeightedScore) != 'undefined') {
                    var propertyWeightedScore = parseFloat(propertyValue[0].WeightedScore);
                    weightedScore += propertyWeightedScore;

                    if (propertyName == 'm_ALL6203_Attribute') {
                        diff_xb_ALL6203 = propertyWeightedScore - (-0.008371653);
                        diffDictionary.push({
                            key: 'diff_xb_ALL6203', value: diff_xb_ALL6203,
                            description: 'Worst ever status on the most recently opened trade, excluding collections including in determinates'
                        });
                    }
                    else if (propertyName == 'm_REH3423_Attribute') {
                        diff_xb_REH3423 = propertyWeightedScore - 0.089735534;
                        diffDictionary.push({
                            key: 'diff_xb_REH3423', value: diff_xb_REH3423,
                            description: 'Total number of open revolving trades with a balance to credit amount ratio >= 100 reported in the last 6 months excluding home equity line of credit trades'
                        });
                    }
                    else if (propertyName == 'm_MTF6244_Attribute') {
                        diff_xb_MTF6244 = propertyWeightedScore - (-0.029805488);
                        diffDictionary.push({
                            key: 'diff_xb_MTF6244', value: diff_xb_MTF6244,
                            description: 'Worst ever status on a first mortgage trade opened in 2004 including in determinates'
                        });
                    }
                    else if (propertyName == 'm_IQT9536_Attribute') {
                        diff_xb_IQT9536 = propertyWeightedScore - (-0.291246931);
                        diffDictionary.push({
                            key: 'diff_xb_IQT9536', value: diff_xb_IQT9536,
                            description: 'Average number of days between inquiries made in the last 6 months (no deduplication)'
                        });
                    }
                    else if (propertyName == 'm_ILN7310_Attribute') {
                        diff_xb_ILN7310 = propertyWeightedScore - 0.234722045;
                        diffDictionary.push({
                            key: 'diff_xb_ILN7310', value: diff_xb_ILN7310,
                            description: 'Percentage of all trades excluding collections that are open installment trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_BUS0416_Attribute') {
                        diff_xb_BUS0416 = propertyWeightedScore - (-0.086019087);
                        diffDictionary.push({
                            key: 'diff_xb_BUS0416', value: diff_xb_BUS0416,
                            description: 'Total number of open personal liable business loan, line of credit, or credit card trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_AUL5020_Attribute') {
                        diff_xb_AUL5020 = propertyWeightedScore - (-0.116712036);
                        diffDictionary.push({
                            key: 'diff_xb_AUL5020', value: diff_xb_AUL5020,
                            description: 'Total balance on open auto lease trades reported in the last 6 months'
                        });
                    }
                    else if (propertyName == 'm_AUA6280_Attribute') {
                        diff_xb_AUA6280 = propertyWeightedScore - (-0.014769982);
                        diffDictionary.push({
                            key: 'diff_xb_AUA6280', value: diff_xb_AUA6280,
                            description: 'Worst ever status on an auto loan or lease trade in the last 24 months including in determinates'
                        });
                    }
                    else if (propertyName == 'm_ALM6160_Attribute') {
                        diff_xb_ALM6160 = propertyWeightedScore - (-0.02478382);
                        diffDictionary.push({
                            key: 'diff_xb_ALM6160', value: diff_xb_ALM6160,
                            description: 'Worst present status on a trade reported in the last 6 months including non-medical collections and in determinates'
                        });
                    }
                    else if (propertyName == 'm_ALL8323_Attribute') {
                        diff_xb_ALL8323 = propertyWeightedScore - (-0.411425527);
                        diffDictionary.push({
                            key: 'diff_xb_ALL8323', value: diff_xb_ALL8323,
                            description: 'Average number of months since ever 90 or more days delinquent or derogatory trades were opened excluding collections including in determinates'
                        });
                    }
                    else if (propertyName == 'm_action_high_Attribute') {
                        diff_xb_action = propertyWeightedScore - 0.053561222;
                        diffDictionary.push({
                            key: 'diff_xb_action', value: diff_xb_action,
                            description: 'This is the action message that you have set up for the score range.'
                        });
                    }
                    else if (propertyName == 'm_RTC037_Attribute') {
                        diff_xb_RTC037 = propertyWeightedScore - 0.053561222;
                        diffDictionary.push({
                            key: 'diff_xb_RTC037', value: diff_xb_RTC037,
                            description: 'Total number of continuous trades where worst delinquency = current'
                        });
                    }
                    else if (propertyName == 'm_CLC006_Attribute') {
                        diff_xb_CLC006 = propertyWeightedScore - 0.033584112;
                        diffDictionary.push({
                            key: 'diff_xb_CLC006', value: diff_xb_CLC006,
                            description: 'Total number of open or closed unpaid collection trades'
                        });
                    }
                }
            }

            if (diffDictionary != null && diffDictionary.length > 0) {
                diffDictionary.sort(function (a, b) {
                    return b.value - a.value;
                });
            }
            var topReasonCounter = 4;
            var topReasons = diffDictionary.slice(0, topReasonCounter);

            var bp2_score = 0;
            var bp2_xbeta = -1.5010789050 + weightedScore;
            bp2_score = Math.round(650 - 28.8539 * bp2_xbeta, 1);
            var bp2_prob = Math.exp(bp2_xbeta) / (1 + Math.exp(bp2_xbeta));
            var records = {
                'bp2_prob': bp2_prob,
                'bp2_score': bp2_score,
                'top_reasons': topReasons,
                'all_reasons': diffDictionary
            };

            for (var i = 0; i < topReasons.length; i++) {
                var key = 'factor-' + (i + 1);
                records[key] = topReasons[i].description;
            }

            return {
                'result': 'Passed',
                'detail': null,
                'data': records,
                'rejectcode': '',
                'exception': []
            };
        }
        return {
            'result': 'Failed',
            'detail': null,
            'data': '',
            'rejectcode': '',
            'exception': []
        };
    }
    catch (e) {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };

    }

}