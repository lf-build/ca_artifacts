function verification_rule_businesstaxidverificationmanual(payload) {
    var result = 'Failed';
    var Data = {};
    if (payload && payload.businessTaxIdVerificationManualData) {
        Data = payload.businessTaxIdVerificationManualData[0];
        if (Data && Data.pass == true) {
            result = 'Passed';
        }
    }
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}