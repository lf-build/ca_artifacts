function verification_rule_welcomecall(payload) {
	var objData = {};
	try {
		var result = 'Failed';
		if (payload != null && payload != undefined) {

			objData = payload.welcomeCallVerificationData[0];
			if (objData != null && objData != '' && objData.IsWelcomeCallDone == true) {
				result = 'Passed';
			}
		}

		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': 'Unable to verify',
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}