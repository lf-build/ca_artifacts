function DeclineEmailData(payload) {

    if (payload && payload.eventData) {
        if (payload.eventData.EventData) {
            var applicationData = payload.eventData.DataAttributes.application[0];
            var reasonsList = payload.eventData.EventData.Response;
            var reasonsArr = [];
            var email = '';

            var objPrimaryOwner = applicationData.owners.filter(function (item) {
                if (item.IsPrimary == true) { return item; }
            });

            var primaryOwner = null;
            if (objPrimaryOwner != null && objPrimaryOwner.length > 0) {
                primaryOwner = objPrimaryOwner[0];
            }

            if (primaryOwner != null && primaryOwner.EmailAddresses[0]) {
                email = primaryOwner.EmailAddresses[0].Email;
            }

            for (var i = 0; i < reasonsList.length; i++) {
                reasonsArr.push(reasonsList[i]);
            }
            var reasons = reasonsArr.join(', ');
            var result = {
                Email: email,
                Reasons: reasons,
                Logo: 'https://s3-us-west-2.amazonaws.com/lf-staging-ca-assets/CALogo/ca_logo.png'
            };
            return {
                'result': 'Passed',
                'detail': null,
                'data': result,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    var errorData = [];
    errorData.push('Data Not found for email');
    return {

        'result': 'Failed',
        'detail': null,
        'data': '',
        'rejectcode': '',
        'exception': errorData
    };
}
