function data_attribute_yelpbusinessdetailrequestedrule(payload) {
	var result = 'Passed';
	var rating = '';
	var isClosed = null;
	var category = null;
	var name = null;
	var phone = null;
	var url = null;
	var reviewCount = null;
	var address = [];
	var city = null;
	if (typeof (payload) != 'undefined' && payload.eventData.Response
		&& payload.eventData.Response.Businesses
		&& payload.eventData.Response.Businesses.length > 0) {
		var yelpbusinessdetailResponse = payload.eventData.Response.Businesses[0];
		rating = yelpbusinessdetailResponse.Rating;
		isClosed = yelpbusinessdetailResponse.IsClosed;
		if (yelpbusinessdetailResponse.Categories != null && yelpbusinessdetailResponse.Categories.length > 0) {
			category = yelpbusinessdetailResponse.Categories[0][0];
		}
		name = yelpbusinessdetailResponse.Name;
		phone = yelpbusinessdetailResponse.Phone;
		url = yelpbusinessdetailResponse.Url;
		reviewCount = yelpbusinessdetailResponse.ReviewCount;
		address = yelpbusinessdetailResponse.Location.Address;
		city = yelpbusinessdetailResponse.Location.City;
	}
	var Data = {
		'yelpReport': {
			'Rating': rating,
			'IsClosed': isClosed,
			'Category': category,
			'Name': name,
			'Phone': phone,
			'Url': url,
			'ReviewCount': reviewCount,
			'Address': address,
			'City': city
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
