function ExtractPlaidAccountSyndicationData(input) {

	var errorData = [];
	try {
		var lstAccounts = [];
		var data = {};
		if (input != null) {
			var SelectedAccountId = input.SelectedAccountId;
			if (input.Accounts != null && input.Accounts.length > 0) {
				input.Accounts.forEach(function (account) {

					var objAccount = {
						'ProviderAccountId': '',
						'AvailableBalance': null,
						'CurrentBalance': null,
						'BankName': '',
						'AccountType': '',
						'AccountNumber': '',
						'Source': '',
						'BalanceAsOfDate': '',
						'NameOnAccount': '',
						'RoutingNumber': '',
						'AccountId': '',
						'EntityId': '',
						'IsCashflowAccount': false,
						'IsFundingAccount': false
					};
					objAccount.Source = 'Plaid';
					objAccount.ProviderAccountId = account.Id;
					objAccount.BankName = account.InstitutionType;
					objAccount.AccountType = account.SubType;
					if (account.AccountMeta != null) {
						objAccount.AccountNumber = account.AccountMeta.Number;
					}
					if (account.Balance != null) {
						if (account.Balance.Available != undefined && account.Balance.Available != null) {
							objAccount.AvailableBalance = account.Balance.Available;
						}
						if (account.Balance.Current != undefined && account.Balance.Current != null) {
							objAccount.CurrentBalance = account.Balance.Current;
						}
					}
					if (SelectedAccountId == account.Id) {
						objAccount.IsCashflowAccount = true;
						objAccount.IsFundingAccount = true;
					}
					lstAccounts.push(objAccount);
				});
			}

			data = {
				Accounts: lstAccounts
			};
		}
	} catch (e) {
		return null;
	}
	return data;
};