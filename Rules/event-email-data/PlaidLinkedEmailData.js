function PlaidLinkedEmailData(payload) {
	if (payload && payload.eventData.DataAttributes) {
		if (payload.eventData.DataAttributes.application[0]) {
			var applicationNumber = payload.eventData.DataAttributes.application[0].applicationNumber;
			var eventdata = payload.eventData.EventData;
			var result = {
				Email: 'businesslending@sigmainfo.net',
				applicationNumber: applicationNumber
			};
			return {
				'result': 'Passed',
				'detail': null,
				'data': result,
				'rejectcode': '',
				'exception': []
			};
		}
	}
	var errorData = [];
	errorData.push('Data Not found for email');
	return {
		'result': 'Failed',
		'detail': null,
		'data': '',
		'rejectcode': '',
		'exception': errorData
	};
}