function data_attribute_computepscore(payload) {

	function ComputePScore(payload) {
		var m_ILN7310 = 0;
		var m_IQT9536 = 0;
		var m_AUL5020 = 0;
		var m_BUS0416 = 0;
		var m_ALL8323 = 0;
		var m_REH3423 = 0;
		var m_BCC5320 = 0;
		var arrALL6203 = [1, 90];
		var arrALM6160 = [1, 997];
		var arrAUA6200_range1 = [400];
		var arrAUA6200_range2 = [1, 30];
		var arrMTF6244_range1 = [1, 400];
		var arrMTF6244_range2 = [994];
		var m_ALL6203 = 0.035777717;
		var m_ALM6160 = -0.369743806;
		var m_AUA6200 = -0.172544475;
		var m_MTF6244 = -0.290543237;

		if (typeof(payload) != 'undefined') {			
			var objExperianPersonal = payload.experianPersonalReport[0];
			if (objExperianPersonal != null) {
				if (objExperianPersonal.ILN7310 == 999)
					m_ILN7310 = 40;
				else if (objExperianPersonal.ILN7310 == 998)
					m_ILN7310 = 6;
				else if (objExperianPersonal.ILN7310 == 994)
					m_ILN7310 = 0;
				else if (objExperianPersonal.ILN7310 > 40)
					m_ILN7310 = 40;
				else if (objExperianPersonal.ILN7310 < 0)
					m_ILN7310 = 0;
				else if (objExperianPersonal.ILN7310 >= 0 && objExperianPersonal.ILN7310 <= 40)
					m_ILN7310 = objExperianPersonal.ILN7310;
				else {
					m_ILN7310 = 40;
				}
				if (objExperianPersonal.IQT9536 == 994)
					m_IQT9536 = 30;
				else if (objExperianPersonal.IQT9536 > 90)
					m_IQT9536 = 90;
				else if (objExperianPersonal.IQT9536 < 0)
					m_IQT9536 = 0;
				else if (objExperianPersonal.IQT9536 >= 0 && objExperianPersonal.IQT9536 <= 90)
					m_IQT9536 = objExperianPersonal.IQT9536;
				else {
					m_IQT9536 = 0;
				}
				if (objExperianPersonal.AUL5020 == 999999999)
					m_AUL5020 = 0;
				else if (objExperianPersonal.AUL5020 == 999999998)
					m_AUL5020 = 0;
				else if (objExperianPersonal.AUL5020 == 999999997)
					m_AUL5020 = 18000;
				else if (objExperianPersonal.AUL5020 == 999999996)
					m_AUL5020 = 7000;
				else if (objExperianPersonal.AUL5020 > 50000)
					m_AUL5020 = 50000;
				else if (objExperianPersonal.AUL5020 < 0)
					m_AUL5020 = 0;
				else if (objExperianPersonal.AUL5020 >= 0 && objExperianPersonal.AUL5020 <= 50000)
					m_AUL5020 = objExperianPersonal.AUL5020;
				else {
					m_AUL5020 = 0;
				}
				if (objExperianPersonal.BUS0416 == 99)
					m_BUS0416 = 0;
				else if (objExperianPersonal.BUS0416 == 98)
					m_BUS0416 = 0;
				else if (objExperianPersonal.BUS0416 > 1)
					m_BUS0416 = 1;
				else if (objExperianPersonal.BUS0416 < 0)
					m_BUS0416 = 0;
				else if (objExperianPersonal.BUS0416 >= 0 && objExperianPersonal.BUS0416 <= 1)
					m_BUS0416 = objExperianPersonal.BUS0416;
				else {
					m_BUS0416 = 0;
				}
				if (objExperianPersonal.ALL8323 == 9999)
					m_ALL8323 = 90;
				else if (objExperianPersonal.ALL8323 == 9994)
					m_ALL8323 = 200;
				else if (objExperianPersonal.ALL8323 > 200)
					m_ALL8323 = 200;
				else if (objExperianPersonal.ALL8323 < 30)
					m_ALL8323 = 30;
				else if (objExperianPersonal.ALL8323 >= 30 && objExperianPersonal.ALL8323 <= 200)
					m_ALL8323 = objExperianPersonal.ALL8323;
				else {
					m_ALL8323 = 30;
				}
				if (objExperianPersonal.REH3423 == 99)
					m_REH3423 = 2;
				else if (objExperianPersonal.REH3423 == 98)
					m_REH3423 = 3;
				else if (objExperianPersonal.REH3423 == 97)
					m_REH3423 = 3;
				else if (objExperianPersonal.REH3423 > 3)
					m_REH3423 = 3;
				else if (objExperianPersonal.REH3423 < 0)
					m_REH3423 = 0;
				else if (objExperianPersonal.REH3423 >= 0 && objExperianPersonal.REH3423 <= 3)
					m_REH3423 = objExperianPersonal.REH3423;
				else {
					m_REH3423 = 3;
				}
				if (objExperianPersonal.BCC5320 == 999999999)
					m_BCC5320 = 0;
				else if (objExperianPersonal.BCC5320 == 999999998)
					m_BCC5320 = 0;
				else if (objExperianPersonal.BCC5320 == 999999997)
					m_BCC5320 = 0;
				else if (objExperianPersonal.BCC5320 == 999999996)
					m_BCC5320 = 80000;
				else if (objExperianPersonal.BCC5320 > 80000)
					m_BCC5320 = 80000;
				else if (objExperianPersonal.BCC5320 < 0)
					m_BCC5320 = 0;
				else if (objExperianPersonal.BCC5320 >= 0 && objExperianPersonal.BCC5320 <= 80000)
					m_BCC5320 = objExperianPersonal.BCC5320;
				else {
					m_BCC5320 = 0;
				}
				if (arrALL6203.indexOf(objExperianPersonal.ALL6203) > -1) {
					m_ALL6203 = -1.238895901;
				}
				if (arrALM6160.indexOf(objExperianPersonal.ALM6160) > -1) {
					m_ALM6160 = 0.368271862;
				}
				if (arrAUA6200_range1.indexOf(objExperianPersonal.AUA6200) > -1) {
					m_AUA6200 = -0.495447982;
				} else if (arrAUA6200_range2.indexOf(objExperianPersonal.AUA6200) > -1) {
					m_AUA6200 = 0.169560032;
				}
				if (arrMTF6244_range1.indexOf(objExperianPersonal.MTF6244) > -1) {
					m_MTF6244 = 1.025948864;
				} else if (arrMTF6244_range2.indexOf(objExperianPersonal.MTF6244) > -1) {
					m_MTF6244 = 0.074812558;
				}
				var diff_xb_ALL6203 = (-0.6798630820 * m_ALL6203) - (-0.008437071);
				var diff_xb_ALL8323 = (-0.0030273840 * m_ALL8323) - (-0.440456622);
				var diff_xb_ALM6160 = (-0.5549757270 * m_ALM6160) - (-0.028293696);
				var diff_xb_AUA6200 = (-0.4143972070 * m_AUA6200) - (-0.008724728);
				var diff_xb_AUL5020 = (-0.0000299741 * m_AUL5020) - (-0.124599338);
				var diff_xb_BCC5320 = (-0.0000034441 * m_BCC5320) - (-0.074639828);
				var diff_xb_BUS0416 = (-0.4938503510 * m_BUS0416) - (-0.100463939);
				var diff_xb_ILN7310 = (0.0256960260 * m_ILN7310) - (0.260181761);
				var diff_xb_IQT9536 = (-0.0108665440 * m_IQT9536) - (-0.294373456);
				var diff_xb_MTF6244 = (-0.6323012650 * m_MTF6244) - (-0.031898509);
				var diff_xb_REH3423 = (0.1345556110 * m_REH3423) - (0.082993201);
				var p_xbeta = -1.4317297640 + (0.0256960260 * m_ILN7310) + (-0.0108665440 * m_IQT9536) + (-0.6323012650 * m_MTF6244) + (-0.0000299741 * m_AUL5020) + (-0.5549757270 * m_ALM6160) + (-0.6798630820 * m_ALL6203) + (-0.4938503510 * m_BUS0416) + (-0.0030273840 * m_ALL8323) + (0.1345556110 * m_REH3423) + (-0.4143972070 * m_AUA6200) + (-0.0000034441 * m_BCC5320);
				var p_prob = Math.exp(p_xbeta) / (1 + Math.exp(p_xbeta));
				var p_score = Math.round(650 - 28.8539 * p_xbeta, 1);
				var p_grade = 20;
				var objGradeList = GetGradeLookup();
				for (var i = 0; i < objGradeList.GradeLookup.length; i++) {
					if (p_score >= objGradeList.GradeLookup[i].ScoreFrom && p_score <= objGradeList.GradeLookup[i].ScoreTo) {
						p_grade = objGradeList.GradeLookup[i].Grade;
						break;
					}
				}
				var Data = {
					'm_ILN7310': m_ILN7310,
					'm_IQT9536': m_IQT9536,
					'm_AUL5020': m_AUL5020,
					'm_BUS0416': m_BUS0416,
					'm_ALL8323': m_ALL8323,
					'm_REH3423': m_REH3423,
					'm_BCC5320': m_BCC5320,
					'm_ALL6203': m_ALL6203,
					'm_ALM6160': m_ALM6160,
					'm_AUA6200': m_AUA6200,
					'm_MTF6244': m_MTF6244,
					'p_score': p_score,
					'p_grade': p_grade,
					'diff_xb_ALL6203': diff_xb_ALL6203,
					'diff_xb_ALL8323': diff_xb_ALL8323,
					'diff_xb_ALM6160': diff_xb_ALM6160,
					'diff_xb_AUA6200': diff_xb_AUA6200,
					'diff_xb_AUL5020': diff_xb_AUL5020,
					'diff_xb_BCC5320': diff_xb_BCC5320,
					'diff_xb_BUS0416': diff_xb_BUS0416,
					'diff_xb_ILN7310': diff_xb_ILN7310,
					'diff_xb_IQT9536': diff_xb_IQT9536,
					'diff_xb_MTF6244': diff_xb_MTF6244,
					'diff_xb_REH3423': diff_xb_REH3423,
					'p_xbeta': p_xbeta,
					'p_prob': p_prob
				};
				return {
					'Result': true,
					'Data': Data
				};
			}
		}
		return {
			'Result': 'Failed',
			'Data': ''
		};
	}
	function GetGradeLookup() {
		return {
			'GradeLookup': [{
					'ScoreFrom': 748,
					'ScoreTo': 100000,
					'Grade': 1
				}, {
					'ScoreFrom': 740,
					'ScoreTo': 747,
					'Grade': 2
				}, {
					'ScoreFrom': 733,
					'ScoreTo': 739,
					'Grade': 3
				}, {
					'ScoreFrom': 729,
					'ScoreTo': 732,
					'Grade': 4
				}, {
					'ScoreFrom': 725,
					'ScoreTo': 728,
					'Grade': 5
				}, {
					'ScoreFrom': 721,
					'ScoreTo': 724,
					'Grade': 6
				}, {
					'ScoreFrom': 717,
					'ScoreTo': 720,
					'Grade': 7
				}, {
					'ScoreFrom': 714,
					'ScoreTo': 719,
					'Grade': 8
				}, {
					'ScoreFrom': 711,
					'ScoreTo': 713,
					'Grade': 9
				}, {
					'ScoreFrom': 708,
					'ScoreTo': 710,
					'Grade': 10
				}, {
					'ScoreFrom': 705,
					'ScoreTo': 707,
					'Grade': 11
				}, {
					'ScoreFrom': 702,
					'ScoreTo': 704,
					'Grade': 12
				}, {
					'ScoreFrom': 698,
					'ScoreTo': 701,
					'Grade': 13
				}, {
					'ScoreFrom': 694,
					'ScoreTo': 697,
					'Grade': 14
				}, {
					'ScoreFrom': 691,
					'ScoreTo': 693,
					'Grade': 15
				}, {
					'ScoreFrom': 687,
					'ScoreTo': 690,
					'Grade': 16
				}, {
					'ScoreFrom': 681,
					'ScoreTo': 686,
					'Grade': 17
				}, {
					'ScoreFrom': 676,
					'ScoreTo': 680,
					'Grade': 18
				}, {
					'ScoreFrom': 667,
					'ScoreTo': 679,
					'Grade': 19
				}
			]
		}
	}
	return ComputePScore(payload);
}