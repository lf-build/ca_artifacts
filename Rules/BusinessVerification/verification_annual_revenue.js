function AnnualRevenueVerificationRule(payload) {
    var result = 'Passed';
    var AnnualRevenue = 0;
    if (payload && payload.application) {
        var input = payload.application[0];
        if (input) {
            AnnualRevenue = input.annualRevenue;
            var MinAnnualRevenue = 100000;
            if (AnnualRevenue < MinAnnualRevenue) {
                result = 'Failed';
            }
        }
    }
    var Data = {
        'AnnualRevenue': AnnualRevenue
    };
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}
