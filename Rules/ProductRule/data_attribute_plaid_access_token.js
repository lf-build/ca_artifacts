function data_attribute_plaid_access_token(payload) {

	function GetFormattedAccessToken(accessToken) {
		return accessToken;
	}

	function ExtractAccessToken(payload) {
		var result = 'Failed';
		var Data = {};
		try {
			if (typeof (payload) != 'undefined') {
				var accessTokenInput = payload.eventData.Response;
				if (accessTokenInput != null) {
					result = 'Passed';
					var resultKey = 'AccessToken-' + GetFormattedAccessToken(accessTokenInput.AccessToken);
					Data[resultKey] = accessTokenInput.AccessToken;

					return {
						'result': result,
						'detail': null,
						'data': Data,
						'rejectcode': '',
						'exception': []
					};
				}
			}
			return {
				'result': result,
				'detail': null,
				'data': Data,
				'rejectcode': '',
				'exception': []
			};
		}
		catch (e) {
			return {
				'result': result,
				'detail': null,
				'data': null,
				'exception': [e.message],
				'rejectCode': ''
			};

		}		
	}
	
	return ExtractAccessToken(payload);
}