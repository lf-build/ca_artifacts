function ExtractCSVData(payload) {

	var errorData = [];
	try {
		var lstAccounts = [];
		var lstTransactions = [];
		var data = {};
		if (payload != null) {

			var details = payload.details;
			var data = payload.data;
			var availableBalance = 0;
			var ProviderAccountId = '';
			
			if (details != null) {
				ProviderAccountId = details.accountNumber;
			}

			if (data != null && data.length > 0) {
				data.forEach(function (transaction) {
					var objTransaction = {
						'ProviderAccountId': '',
						'AccountId': '',
						'Amount': 0,
						'Categories': '',
						'CategoryId': '',
						'Description': '',
						'Meta': '',
						'Pending': '',
						'TransactionDate': ''
					};

					objTransaction.ProviderAccountId = ProviderAccountId;
					objTransaction.Amount = transaction.Amount;
					objTransaction.TransactionDate = transaction.Date;
					objTransaction.Description = transaction.Name;
					objTransaction.CategoryId = transaction.CategoryId;
					if (availableBalance == 0) {
						availableBalance = transaction.Available;
					}
					lstTransactions.push(objTransaction);
				});
			}
			if (details != null) {

				var objAccount = {
					'ProviderAccountId': '',
					'AvailableBalance': 0,
					'CurrentBalance': 0,
					'BankName': '',
					'AccountType': '',
					'AccountNumber': '',
					'Source': ''
				};
				objAccount.AccountNumber = details.accountNumber;
				objAccount.Source = 'CSV';
				objAccount.ProviderAccountId = details.accountNumber;
				objAccount.BankName = details.instituteName;
				objAccount.AccountType = details.accountType;

				objAccount.AvailableBalance = availableBalance;
				objAccount.CurrentBalance = availableBalance;
				lstAccounts.push(objAccount);
			}

			data = {
				Accounts: lstAccounts,
				Transactions: lstTransactions
			};
		}
	} catch (e) {
		return null;
	}
	return data;
};