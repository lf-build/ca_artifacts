function verification_rule_plaidcashflow(payload) {

	var result = 'Passed';
	var IsAverageBalance = true;
	var IsAverageDepositSize = true;
	var IsNumberOfDeposit = true;

	var MinAverageDailyBalance = 1000;
	var MinNumberOfDeposit = 5;
	var MinAverageDepositSize = 1500;

	var AccountID = '', InstitutionName = '', AccountType = '';

	var errorData = [];
	var errorMessage;
	var Data = {};

	if (payload != null) {
		try {
			var SelectedAccountData = payload.cashflowVerificationData[0].SelectedAccountData;
			if (SelectedAccountData != null) {
				var selectedCashflow = SelectedAccountData[0];
				if (selectedCashflow != null) {
					AccountID = selectedCashflow.AccountNumber;
					InstitutionName = selectedCashflow.InstitutionName;
					AccountType = selectedCashflow.AccountType;

					if (selectedCashflow.CashFlow != null && selectedCashflow.CashFlow.TransactionSummary != null) {
						var input = selectedCashflow.CashFlow.TransactionSummary;
						if (input != null) {
							if (input.AverageDailyBalance <= MinAverageDailyBalance) {
								IsAverageBalance = false;
								result = 'Failed';
							}
							if (input.AverageDeposit <= MinAverageDepositSize) {
								IsAverageDepositSize = false;
								result = 'Failed';
							}
							if (input.TotalCreditsCount <= MinNumberOfDeposit) {
								IsNumberOfDeposit = false;
								result = 'Failed';
							}

							Data = {
								'AccountID': AccountID,
								'InstitutionName': InstitutionName,
								'AccountType': AccountType,
								'IsAverageBalance': IsAverageBalance,
								'IsAverageDepositSize': IsAverageDepositSize,
								'IsNumberOfDeposit': IsNumberOfDeposit
							};
						}
					}
				}
				else {
					errorMessage = 'plaidCashflow information is Null : Unable to verify';
					errorData.push(errorMessage);
					result = 'Failed';
				}

			} else {
				errorMessage = 'plaidCashflow information is Null : Unable to verify';
				errorData.push(errorMessage);
				result = 'Failed';
			}
		} catch (e) {

			errorMessage = 'Unable to verify : ' + e.message;
			errorData.push(errorMessage);
			result = 'Failed';

		}
	} else {
		result = 'Failed';
		errorMessage = 'Payload is not available';
		errorData.push(errorMessage);
	}

	return {
		'result': result,
		'detail': '',
		'data': Data,
		'rejectcode': '',
		'exception': errorData
	};
}
