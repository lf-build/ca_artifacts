function verification_rule_fraudmanual(payload) {
    var result = 'Failed';
    var Data = {};
    if (payload && payload.bankruptcyVerificationData) {
        Data = payload.bankruptcyVerificationData[0];
        if (Data && Data.pass == true) {
            result = 'Passed';
        }
    }
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}