function GeneratePlaidTransactionCashFlow(payload) {
	var Plaidmonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var result = 'Passed';
	var Category_Payroll = '21009000';
	var Category_OnlineTransafer = '21001000';
	var Category_LoanPayment = '16003000';
	var Category_NSF = '10007000';
	var Category_SkipDeposit = ['21000000', '21001000'];
	var Category_Revenue = ['21002000', '21004000', '21005000', '21007000', '21007001', '21007002', '21010000', '21010001', '21010002', '21010003', '21010004', '21010005', '21010006', '21010007', '21010008', '21010009', '21010010', '21010011', '16001000', '21012000', '21012001', '21012002'];
	var endingBalance = 0;
	var beginingBalance = 0;
	var StartDate;
	var OfficialName = '';
	var NameOnAccount = '';
	var EndDate;
	var NegativeBalanceCount = 0;
	var allAccountsCashflows = {};
	var DataAttributePostFix = '';
	var allowedCategory = ['21009000', '21002000', '10001000', '16003000', '10007000', '21001000'];
	var ExcludeWeekEnds = false;
	var addMissingMonths = true;
	var calculateRecurringTransaction = true;
	var self = this;
	var cashflowService = self.call('cashflow');
	if (typeof (payload) != 'undefined') {
		var entityId = payload.entityId;
		var entityType = payload.entityType;
		var cashFlowInput = payload.eventData;
		var account = cashFlowInput.Accounts;
		return cashflowService.getTransactions(entityType, entityId, account.Id).then(function (allTrans) {
			var AllTransactions = [];
			AllTransactions = allTrans.transaction;
			var myCustomCashFlow = {
				'MonthlyCashFlows': [{}],
				'TransactionSummary': {},
				'CategorySummary': [{}],
				'TransactionList': [{}],
				'RecurringList': [{}],
				'MCARecurringList': [{}]
			};
			var PlaidCashFlowViewModel = {
				'AccountHeader': '',
				'AccountID': '',
				'OfficialName': '',
				'NameOnAccount': '',
				'AccountNumber': '',
				'InstitutionName': '',
				'AccountType': '',
				'referenceNumber': '',
				'IsSelected': false,
				'Source': '',
				'CashFlow': myCustomCashFlow
			};
			var cashFlowResult = PlaidCashFlowViewModel;
			endingBalance = account.CurrentBalance;
			beginingBalance = endingBalance;
			var SelectedTransactions = [];
			var MonthlyCashFlowsList = [];
			var CategorySummaryList = [];
			var TransactionList = [];
			var RecurringList = [];
			var MCARecurringList = [];
			var LastFourDigitAccountNumber = account.AccountNumber;
			if (account.AccountNumber != null) {
				LastFourDigitAccountNumber = account.AccountNumber.substr(-4);
				LastFourDigitAccountNumber = 'BS-' + LastFourDigitAccountNumber;
			}
			if (AllTransactions != null && AllTransactions.length > 0) {
				SelectedTransactions = AllTransactions.filter(function (trans) {
					return trans.providerAccountId == account.ProviderAccountId;
				});
				if (SelectedTransactions != null && SelectedTransactions.length > 0) {
					var totalTransactions = SelectedTransactions.length;
					SelectedTransactions.sort((a, b) => {
						var start = +new Date(a.transactionDate);
						var elapsed = +new Date(b.transactionDate) - start;
						return elapsed;
					});
					if (calculateRecurringTransaction == true) {
						GetRecurringSummary(account.Id, SelectedTransactions, function (trs) {
							RecurringList = trs;
						});
						GetRecurringSummaryByCategoryId(account.Id, SelectedTransactions, function (trs) {
							MCARecurringList = trs;
						});
					}
					GetCategorySummary(SelectedTransactions, function (s) {
						CategorySummaryList = s;
					});
					EndDate = SelectedTransactions[0].transactionDate;
					StartDate = SelectedTransactions[totalTransactions - 1].transactionDate;
					OfficialName = account.OfficialAccountName;
					NameOnAccount = account.NameOnAccount;
					var maxTransactionDate = new Date(EndDate);
					var currentMonth = maxTransactionDate.getMonth();
					var currentYear = maxTransactionDate.getFullYear();
					var lastMonth = currentMonth + '-' + currentYear;
					var minTransactionDate = new Date(StartDate);
					var firstMonth = minTransactionDate.getMonth() + '-' + minTransactionDate.getFullYear();
					var isFirstTransaction = true;
					var isLastMonth = true;
					var dateOfLastTransaction;
					var TotalDailyBalanceArray = new Array();
					TotalDailyBalanceArray.push(beginingBalance);
					var TotalDailyDepositArray = new Array();
					var TotalBrokerAGSDepositArray = new Array();
					var AGS_Month = 3;
					var Last_N_Month = GetLastCalendarMonth(AGS_Month);
					var AGSDeposit_N_Month = 0;
					while (SelectedTransactions.length > 0) {
						var currentMonthTransactions = SelectedTransactions.filter(function (item) {
							if (new Date(item.transactionDate).getMonth() == currentMonth && new Date(item.transactionDate).getFullYear() == currentYear) {
								return item;
							}
						});
						if (addMissingMonths == true) {
							if ((currentMonthTransactions == null || currentMonthTransactions == undefined) || (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length == 0)) {
								var tempTransaction = {
									'categoryId': 0,
									'amount': 0,
									'transactionDate': (currentMonth + 1) + '/01/' + currentYear,
									'IsDummyRecord': true
								};
								currentMonthTransactions = [];
								currentMonthTransactions.push(tempTransaction);
							}
						}
						if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {
							var PlaidMonthlyCashFlowViewModel = {
								Name: '',
								Year: 0,
								BeginingBalance: 0.00,
								EndingBalance: 0.00,
								DepositCount: 0,
								WithdrawalCount: 0,
								TotalDepositAmount: 0.00,
								TotalWithdrawalAmount: 0.00,
								AverageDailyBalance: 0.00,
								FirstTransactionDate: '',
								EndTransactionDate: '',
								MinDepositAmount: 0.00,
								MaxDepositAmount: 0.00,
								MinWithdrawalAmount: 0.00,
								MaxWithdrawalAmount: 0.00,
								NumberOfNSF: 0,
								NSFAmount: 0.00,
								LoanPaymentAmount: 0.00,
								NumberOfLoanPayment: 0,
								PayrollAmount: 0.00,
								NumberOfPayroll: 0,
								NumberOfNegativeBalance: 0.00,
								CustomAttributes: '',
								DaysBelow100Count: 0,
								TotalMonthlyRevenueAmount: 0.00,
								TotalMonthlyRevenueCount: 0,
								PDFReportName: ''
							};
							var currentMonthCashFlow = PlaidMonthlyCashFlowViewModel;
							var monthlyDailyBalance = new Array(31);
							var MinDepositAmount = 0,
								MaxDepositAmount = 0,
								MinWithdrawalAmount = 0,
								MaxWithdrawalAmount = 0;
							var DepositCount = 0,
								WithdrawalCount = 0,
								TotalMonthlyDepositAmount = 0,
								TotalMonthlyWithdrawalAmount = 0;
							var TotalMonthlyLoanPaymentAmount = 0,
								TotalMonthlyLoanPaymentCount = 0;
							var TotalMonthlyPayrollAmount = 0,
								TotalMonthlyPayrollCount = 0;
							var TotalMonthlyNSFAmount = 0,
								TotalMonthlyNSFCount = 0;
							var TotalMonthlyRevenueAmount = 0,
								TotalMonthlyRevenueCount = 0;
							var TotalMonthlyNegativeBalanceCount = 0;
							currentMonthCashFlow.Name = Plaidmonths[currentMonth];
							currentMonthCashFlow.Year = currentYear;
							var monthInNumber = (parseInt(currentMonth) + 1).toString();
							monthInNumber = monthInNumber.length > 1 ? monthInNumber : '0' + monthInNumber;
							currentMonthCashFlow.PDFReportName = LastFourDigitAccountNumber + '-' + monthInNumber + '-' + currentYear + '.pdf';
							currentMonthCashFlow.EndingBalance = PlaidcustomRound(beginingBalance, 2);
							var index = -1;
							currentMonthTransactions.forEach(function (objTransaction) {
								if (objTransaction != undefined) {
									if (objTransaction.categoryId != undefined) {
										if (objTransaction.categoryId == Category_Payroll) {
											TotalMonthlyPayrollAmount = TotalMonthlyPayrollAmount + objTransaction.amount;
											TotalMonthlyPayrollCount = TotalMonthlyPayrollCount + 1;
											currentMonthCashFlow.PayrollAmount = Math.abs(TotalMonthlyPayrollAmount);
											currentMonthCashFlow.NumberOfPayroll = TotalMonthlyPayrollCount;
										} else if (objTransaction.categoryId == Category_NSF) {
											TotalMonthlyNSFAmount = TotalMonthlyNSFAmount + objTransaction.amount;
											TotalMonthlyNSFCount = TotalMonthlyNSFCount + 1;
											currentMonthCashFlow.NSFAmount = TotalMonthlyNSFAmount;
											currentMonthCashFlow.NumberOfNSF = TotalMonthlyNSFCount;
										} else if (objTransaction.categoryId == Category_LoanPayment) {
											TotalMonthlyLoanPaymentAmount = TotalMonthlyLoanPaymentAmount + objTransaction.amount;
											TotalMonthlyLoanPaymentCount = TotalMonthlyLoanPaymentCount + 1;
											currentMonthCashFlow.LoanPaymentAmount = TotalMonthlyLoanPaymentAmount;
											currentMonthCashFlow.NumberOfLoanPayment = TotalMonthlyLoanPaymentCount;
										} else if (Category_Revenue.indexOf(objTransaction.categoryId) > -1) {
											TotalMonthlyRevenueAmount = TotalMonthlyRevenueAmount + objTransaction.amount;
											TotalMonthlyRevenueCount = TotalMonthlyRevenueCount + 1;
											currentMonthCashFlow.TotalMonthlyRevenueAmount = Math.abs(TotalMonthlyRevenueAmount);
											currentMonthCashFlow.TotalMonthlyRevenueCount = TotalMonthlyRevenueCount;
										}
									}
									var currentTransactionDate = new Date(objTransaction.transactionDate);
									var day = currentTransactionDate.getDate();
									var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (getFormattedDate(dateOfLastTransaction) != getFormattedDate(currentTransactionDate));
									if (calculateDailyEnding) {
										endingBalance = beginingBalance;
										monthlyDailyBalance[day - 1] = endingBalance;
										if (endingBalance < 0) {
											NegativeBalanceCount++;
											TotalMonthlyNegativeBalanceCount++;
										}
									}
									dateOfLastTransaction = currentTransactionDate;
									var transactionAmount = objTransaction.amount;
									if (transactionAmount < 0) {
										beginingBalance -= Math.abs(transactionAmount);
										TotalDailyDepositArray.push(Math.abs(transactionAmount));
										var tempDate = new Date(objTransaction.transactionDate).getMonth() + '-' + new Date(objTransaction.transactionDate).getFullYear();
										if (Last_N_Month.indexOf(tempDate) > -1) {
											TotalBrokerAGSDepositArray.push(Math.abs(transactionAmount));
										}
										if (transactionAmount < MaxDepositAmount || MaxDepositAmount == 0) {
											MaxDepositAmount = transactionAmount;
										}
										if (transactionAmount > MinDepositAmount || MinDepositAmount == 0) {
											MinDepositAmount = transactionAmount;
										}
										DepositCount = DepositCount + 1;
										TotalMonthlyDepositAmount = TotalMonthlyDepositAmount + transactionAmount;
									} else if (transactionAmount > 0) {
										beginingBalance += transactionAmount;
										if (transactionAmount > MaxWithdrawalAmount || MaxWithdrawalAmount == 0) {
											MaxWithdrawalAmount = transactionAmount;
										}
										if (transactionAmount < MinWithdrawalAmount || MinWithdrawalAmount == 0) {
											MinWithdrawalAmount = transactionAmount;
										}
										WithdrawalCount = WithdrawalCount + 1;
										TotalMonthlyWithdrawalAmount = TotalMonthlyWithdrawalAmount + transactionAmount;
									}
									isFirstTransaction = false;
									index = -1;
									index = SelectedTransactions.indexOf(objTransaction, 0);
									if (index > -1) {
										SelectedTransactions.splice(index, 1);
									}
									if (totalTransactions > 1 && index > -1) {
										TotalDailyBalanceArray.push(beginingBalance);
									}
								}
								if (index > -1) {
									totalTransactions -= 1;
								}
							});
							currentMonthCashFlow.NumberOfNegativeBalance = PlaidcustomRound(TotalMonthlyNegativeBalanceCount, 2);
							currentMonthCashFlow.TotalWithdrawalAmount = PlaidcustomRound(TotalMonthlyWithdrawalAmount, 2);
							currentMonthCashFlow.WithdrawalCount = WithdrawalCount;
							currentMonthCashFlow.TotalDepositAmount = PlaidcustomRound(Math.abs(TotalMonthlyDepositAmount), 2);
							currentMonthCashFlow.DepositCount = DepositCount;
							currentMonthCashFlow.MinDepositAmount = Math.abs(MinDepositAmount);
							currentMonthCashFlow.MaxDepositAmount = Math.abs(MaxDepositAmount);
							currentMonthCashFlow.MinWithdrawalAmount = MinWithdrawalAmount;
							currentMonthCashFlow.MaxWithdrawalAmount = MaxWithdrawalAmount;
							if (currentMonthTransactions[0].IsDummyRecord == undefined) {
								currentMonthCashFlow.FirstTransactionDate = PlaidcustomDate(dateOfLastTransaction);
								if (currentMonthTransactions.length > 0) {
									currentMonthCashFlow.EndTransactionDate = PlaidcustomDate(new Date(currentMonthTransactions[0].transactionDate));
								}
							}
							currentMonthCashFlow.BeginingBalance = PlaidcustomRound(beginingBalance, 2);
							var totalDaysInmonth = 0;
							totalDaysInmonth = plaidGetDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear());
							var monthCheck = currentMonth + '-' + currentYear;
							if (firstMonth == lastMonth) {
								currentMonthCashFlow.AverageDailyBalance = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
							} else if (lastMonth == monthCheck) {
								currentMonthCashFlow.AverageDailyBalance = CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
							} else if (firstMonth == monthCheck) {
								currentMonthCashFlow.AverageDailyBalance = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
							} else {
								currentMonthCashFlow.AverageDailyBalance = CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
							}
							currentMonthCashFlow.DaysBelow100Count = GetDaysBelow100Count(monthlyDailyBalance);
							MonthlyCashFlowsList.push(PlaidsortObject(currentMonthCashFlow));
							isLastMonth = false;
							if (Last_N_Month.indexOf(monthCheck) > -1 && TotalBrokerAGSDepositArray != null && TotalBrokerAGSDepositArray.length > 0) {
								AGSDeposit_N_Month = AGSSum(TotalBrokerAGSDepositArray);
							}
						}
						currentMonth -= 1;
						if (currentMonth < 0) {
							currentYear -= 1;
							currentMonth = 11;
						}
					}
					var PlaidTransactionSummaryViewModel = {
						CountOfMonthlyStatement: 0,
						OfficialName: '',
						NameOnAccount: '',
						StartDate: '',
						EndDate: '',
						AverageDeposit: 0,
						AnnualCalculatedRevenue: 0.00,
						AverageWithdrawal: 0,
						AverageDailyBalance: 0,
						NumberOfNegativeBalance: 0,
						NumberOfNSF: 0,
						NSFAmount: 0,
						LoanPaymentAmount: 0,
						NumberOfLoanPayment: 0,
						PayrollAmount: 0,
						NumberOfPayroll: 0,
						ChangeInDepositVolume: 0,
						TotalCredits: 0,
						TotalDebits: 0,
						TotalCreditsCount: 0,
						TotalDebitsCount: 0,
						AvailableBalance: 0,
						CurrentBalance: 0,
						AverageBalanceLastMonth: 0,
						MedianMonthlyIncome: 0,
						MedianDailyBalance: 0,
						MaxDaysBelow100Count: 0,
						CVOfDailyDeposit: 0,
						CVOfDailyBalance: 0,
						AverageMonthlyRevenue: 0.00,
						TotalRevenueAmount: 0.00,
						TotalRevenueCount: 0,
						PDFReportName: '',
						BrokerAGS: 0,
						CAPAGS: 0,
						BrokerAGSText: ''
					};
					var objTransactionSummary = PlaidTransactionSummaryViewModel;
					objTransactionSummary.PDFReportName = LastFourDigitAccountNumber + '.pdf';
					objTransactionSummary.CountOfMonthlyStatement = MonthlyCashFlowsList.length;
					objTransactionSummary.StartDate = getFormattedDate(StartDate);
					objTransactionSummary.OfficialName = OfficialName;
					objTransactionSummary.NameOnAccount = NameOnAccount;
					objTransactionSummary.EndDate = getFormattedDate(EndDate);
					var TotalRevenueAmount = 0,
						TotalRevenueCount = 0;
					var TotalDepositAmount = 0;
					var TotalWithdrawalAmount = 0;
					var TotalDailyAverageBalance = 0;
					var TotalPayrollAmount = 0,
						TotalLoanPaymentAmount = 0,
						TotalNSFAmount = 0;
					var TotalPayrollCount = 0,
						TotalLoanPaymentCount = 0,
						TotalNSFCount = 0;
					var TotalDebitsCount = 0,
						TotalCreditsCount = 0;
					var TotalMonthlyIncomeArray = new Array();
					MonthlyCashFlowsList.forEach(function (cashflow) {
						TotalRevenueAmount += cashflow.TotalMonthlyRevenueAmount;
						TotalRevenueCount += cashflow.TotalMonthlyRevenueCount;
						TotalDepositAmount += cashflow.TotalDepositAmount;
						TotalWithdrawalAmount += cashflow.TotalWithdrawalAmount;
						TotalDailyAverageBalance += cashflow.AverageDailyBalance;
						TotalPayrollAmount += cashflow.PayrollAmount;
						TotalLoanPaymentAmount += cashflow.LoanPaymentAmount;
						TotalNSFAmount += cashflow.NSFAmount;
						TotalPayrollCount += cashflow.NumberOfPayroll;
						TotalLoanPaymentCount += cashflow.NumberOfLoanPayment;
						TotalNSFCount += cashflow.NumberOfNSF;
						TotalCreditsCount += cashflow.DepositCount;
						TotalDebitsCount += cashflow.WithdrawalCount;
						TotalMonthlyIncomeArray.push(cashflow.TotalDepositAmount);
					});
					objTransactionSummary.TotalRevenueAmount = PlaidcustomRound((Math.abs(TotalRevenueAmount)), 2);
					objTransactionSummary.TotalRevenueCount = TotalRevenueCount;
					objTransactionSummary.AverageMonthlyRevenue = PlaidcustomRound((TotalRevenueAmount / MonthlyCashFlowsList.length), 2);
					objTransactionSummary.MedianMonthlyIncome = GetMedianSingleArray(TotalMonthlyIncomeArray);
					objTransactionSummary.MedianDailyBalance = GetMedianSingleArray(TotalDailyBalanceArray);
					objTransactionSummary.MaxDaysBelow100Count = GetMaxDaysBelow100(MonthlyCashFlowsList);
					objTransactionSummary.CVOfDailyBalance = PlaidcustomRound(DailyCoEfficient(TotalDailyBalanceArray), 2);
					objTransactionSummary.CVOfDailyDeposit = PlaidcustomRound(DailyCoEfficient(TotalDailyDepositArray), 2);
					objTransactionSummary.TotalCredits = TotalDepositAmount;
					objTransactionSummary.TotalDebits = TotalWithdrawalAmount;
					objTransactionSummary.TotalCreditsCount = TotalCreditsCount;
					objTransactionSummary.TotalDebitsCount = TotalDebitsCount;
					objTransactionSummary.AvailableBalance = account.AvailableBalance;
					objTransactionSummary.CurrentBalance = account.CurrentBalance;
					objTransactionSummary.AverageBalanceLastMonth = MonthlyCashFlowsList[0].AverageDailyBalance;
					objTransactionSummary.AverageDeposit = PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length), 2);
					objTransactionSummary.AnnualCalculatedRevenue = PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length) * 12, 2);
					objTransactionSummary.AverageWithdrawal = PlaidcustomRound((TotalWithdrawalAmount / MonthlyCashFlowsList.length), 2);
					objTransactionSummary.AverageDailyBalance = PlaidcustomRound((TotalDailyAverageBalance / MonthlyCashFlowsList.length), 2);
					objTransactionSummary.NumberOfNegativeBalance = NegativeBalanceCount;
					objTransactionSummary.NumberOfNSF = TotalNSFCount;
					objTransactionSummary.NSFAmount = PlaidcustomRound(TotalNSFAmount, 2);
					objTransactionSummary.LoanPaymentAmount = PlaidcustomRound((TotalLoanPaymentAmount), 2);
					objTransactionSummary.NumberOfLoanPayment = TotalLoanPaymentCount;
					objTransactionSummary.NumberOfPayroll = TotalPayrollCount;
					objTransactionSummary.PayrollAmount = PlaidcustomRound((Math.abs(TotalPayrollAmount)), 2);
					if (AGSDeposit_N_Month != 0) {
						var Broker_AverageDepositVolume = AGSDeposit_N_Month / AGS_Month;
						objTransactionSummary.BrokerAGS = PlaidcustomRound(Broker_AverageDepositVolume * 12, 2);
					}
					var BrokerAGSMonthlyLimit = 4;
					if (MonthlyCashFlowsList.length < BrokerAGSMonthlyLimit) {
						objTransactionSummary.BrokerAGS = 0;
						objTransactionSummary.BrokerAGSText = 'Less than 3 months data';
					}
					if (TotalDailyDepositArray != null && TotalDailyDepositArray.length > 0) {
						var CAP_AverageDepositVolume = AGSSum(TotalDailyDepositArray) / MonthlyCashFlowsList.length;
						objTransactionSummary.CAPAGS = PlaidcustomRound(CAP_AverageDepositVolume * 12, 2);
					}
					var t = myCustomCashFlow;
					t.MonthlyCashFlows = MonthlyCashFlowsList;
					t.CategorySummary = CategorySummaryList;
					t.TransactionList = TransactionList;
					t.RecurringList = RecurringList;
					t.MCARecurringList = MCARecurringList;
					var objTemp = PlaidsortObject(objTransactionSummary);
					t.TransactionSummary = objTemp;
					cashFlowResult.CashFlow = t;
				} else {
					var PlaidTransactionSummaryViewModel = {
						CountOfMonthlyStatement: 0,
						OfficialName: '',
						NameOnAccount: '',
						StartDate: '',
						EndDate: '',
						AverageDeposit: 0,
						AnnualCalculatedRevenue: 0.00,
						AverageWithdrawal: 0,
						AverageDailyBalance: 0,
						NumberOfNegativeBalance: 0,
						NumberOfNSF: 0,
						NSFAmount: 0,
						LoanPaymentAmount: 0,
						NumberOfLoanPayment: 0,
						PayrollAmount: 0,
						NumberOfPayroll: 0,
						ChangeInDepositVolume: 0,
						TotalCredits: 0,
						TotalDebits: 0,
						TotalCreditsCount: 0,
						TotalDebitsCount: 0,
						AvailableBalance: 0,
						CurrentBalance: 0,
						AverageBalanceLastMonth: 0,
						MedianMonthlyIncome: 0,
						MedianDailyBalance: 0,
						MaxDaysBelow100Count: 0,
						CVOfDailyDeposit: 0,
						CVOfDailyBalance: 0,
						AverageMonthlyRevenue: 0.00,
						TotalRevenueAmount: 0.00,
						TotalRevenueCount: 0,
						PDFReportName: '',
						BrokerAGS: 0,
						CAPAGS: 0,
						BrokerAGSText: ''
					};
					var objTransactionSummary = PlaidTransactionSummaryViewModel;
					objTransactionSummary.AvailableBalance = account.AvailableBalance;
					objTransactionSummary.CurrentBalance = account.CurrentBalance;
					var t = myCustomCashFlow;
					var objTemp = PlaidsortObject(objTransactionSummary);
					t.TransactionSummary = objTemp;
					cashFlowResult.CashFlow = t;
				}
			} else {
				var PlaidTransactionSummaryViewModel = {
					CountOfMonthlyStatement: 0,
					OfficialName: '',
					NameOnAccount: '',
					StartDate: '',
					EndDate: '',
					AverageDeposit: 0,
					AnnualCalculatedRevenue: 0.00,
					AverageWithdrawal: 0,
					AverageDailyBalance: 0,
					NumberOfNegativeBalance: 0,
					NumberOfNSF: 0,
					NSFAmount: 0,
					LoanPaymentAmount: 0,
					NumberOfLoanPayment: 0,
					PayrollAmount: 0,
					NumberOfPayroll: 0,
					ChangeInDepositVolume: 0,
					TotalCredits: 0,
					TotalDebits: 0,
					TotalCreditsCount: 0,
					TotalDebitsCount: 0,
					AvailableBalance: 0,
					CurrentBalance: 0,
					AverageBalanceLastMonth: 0,
					MedianMonthlyIncome: 0,
					MedianDailyBalance: 0,
					MaxDaysBelow100Count: 0,
					CVOfDailyDeposit: 0,
					CVOfDailyBalance: 0,
					AverageMonthlyRevenue: 0.00,
					TotalRevenueAmount: 0.00,
					TotalRevenueCount: 0,
					PDFReportName: '',
					BrokerAGS: 0,
					CAPAGS: 0,
					BrokerAGSText: ''
				};
				var objTransactionSummary = PlaidTransactionSummaryViewModel;
				objTransactionSummary.AvailableBalance = account.AvailableBalance;
				objTransactionSummary.CurrentBalance = account.CurrentBalance;
				var t = myCustomCashFlow;
				var objTemp = PlaidsortObject(objTransactionSummary);
				t.TransactionSummary = objTemp;
				cashFlowResult.CashFlow = t;
			}
			var formattedAccountNumber = account.BankName;
			cashFlowResult.InstitutionName = account.BankName;
			DataAttributePostFix = account.Id;
			cashFlowResult.referenceNumber = payload.eventData.ReferenceNumber;
			cashFlowResult.AccountID = account.Id;
			cashFlowResult.OfficialName = account.OfficialAccountName;
			cashFlowResult.NameOnAccount = account.NameOnAccount;
			cashFlowResult.AccountType = account.AccountType;
			cashFlowResult.AccountHeader = formattedAccountNumber + '-' + account.AccountNumber + '-' + account.AccountType;
			cashFlowResult.AccountNumber = account.AccountNumber;
			cashFlowResult.Source = account.Source;
			allAccountsCashflows = cashFlowResult;
			var resultKey = DataAttributePostFix;
			var Data = {};
			Data[resultKey] = allAccountsCashflows;
			return {
				'result': result,
				'detail': null,
				'data': Data,
				'rejectcode': '',
				'exception': []
			};
		});
	} else {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}

	function RemoveSpace(str) {
		var result = '';
		var allStr = str.split(' ');
		for (var s = 0; s < allStr.length; s++) {
			result += allStr[s].toString();
		}
		return result;
	}

	function FilterOutlier(trans, handleSuccess) {
		handleSuccess(trans);
	}

	function GetRecurringSummaryByCategoryId(accountId, trans, handleSuccess) {
		var recurringSummary = [];
		var categoryId = '21002000';
		var daySpanFlag = 15;
		var unsureFlag = 3;
		if (trans != null && trans.length > 0) {
			var allDebitTransactions = trans.filter(function (item) {
				return item.amount > 0 && item.categoryId == categoryId && GetDaysBetweenDate(item.transactionDate) <= daySpanFlag;
			});
			var moduloGroups = GetGroupByData(allDebitTransactions, 'amount');
			if (moduloGroups != null) {
				for (var property in moduloGroups) {
					if (moduloGroups[property].constructor === Array) {
						var grp = moduloGroups[property];
						if (grp != null && grp != undefined && grp.length < unsureFlag) { } else {
							var moduloSubGroups = GetGroupByData(grp, 'description');
							if (moduloSubGroups != null) {
								for (var p in moduloSubGroups) {
									if (moduloSubGroups[p].constructor === Array) {
										var grpSub = moduloSubGroups[p];
										if (grpSub != null && grpSub != undefined && grpSub.length >= unsureFlag) {
											var allMatchingTransactions = [];
											GetTransactionList(grpSub, function (trs) {
												allMatchingTransactions = trs;
											});
											var temp = {
												'Account': accountId,
												'Amount': grpSub[0].amount,
												'RoudingAmount': grpSub[0].amount,
												'Transactions': allMatchingTransactions,
												'ConfidenceLevel': 'High',
												'TotalTransactions': grpSub.length,
												'IsRecurring': true,
												'Status': 'RECURRING'
											};
											recurringSummary.push(temp);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		handleSuccess(recurringSummary);
	}

	function GetRecurringSummary(accountId, trans, handleSuccess) {
		var recurringSummary = [];
		var moduloRange = 5;
		var roundingMethodType = 2;
		var unsureFlag = 3;
		var minTransactionFlag = 5;
		var dayDifferenceFlag = 7;
		if (trans != null && trans.length > 0) {
			var allDebitTransactions = trans.filter(function (item) {
				return item.amount > 0;
			});
			for (var i = 0; i < allDebitTransactions.length; i++) {
				var rNumber = GetRoundingAmount(allDebitTransactions[i].amount, roundingMethodType);
				allDebitTransactions[i].RoudingAmount = rNumber;
				allDebitTransactions[i].RoudingAmountModulus = rNumber % moduloRange;
			}
			var moduloGroups = GetGroupByData(allDebitTransactions, 'RoudingAmountModulus');
			if (moduloGroups != null) {
				for (var property in moduloGroups) {
					if (moduloGroups[property].constructor === Array) {
						var grp = moduloGroups[property];
						if (grp != null && grp != undefined && grp.length <= unsureFlag) { } else {
							var moduloSubGroups = GetGroupByData(grp, 'RoudingAmount');
							if (moduloSubGroups != null) {
								for (var p in moduloSubGroups) {
									if (moduloSubGroups[p].constructor === Array) {
										var grpSub = moduloSubGroups[p];
										if (grpSub != null && grpSub != undefined && grpSub.length >= minTransactionFlag) {
											FilterOutlier(grpSub, function (d) {
												grpSub = d;
											});
											grpSub.sort((a, b) => {
												var start = +new Date(a.transactionDate);
												var elapsed = +new Date(b.transactionDate) - start;
												return elapsed;
											});
											for (var i = 0; i < grpSub.length; i++) {
												if (grpSub[i + 1] != undefined) {
													var diffDays = GetDaysBetweenTransaction(grpSub[i].transactionDate, grpSub[i + 1].transactionDate);
													grpSub[i].diffDays = diffDays;
												}
											}
											var MinValue = GetMinMaxValue(grpSub, 'min');
											var MaxValue = GetMinMaxValue(grpSub, 'max');
											var rangeDifference = MaxValue - MinValue;
											if (rangeDifference > 0) {
												if (rangeDifference <= dayDifferenceFlag) {
													var allMatchingTransactions = [];
													GetTransactionList(grpSub, function (trs) {
														allMatchingTransactions = trs;
													});
													var temp = {
														'Account': accountId,
														'Amount': grpSub[0].amount,
														'RoudingAmount': grpSub[0].RoudingAmount,
														'Transactions': allMatchingTransactions,
														'ConfidenceLevel': 'High',
														'Min': MinValue,
														'Max': MaxValue,
														'MaxMinDiff': rangeDifference,
														'TotalTransactions': grpSub.length,
														'IsRecurring': true,
														'Status': 'RECURRING'
													};
													recurringSummary.push(temp);
												} else { }
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		handleSuccess(recurringSummary);
	}

	function GetTransactionList(trans, handleSuccess) {
		var TransactionList = [];
		if (trans != null) {
			for (var t = 0; t < trans.length; t++) {
				var customDate = getCustomFormattedDate(trans[t].transactionDate);
				var transItem = {
					'Date': customDate.Date,
					'Description': trans[t].description.trim(),
					'Amount': Math.abs(trans[t].amount),
					'TransactionType': trans[t].amount < 0 ? 'Credit' : 'Debit',
					'RunningBalance': 0,
					'Month': customDate.Month,
					'Year': customDate.Year
				};
				TransactionList.push(transItem);
			}
		}
		handleSuccess(TransactionList);
	}

	function GetCategorySummary(trans, handleSuccess) {
		var predefinedCategoryTransactions = [];
		var CategorySummaryList = [];
		for (var c = 0; c < trans.length; c++) {
			if (allowedCategory.indexOf(trans[c].categoryId) > -1) {
				predefinedCategoryTransactions.push(trans[c]);
			}
		}
		var temp = [];
		var dayRange = 30;
		var allData = GetCategoryLookup();
		for (var n = 0; n < allData.length; n++) {
			var tempIndex = allData[n].CustomCategoryId;
			temp[tempIndex] = {
				TransactionCount: 0,
				CategoryName: allData[n].Name,
				TransactionTotal: 0.00,
				CategoryId: allData[n].PlaidCategoryId,
				CustomCategoryId: tempIndex,
				LastMonthTransactionCount: 0,
				LastMonthTransactionTotal: 0.00
			};
			CategorySummaryList.push(temp[tempIndex]);
		}
		predefinedCategoryTransactions.reduce(function (res, value) {

			var tIndex = parseInt(value.categoryId);
			var objTemp = temp.filter(function (e) {
				return e.CategoryId == tIndex;
			});
			var customIndex = 0;
			if (objTemp != null) {
				if (objTemp.length == 1) {
					customIndex = objTemp[0].CustomCategoryId;
				} else {
					if (value.amount > 0) {
						customIndex = 4;
					} else {
						customIndex = 5;
					}
				}
				if (customIndex != 7) {
					temp[customIndex].TransactionTotal += Math.abs(parseFloat(value.amount));
					temp[customIndex].TransactionCount += 1;
				}
				else {
					if (value.amount < 0) {
						temp[customIndex].TransactionTotal += Math.abs(parseFloat(value.amount));
						temp[customIndex].TransactionCount += 1;
					}
				}


				if (GetDaysBetweenDate(value.transactionDate) <= dayRange) {
					if (customIndex != 7) {
						temp[customIndex].LastMonthTransactionTotal += Math.abs(parseFloat(value.amount));
						temp[customIndex].LastMonthTransactionCount += 1;
					}
					else {
						if (value.amount < 0) {
							temp[customIndex].LastMonthTransactionTotal += Math.abs(parseFloat(value.amount));
							temp[customIndex].LastMonthTransactionCount += 1;
						}

					}

				}
			}
			return temp;
		}, {});
		handleSuccess(CategorySummaryList);
	}

	function GetCategoryLookup() {
		var myCat = [{
			'PlaidCategoryId': '21002000',
			'Name': 'ACH Credit',
			'CustomCategoryId': '5'
		}, {
			'PlaidCategoryId': '21002000',
			'Name': 'ACH Debit',
			'CustomCategoryId': '4'
		}, {
			'PlaidCategoryId': '10001000',
			'Name': 'Overdraft',
			'CustomCategoryId': '3'
		}, {
			'PlaidCategoryId': '21009000',
			'Name': 'Payroll',
			'CustomCategoryId': '1'
		}, {
			'PlaidCategoryId': '16003000',
			'Name': 'Loan Payment',
			'CustomCategoryId': '2'
		}, {
			'PlaidCategoryId': '10007000',
			'Name': 'NSF',
			'CustomCategoryId': '6'
		},
		{
			'PlaidCategoryId': '21001000',
			'Name': 'Online Transfer',
			'CustomCategoryId': '7'
		}
		];
		return myCat;
	}

	function GetLastCalendarMonth(counter) {
		var today = new Date();
		var lastNMonths = [];
		for (var i = 1; i <= counter; i++) {
			var month = today.getMonth() - i;
			var year = today.getFullYear();
			if (month < 0) {
				month = month + 12;
				year = year - 1;
			}
			lastNMonths.push(month + '-' + year);
		}
		return lastNMonths;
	}

	function AGSSum(numbersArray) {
		if (numbersArray != null && numbersArray.length > 0) {
			return numbersArray.reduce(function (a, b) {
				return a + b
			});
		} else {
			return 0;
		}
	}

	function GetDaysBetweenTransaction(d1, d2) {
		var date1 = new Date(d1);
		var date2 = new Date(d2);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return diffDays;
	}

	function GetRoundingAmount(amount, methodType) {
		var result = amount;
		switch (methodType) {
			case 1:
				result = Math.floor(amount);
				break;
			case 2:
				result = Math.ceil(amount);
				break;
		}
		return result;
	}

	function GetGroupByData(data, prop) {
		return data.reduce(function (groups, item) {
			var val = item[prop];
			groups[val] = groups[val] || [];
			groups[val].push(item);
			return groups;
		}, {});
	}

	function GetMinMaxValue(Values, search) {
		var result = 0;
		if (search == 'min') {
			result = 10000;
			for (var i = 0; i < Values.length; i++) {
				if (Values[i].diffDays != undefined && Values[i].diffDays <= result) {
					if (Values[i].diffDays != 0) {
						result = Values[i].diffDays;
					}
				}
			}
		} else if (search == 'max') {
			for (var i = 0; i < Values.length; i++) {
				if (Values[i].diffDays != undefined && Values[i].diffDays >= result) {
					result = Values[i].diffDays;
				}
			}
		}
		return result;
	}

	function GetDaysBetweenDate(d1) {
		var date1 = new Date(d1);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return diffDays;
	}

	function getFormattedDate(date) {
		date = new Date(date);
		var year = date.getFullYear();
		var month = (1 + date.getMonth()).toString();
		month = month.length > 1 ? month : '0' + month;
		var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
		return month + '/' + day + '/' + year;
	}

	function getCustomFormattedDate(date) {
		date = new Date(date);
		var year = date.getFullYear();
		var month = (1 + date.getMonth()).toString();
		var MonthName = Plaidmonths[date.getMonth()];
		month = month.length > 1 ? month : '0' + month;
		var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
		var customData = {
			Date: month + '/' + day + '/' + year,
			Month: MonthName,
			Year: year
		};
		return customData;
	}

	function isWeekday(year, month, day) {
		var day = new Date(year, month, day).getDay();
		return day != 0 && day != 6;
	}

	function plaidGetDaysInMonth(month, year) {
		var TotalDaysInMonth = new Date(year, month + 1, 0).getDate();
		if (ExcludeWeekEnds == false) {
			return TotalDaysInMonth;
		} else {
			var weekdays = 0;
			for (var i = 0; i < TotalDaysInMonth; i++) {
				if (isWeekday(year, month, i + 1))
					weekdays++;
			}
			return weekdays;
		}
	}

	function PlaidcustomRound(value, precision) {
		var multiplier = Math.pow(10, precision || 0);
		return Math.round(value * multiplier) / multiplier;
	}

	function PlaidsortObject(o) {
		var sorted = {},
			key,
			a = [];
		for (key in o) {
			if (o.hasOwnProperty(key)) {
				a.push(key);
			}
		}
		a.sort();
		for (key = 0; key < a.length; key++) {
			sorted[a[key]] = o[a[key]];
		}
		return sorted;
	}

	function Plaidpad(s) {
		return (s < 10) ? '0' + s : s;
	}

	function PlaidcustomDate(d) {
		return [Plaidpad(d.getMonth() + 1), Plaidpad(d.getDate()), d.getFullYear()].join('/');
	}

	function PlaidcalculateAvgBalanceOfMonth(dailyBalanceList, numberOfDaysInMonth) {
		var totalMonthlyDailyBalance = 0;
		var emptyCount = 1;
		for (var index = 30; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}

	function CalculateADB(dailyBalanceList, numberOfDaysInMonth, totalDailyBalance) {
		var totalMonthlyDailyBalance = 0;
		var daysCounter = numberOfDaysInMonth - 1;
		var emptyCount = 1;
		for (var index = daysCounter; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		if (emptyCount > 1) {
			var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
			totalMonthlyDailyBalance += lastDailyBalance;
		}
		return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}

	function CalculateADBFirstMonth(dailyBalanceList, numberOfDaysInMonth) {
		var totalMonthlyDailyBalance = 0;
		var emptyCount = 1;
		var daysCounter = numberOfDaysInMonth - 1;
		for (var index = daysCounter; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		if (emptyCount > 1) {
			numberOfDaysInMonth = (numberOfDaysInMonth - emptyCount) + 1;
		}
		return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}

	function CalculateADBLastMonth(dailyBalanceList, numberOfDaysInMonth) {
		var totalMonthlyDailyBalance = 0;
		var emptyCount = 1;
		var lastDailyBalance = 0;
		var IsLastBalance = false;
		var daysCounter = numberOfDaysInMonth - 1;
		for (var index = daysCounter; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				if (IsLastBalance == false) {
					IsLastBalance = true;
					lastDailyBalance = dailyBalanceList[index];
				}
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		if (emptyCount > 1) {
			emptyCount = emptyCount - 1;
			totalMonthlyDailyBalance += lastDailyBalance * emptyCount;
		}
		return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}

	function GetDaysBelow100Count(data) {
		var count = 0;
		data = RemoveUndefinedData(data);
		for (var c = 0; c < data.length; c++) {
			if (data[c] < 1000) {
				count = count + 1;
			}
		}
		return count;
	}

	function RemoveUndefinedData(data) {
		data = data.filter(function (element) {
			return element !== undefined;
		});
		return data;
	}

	function GetMedianSingleArray(data) {
		data = RemoveUndefinedData(data);
		var m = data.sort(function (a, b) {
			return a - b;
		});
		var middle = Math.floor((m.length - 1) / 2);
		if (m.length % 2) {
			return m[middle];
		} else {
			return (m[middle] + m[middle + 1]) / 2.0;
		}
	}

	function GetMaxDaysBelow100(data) {
		return Math.max.apply(Math, data.map(function (o) {
			return o.DaysBelow100Count;
		}));
	}

	function DailyCoEfficient(data, sum, count) {
		Math.mean = function (array) {
			return array.reduce(function (a, b) {
				return a + b;
			}) / array.length;
		};
		Math.stDeviation = function (array, mean) {
			var dev = array.map(function (itm) {
				return (itm - mean) * (itm - mean);
			});
			return Math.sqrt(dev.reduce(function (a, b) {
				return a + b;
			}) / (array.length - 1));
		};
		data = RemoveUndefinedData(data);
		if (data != null && data.length > 0) {
			var average = Math.mean(data);
			var standardDeviation = Math.stDeviation(data, average);
			if (standardDeviation != undefined && !isNaN(standardDeviation)) {
				return (standardDeviation / average) * 100;
			}
			return 0;
		} else {
			return 0;
		}
	}
}