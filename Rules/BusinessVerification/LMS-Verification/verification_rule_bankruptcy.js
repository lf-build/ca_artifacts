function verification_rule_bankruptcy(payload) {
	var result = 'Passed';
	var Data = {};
	if (payload != null && payload != undefined) {
		Data = payload.bankruptcyData[0];
		if(Data.chapter == null)
		{
			result = 'Failed';
		}
		if (Data.chapter != '7' && Data.chapter != '11' && Data.chapter != '12' && Data.chapter != '13'){
			result = 'Failed';
		}
		if(Data.court == null)
		{
			result = 'Failed';
		}
		if(Data.caseReferenceNumber == null)
		{
			result = 'Failed';
		}
		if(Data.caseStatus.toLowerCase() != 'submitted' && Data.caseStatus.toLowerCase() != 'discharged' && Data.caseStatus.toLowerCase() != 'dismissed' )
		{
			result = 'Failed';
		}

	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
