function verification_rule_searchvarificationmanual(payload) {
	var result = 'Failed';
	var searchData = null;
	var Data = null;
	var webLink1 = '';
	var webLink2 = '';
	var webLink3 = '';
	var webLink4 = '';
	var webLink5 = '';
	var webLink6 = '';
	var webLink7 = '';
	var webLink8 = '';
	var webLink9 = '';
	var webLink10 = '';
	if (payload != null && payload != undefined) {
		searchData = payload.searchVerificatioManualData[0];
		if (searchData != null) {
			webLink1 = searchData.webLink1;
			webLink2 = searchData.webLink2;
			webLink3 = searchData.webLink3;
			webLink4 = searchData.webLink4;
			webLink5 = searchData.webLink5;
			webLink6 = searchData.webLink6;
			webLink7 = searchData.webLink7;
			webLink8 = searchData.webLink8;
			webLink9 = searchData.webLink9;
			webLink10 = searchData.webLink10;
			result = 'Passed';
		}
		Data = {
			'webLink1': webLink1,
			'webLink2': webLink2,
			'webLink3': webLink3,
			'webLink4': webLink4,
			'webLink5': webLink5,
			'webLink6': webLink6,
			'webLink7': webLink7,
			'webLink8': webLink8,
			'webLink9': webLink9,
			'webLink10': webLink10
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
