function data_attribute_datamerchsearchmerchantrequestedfailrule(payload) {
    var result = 'Passed';
    var NoHit = true;
    return {
        'result': result,
        'detail': null,
        'data': {
            'datamerchReport':
            {
                'Fein': '',
                'LegalName': '',
                'Dba': '',
                'Address': '',
                'Street1': '',
                'Street2': '',
                'City': '',
                'State': '',
                'BusinessPhone': '',
                'BusinessStartdate': '',
                'Industry': '',
                'NoHitOrFailed': true,
                'MerchantNotes': {
                    'Notes': []
                },
                'referenceNumber': payload.eventData.ReferenceNumber
            }
        },
        'rejectcode': '',
        'exception': []
    };
}