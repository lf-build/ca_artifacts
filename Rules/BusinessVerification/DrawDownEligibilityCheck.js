function DrawDownEligibilityCheck(payload) {

	if (payload != null && payload.drawDown != null) {
		var laonAmount = payload.drawDown[0].applicationData.LoanAmount;
		var drawdownAmount = payload.drawDown[0].applicationData.TotalDrawDownAmount;
		var requestedAmount = payload.drawDown[0].DrawDownData.RequestedAmount;
		var result = 'Passed';

		if (requestedAmount <= (laonAmount - drawdownAmount)) {
			result = 'Passed';
		}
		else
		{
			result = 'Failed';
		}
		
		return {
			'result': result,
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	} else {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': ['drawDown Data not fount']
		};
	}
}
