function data_attribute_experian_business_search_failed_rule(payload) {
    try {
        var subscriberNumber = null;
        var subscriberNumber = null;
        var inquiryTransactionNumber = null;
        var cpuVersion = null;
        var action = 'HIGH RISK';
        var RTC037 = 0;
        var CLC006 = 0;
        var TaxLienFilingCount = '';
        var JudgmentFilingCount = '';
        var UccFilings = '';
        var IntelliScore = '';
        var FinancialStability = '';
        var LegalBalance = '';
        var Bankruptcy = '';
        var UCCFiledDate = '';
        var SecuredParty = '';
        var CautionaryUCCFilings = '';
        var Sumoflegalfilings = '';
        var Collateral = '';
        var DateofIncorporation = '';
        var CurrentStatus = '';
        var Timeinbusiness = 0;
        var m_action_high = 0;
        var m_action_miss = 0;
        var m_CLC006 = 0;
        var m_RTC037 = 0;
        var arrActions = ['HIGH RISK', 'RECENT BANKRUPTCY ON FILE'];
        var Data = {
            'experianBusinessReport': {
                'action': action,
                'CLC006': CLC006,
                'RTC037': RTC037,
                'IntelliScore': IntelliScore,
                'FinancialStability': FinancialStability,
                'TaxLienFilingCount': TaxLienFilingCount,
                'JudgmentFilingCount': JudgmentFilingCount,
                'UccFilings': UccFilings,
                'LegalBalance': LegalBalance,
                'Bankruptcy': Bankruptcy,
                'm_action_miss': m_action_miss,
                'm_action_high': m_action_high,
                'm_CLC006': m_CLC006,
                'm_RTC037': m_RTC037,
                'CautionaryUCCFilings': CautionaryUCCFilings,
                'Sumoflegalfilings': Sumoflegalfilings,
                'SecuredParty': SecuredParty,
                'Collateral': Collateral,
                'DateofIncorporation': DateofIncorporation,
                'CurrentStatus': CurrentStatus,
                'UCCFiledDate': UCCFiledDate,
                'Timeinbusiness': Timeinbusiness,
                'referenceNumber': payload.eventData.ReferenceNumber,
                'Failed': true
            }
        };
        return {
            'result': 'Passed',
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };

    } catch (e) {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };
    }
}
