var matchStrings = ['context.call(\'', 'this.service(\'', 'this.call(\'', 'context.service(\''];
var path = require('path');
var fs = require('fs');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function getDependencies(str, matchString) {
    var all = [];
    var firstMatch = str.split(matchString);
    if (firstMatch[1] != undefined) {
        var lastIndex = firstMatch[1].indexOf('\'');
        var pushIt = str.split(matchString)[1].substring(0, lastIndex).replace(/'/g, '');
        all.push(pushIt);
        var newStr = str.substring(str.indexOf(pushIt), str.length);
        if (firstMatch.length > 2 && newStr.indexOf(matchString) >= 0) {
            var depend = getDependencies(newStr, matchString)
            for (var p = 0; p < depend.length; p++) {
                all.push(depend[p]);
            }
        }
        return all;
    }
    return null;
}

function saveRule(serviceUrl, source, token) {
    var api = require('./api_module.js')(token);
    var rule = {};
    var dependency = [];
    var childDependency = [];
    for (var i = 0; i < matchStrings.length; i++) {
        childDependency = getDependencies(source, matchStrings[i]);
        if (childDependency != null) {
            childDependency.forEach(function (element) {
                if (dependency.indexOf(element) == -1) {
                    dependency.push(element);
                }
            }, this);
        }
    }
    if (dependency != null) {
        rule = { source: source, dependencies: dependency };
    } else {
        rule = { source: source };
    }
    return new Promise(function (resolve, reject) {
        api.put(serviceUrl, rule).then(function (result) {
            if (result.statusCode == 200) {
                console.log("\r\nINFO:Save Rule Success ");
                resolve(result);
            }
            else {
                console.error("\r\n\r\nSave Rule Failed " + result.statusCode + ",message:" + result.statusMessage + ",response:" + JSON.stringify(result.body));
                console.log(rule);
                //reject(response);
            }
        });
    });
}

function importRules(serviceUrl, folderPath, token) {
    var files = walkSync(folderPath);
    var rulesToProcess = [];

    for (var i = 0; i < files.length; i++) {
        var element = files[i];
        var pathObject = path.parse(element);
        if (pathObject.ext.toLocaleLowerCase() == ".js") {
            var data = fs.readFileSync(element, 'utf8');
            rulesToProcess.push(data.replace(/\r|\n|\t/g, ''));
        }
        else {
            console.warn('Skipping file ' + element + ' as extension :' + pathObject.ext.toLocaleLowerCase());
        }
    }

    rulesToProcess = rulesToProcess.map(r => () => saveRule(serviceUrl, r, token));

    return rulesToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Rule failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveRule: function (serviceUrl, source) {
            return saveRule(serviceUrl, source, token);
        },
        importRules: function (serviceUrl, folderPath) {
            return importRules(serviceUrl, folderPath, token);
        }
    }
}