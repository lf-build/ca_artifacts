function applicationFilters() {
	var self = this;
	var baseUrl = 'http://application-filters:5000';
	var url = [baseUrl, 'getapplicationsbyentityids', 'all'].join('/');
	return {
		get: function (input) {
			return self.http.post(url, input);
		}
	};
}