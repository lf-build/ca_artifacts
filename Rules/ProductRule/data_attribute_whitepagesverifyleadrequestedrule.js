function data_attribute_whitepagesverifyleadrequestedrule(payload) {
	var result = 'Passed';
	if (payload && payload.eventData
		&& payload.eventData.Response
		&& payload.eventData.Response.Request) {
		var phoneChecks = payload.eventData.Response.PhoneChecks;
		var name = payload.eventData.Response.Request.Name;
		var phone = payload.eventData.Response.Request.Phone;
		var email = payload.eventData.Response.Request.Email;
		var firstName = payload.eventData.Response.Request.FirstName;
		var lastName = payload.eventData.Response.Request.LastName;
		var postalCode = payload.eventData.Response.Request.PostalCode;
		var stateCode = payload.eventData.Response.Request.StateCode;
		var streetLine1 = payload.eventData.Response.Request.StreetLine1;
		var streetLine2 = payload.eventData.Response.Request.StreetLine2;
		var addressChecks = payload.eventData.Response.AddressChecks;
		var emailAddressChecks = payload.eventData.Response.EmailAddressChecks;
		var subscriberAddress = null;
		if (phoneChecks && phoneChecks.SubscriberAddress) {
			subscriberAddress = {
				'StreetLine1': phoneChecks.SubscriberAddress.StreetLine1,
				'StreetLine2': phoneChecks.SubscriberAddress.StreetLine2,
				'City': phoneChecks.SubscriberAddress.City,
				'PostalCode': phoneChecks.SubscriberAddress.PostalCode,
				'StateCode': phoneChecks.SubscriberAddress.StateCode,
				'CountryName': phoneChecks.SubscriberAddress.CountryName,
				'CountryCode': phoneChecks.SubscriberAddress.CountryCode
			}
		};
		var Data = {
			'whitepageReport': {
				'Name': name,
				'Phone': phone,
				'IsConnected': phoneChecks.IsConnected,
				'SubscriberName': phoneChecks.SubscriberName,
				'PhoneToName': phoneChecks.PhoneToName,
				'IsPrepaid': phoneChecks.IsPrepaid,
				'IsCommercial': phoneChecks.IsCommercial,
				'SubscriberAddress': subscriberAddress,
				'error': null,
				'warnings': [],
				'isValid': true,
				'AddressChecks': addressChecks,
				'EmailAddressChecks': emailAddressChecks,
				'Carrier': phoneChecks.Carrier,
				'CountryCode': phoneChecks.CountryCode,
				'LineType': phoneChecks.LineType,
				'PhoneContactScore': phoneChecks.PhoneContactScore,
				'Email': email,
				'FirstName': firstName,
				'LastName': lastName,
				'PostalCode': postalCode,
				'StateCode': stateCode,
				'StreetLine1': streetLine1,
				'StreetLine2': streetLine2,
				'referenceNumber': payload.eventData.ReferenceNumber
			}
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} else {
		return {
			'result': result,
			'detail': null,
			'data': {
				'whitepageReport': {
					'Name': null,
					'Phone': null,
					'IsConnected': null,
					'SubscriberName': null,
					'PhoneToName': null,
					'IsPrepaid': null,
					'IsCommercial': null,
					'SubscriberAddress': null,
					'error': null,
					'warnings': [],
					'isValid': true,
					'AddressChecks': null,
					'EmailAddressChecks': null,
					'Carrier': null,
					'CountryCode': null,
					'LineType': null,
					'PhoneContactScore': null,
					'Email': null,
					'FirstName': null,
					'LastName': null,
					'PostalCode': null,
					'StateCode': null,
					'StreetLine1': null,
					'StreetLine2': null,
					'referenceNumber': null
				}
			},
			'rejectcode': '',
			'exception': []
		};
	}
}
