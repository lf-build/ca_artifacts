function verification_rule_merchantstatementmanual(payload) {
	try {
		var result = 'Failed';
		if (payload != null && payload != undefined) {
			var objData = payload.merchantStatementVerificationData[0];
			if (objData.merchantStatement == true) {
				result = 'Passed';
			}
		}
		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': 'Unable to verify',
			'data': payload,
			'rejectcode': '',
			'exception': []
		};
	}
}